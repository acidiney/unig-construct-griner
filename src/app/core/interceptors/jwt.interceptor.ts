import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '@core/authentication/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const t = this.auth.tokenValue;
    if (t) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${t.token}`,
          Accept: 'application/json',
        },
      });
    }
    return next.handle(request);
  }
}
