// @ts-nocheck
import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { Router } from '@angular/router';

@Injectable()
export class ErroInterceptor implements HttpInterceptor {
  protected errorHandler;
  constructor(private router: Router, public iziToast: Ng2IzitoastService) {
    this.iziToast.settings({
      timeout: 10000,
      toastOnce: true,
      pauseOnHover: false,
      transitionIn: 'flipInX',
      transitionOut: 'flipOutX',
      position: 'topRight',
    });
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap((evt) => {
        if (evt instanceof HttpResponse) {
          if (evt.body.message) {
            this.iziToast.success({
              title: 'Sucesso: ',
              message: evt.body.message,
            });
          }
        }
      }),
      catchError(({ error: err, status }: HttpErrorResponse) => {  
          let errorMessage = status === 0 ? 'Dificuldades em estabelecer conexão com o servidor.' 
                    : err instanceof Array ? err[0].message 
                    : err.error === undefined? err.message : err.error.message
          

        if ( status === 401 &&
          errorMessage.split(':')[0].includes('E_INVALID_JWT_TOKEN')
        ) {
          errorMessage = 'Token expirado, faça login.';
        }

        const error = this.createErrorHandler(errorMessage)[status];
        if (!error) {
          this.iziToast.error({
            title: 'Erro desconhecido',
            message: 'Contacte o Administrador',
          });
        } else {
          error();
        }
        return throwError(err);
      })
    );
  }

  protected createErrorHandler(message: string) {
    const handleError = (
      title: string,
      callback: Function,
      type: string = 'error',
      notify: boolean = true,
      alternativeMessage?: string
    ) => {
      if (notify) {
        this.iziToast[type]({
          title: title || '',
          message: alternativeMessage || message,
        });
      }
      if (callback) {
        callback();
      }
    };

    return {
      400: () => {
        handleError(null, null, 'warning');
      },
      401: () => {
        handleError(
          'Não autorizado:',
          () => {
            this.router.navigate(['/login']);
          },
          'error',
          true
        );
      },
      403: () => {
        handleError(
          null,
          () => {
            this.router.navigate(['/403']);
          },
          null,
          false
        );
      },
      404: () => {
        handleError('Não encontrado', null, 'warning');
      },0: () => {
        handleError('Erro de Conexão', null, 'error');
      },
      500: () => {
        handleError('Erro Interno:', null, 'error', true, 'Contacte o administrador.');
      },
    };
  }
}
