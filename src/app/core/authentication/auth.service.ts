import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

import { environment } from '@environments/environment';
import { UserResponse } from '@shared/models/userResponse';
import { Token } from '@shared/models/token';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, finalize, debounceTime } from 'rxjs/operators';
import { Router } from '@angular/router';

/**
 * @description private method to decode token
 * @param {String} token
 * @author Acidiney Dias <acidineydias@gmail.com>
 */
const _decodeToken = (token) => {
  const base64HeaderUrl = token.split('.')[1];
  const base64Header = base64HeaderUrl.replace('-', '+').replace('_', '/');
  const headerData = JSON.parse(window.atob(base64Header));
  return headerData;
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private tokenSubject: BehaviorSubject<Token>;
  public token: Observable<Token>;

  private current = 'currentUserId';

  constructor(
    private http: HttpClient,
    public jwtHelper: JwtHelperService,
    private router: Router
  ) {
    this.tokenSubject = new BehaviorSubject<Token>(
      JSON.parse(localStorage.getItem(this.current))
    );
    this.token = this.tokenSubject.asObservable();
  }

  //
  public get tokenValue(): Token {
    return this.tokenSubject.value;
  }

  /**
   *
   * @param username
   * @param password
   *
   * Descricao: recebe 2 parametros retorna um objecto do tipo UserResponse
   */
  public login(username, password) {
    return this.http
      .post<UserResponse>(environment.app_url + 'user/authenticate', {
        username,
        password,
      })
      .pipe(
        map((response) => {
          const object = Object(response).data 
          // login successful if there's a jwt token in the response
          if (object) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem(this.current, JSON.stringify(object));
            this.tokenSubject.next(object);
          }
        })
      );
  }

  /**
   *
   * @param username
   * @param password
   *
   * Descricao: recebe 2 parametros retorna um objecto do tipo UserResponse
   */
  public alterarSenha(form, token) {
    return this.http
      .post<UserResponse>(environment.app_url + 'auth/password/reset/' + token, form)
      .pipe(
        map((response) => {
          const object = Object(response).object;
          // login successful if there's a jwt token in the response
          if (object) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem(this.current, JSON.stringify(object));
            this.tokenSubject.next(object);
          }
        })
      );
  }

  public getMenu() {
    return this.http
      .get<any>(environment.app_url + 'menu')
      .pipe(map((response) => Object(response).object));
  }

  /**
   *
   * @param username
   * @param password
   *
   * Descricao: recebe 2 parametros retorna um objecto do tipo UserResponse
   */
  public recuperarSenha(username) {
    return this.http
      .post<UserResponse>(environment.app_url + 'auth/password/reset', {
        username
      })
      .pipe(
        map((response) => {
          const object = Object(response).object;
          // login successful if there's a jwt token in the response
          if (object) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem(this.current, JSON.stringify(object));
            this.tokenSubject.next(object);
          }
        })
      );
  }

  // Terminar sessão
  public logout() {
    const token = this.tokenValue.type + ' ' + this.tokenValue.token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: token,
    });
    // remove user from local storage to log user out
    this.http
      .post(environment.app_url + 'auth/logout', { headers })
      .pipe(finalize(() => {}))
      .subscribe((data) => {
        // remove user from local storage to log user out
        localStorage.removeItem(this.current);
        this.router.navigate(['/login']);
      });
  }

  public getUser() {
    return _decodeToken(this.tokenValue.token).data.user.name;
  }

  public profile() {
    return _decodeToken(this.tokenValue.token).data.user;
  }

  // Retorna os dados de login decriptografado
  public getUserFull() {
    return _decodeToken(this.tokenValue.token).data;
  }

  // verifica se o user tem permissão
  isPermission(permission: string = null, route: any = null): boolean {
    permission =
      permission == null ? route.data.expectedPermission : permission;
    const permissions = _decodeToken(this.tokenValue.token).data.permissions;

    if (permission) {
      if (permissions.includes(permission)) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  // Verifica se o user esta logado
  getAuthStatus() {
    return localStorage.getItem(this.current) ? true : false;
  }

  // verifica se o user esta logado
  public isAuthenticated(): boolean {
    const token = !this.getAuthStatus() ? null : this.tokenValue.token;
    // Check whether the token is expired and return
    // Verifique se o token expirou e retorne
    // true or false
    return !this.jwtHelper.isTokenExpired(token);
  }
}
