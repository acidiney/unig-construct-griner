import { Injectable } from '@angular/core';
interface Scripts {
  name: string;
  src: string;
}
export const ScriptStore: Scripts[] = [  
  
  { name: 'jqueryMin', src: 'assets/plugins/jquery/jquery.min.js'},
  { name: 'select2', src: 'assets/plugins/select2/select2.full.min.js'},
  { name: 'custom', src: 'assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js'},
  { name: 'bundle', src: 'assets/plugins/bootstrap/js/bootstrap.bundle.min.js'},
  { name: 'steps', src: 'assets/plugins/jquery.steps-1.1.0/jquery.steps.js'},
  { name: 'initialize', src: 'assets/plugins/jquery.steps-1.1.0/jquery.step.initialize.js'},
  { name: 'adminlte', src: 'assets/js/adminlte.js'},
  { name: 'demo', src: 'assets/js/demo.js'} 
  
];
/* 
{ name: 'jqueryMin', src:'assets/plugins/jquery/jquery.min.js'}, 
  { name: 'bootstrap', src:'assets/plugins/bootstrap/js/bootstrap.bundle.min.js'}, 
  { name: 'slimscroll', src:'assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'},  
  { name: 'adminlte', src:'assets/js/adminlte.min.js'}, 
  { name: 'demo', src:'assets/js/demo.js'},*/

@Injectable({
  providedIn: 'root'
})
export class ScriptService {

  private scripts: any ={};
 i  = 1;
  constructor() {
    ScriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src
      };
    });
  }

  load(...scripts: string[]) {
    const promises: any[] = [];
    scripts.forEach((script) => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

  loadScript(name: string) {
    return new Promise((resolve, reject) => {
      // resolve if already loaded
      //console.log(`Loaded:::::: `+this.i,this.scripts[name]);
      if (this.scripts[name].loaded==undefined) {
        resolve({ script: name, loaded: true, status: 'Already Loaded' });
      } else {
        // load script
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = this.scripts[name].src;
        script.onload = () => {
          this.scripts[name].loaded = true;
         // console.log(`${name} Loaded.`);
          resolve({ script: name, loaded: true, status: 'Loaded' });
        };
        script.onerror = (error: any) => resolve({ script: name, loaded: false, status: 'Loaded' });
        document.getElementsByTagName('head')[0].appendChild(script);
      }
      this.i++;
    });
  }
}