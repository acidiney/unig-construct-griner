import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class ApiService { 
  
  constructor(private http: HttpClient, private jwtService: JwtHelperService) {}

  private setHeaders(): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };
    if (this.jwtService.tokenGetter()) {
      headersConfig['Authorization'] = this.jwtService.tokenGetter();
    }
    return new HttpHeaders(headersConfig);
  }

  public formatErrors(error: any) {
    return throwError(JSON.stringify(error));
  }

  get(path: string, httpParams: HttpParams = new HttpParams()): Observable<any> {
    return this.http
      .get<any>(`${environment.app_url}${path}`, { 
        headers: this.setHeaders(),
        params: httpParams,
      })
      .pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http
      .put<any>(`${environment.app_url}${path}`, JSON.stringify(body), {
        headers: this.setHeaders(),
      })
      .pipe(catchError(this.formatErrors));
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http
      .post<any>(`${environment.app_url}${path}`, body, {
        headers: this.setHeaders(),
      })
      .pipe(catchError(this.formatErrors));
  }

  delete(
    path: string,
    httpParams: HttpParams = new HttpParams()
  ): Observable<any> {
    return this.http
      .delete<any>(`${environment.app_url}${path}`, {
        headers: this.setHeaders(),
        params: httpParams,
      })
      .pipe(catchError(this.formatErrors));
  }
}
