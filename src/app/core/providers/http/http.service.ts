import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@environments/environment';
import { AuthService } from '@core/authentication/auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private auth: AuthService, private http: HttpClient) {}

  public __call(url: string, body: object) {
    const token = this.auth.tokenValue.type + ' ' + this.auth.tokenValue.token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: token,
    });
    url = this.getBaseUrl() + url;
    return this.http.post<any>(url, body, { headers });
  }

  public call_get(url: string) {
    const token = this.auth.tokenValue.type + ' ' + this.auth.tokenValue.token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: token,
    });
    url = this.getBaseUrl() + url;
    return this.http.get(url, { headers });
  }

  public call_patch(url: string, body: object) {
    const token = this.auth.tokenValue.type + ' ' + this.auth.tokenValue.token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: token,
    });
    url = this.getBaseUrl() + url;
    return this.http.patch<any>(url, body, { headers });
  }

  public call_delete(url: string) {
    const token = this.auth.tokenValue.type + ' ' + this.auth.tokenValue.token;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: token,
    });
    url = this.getBaseUrl() + url;
    return this.http.delete(url, { headers });
  }

  private getBaseUrl() {
    return environment.app_url;
  }
  canActivateRouterLink(permission: string): boolean {
    return this.auth.isPermission(permission);
  }

  public _getFileFromServer(url): any {
    url = this.getBaseUrl() + url;
    return this.http
      .post<Blob>(url, null, { responseType: 'blob' as 'json' })
      .pipe(
        map((res) => {
          return new Blob([res], { type: 'application/pdf' });
        })
      );
  }
}
