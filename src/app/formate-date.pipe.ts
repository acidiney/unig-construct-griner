import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formateDate'
})
export class FormateDatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) return value
    return new Date(value).toLocaleDateString('pt-PT')
  }

}
