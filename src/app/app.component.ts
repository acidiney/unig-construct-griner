import { Component, OnInit } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { ScriptService } from "@core/services/script.service";
import { NgxSpinnerService } from "ngx-spinner";

declare let pdfMake: any;
@Component({
  // tslint:disable-next-line
  selector: "body",
  templateUrl: "app.component.html",
})
export class AppComponent implements OnInit {
  constructor(
    private scriptService: ScriptService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    //console.log('Loading External Scripts');
    //this.scriptService.load('jqueryMin','bootstrap','slimscroll','adminlte2','demo');
    this.scriptService.load(
      "jqueryMin",
      "select2",
      "custom",
      "bundle",
      "steps",
      "initialize",
      "adminlte",
      "demo"
    );
  }

  ngOnInit() {
    /** spinner starts on init */
    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
