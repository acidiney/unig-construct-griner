import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgxLoadingModule } from 'ngx-loading';
import { JwtModule } from '@auth0/angular-jwt';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule } from '@angular/forms'; 
 
import { ApiService } from '@core/providers/http/api.service';
import { HelperFunctionsService } from '@shared/services/helper-functions.service';
import { JwtInterceptor } from './core/interceptors/jwt.interceptor';
import { ErroInterceptor } from './core/interceptors/erro-interceptor';
import { environment, server_config } from '../environments/environment';

import { AppComponent } from './app.component';

// Import containers 

import { P404Component } from './resources/error/404.component';
import { P403Component } from './resources/error/403.component';
import { P500Component } from './resources/error/500.component';  

import { LayoutModule } from './containers/layout/layout.module';
import { LayoutComponent } from './containers/layout/layout.component'; 
// Import routing module
import { AppRoutingModule } from './app.routing';
  
import { Ng2IziToastModule } from 'ng2-izitoast';   
import { OrcamentoDashboardComponent } from 'app/resources/views/crm/orcamentos/orcamento-dashboard/orcamento-dashboard.component';
// Import library module
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxSpinnerService } from 'ngx-spinner';
import { FormateDatePipe } from './formate-date.pipe'; 


@NgModule({
  imports: [ 
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule, 
    Ng2IziToastModule,
    NgxPaginationModule,
    LayoutModule.forRoot({}), 
    NgxSpinnerModule,
    NgxLoadingModule.forRoot({
      backdropBorderRadius: '3px',
      backdropBackgroundColour: 'rgba(255, 255, 255, 0.78)',
      primaryColour: '#20a8d8',
      secondaryColour: '#20a8d8',
      tertiaryColour: '#20a8d8',
    }),
    JwtModule.forRoot({
      config: {
        allowedDomains: [server_config.host],
        disallowedRoutes: [environment.app_url],
      },
    }), 
    
  ],
  declarations: [ 
    OrcamentoDashboardComponent,
    LayoutComponent,
    AppComponent, 
    P404Component,
    P403Component,
    P500Component,
    FormateDatePipe
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErroInterceptor, multi: true },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    ApiService,NgxSpinnerService,HelperFunctionsService
    // provider used to create fake backend
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent],
})
export class AppModule {}
