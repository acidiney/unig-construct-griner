import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditImpostoComponent } from './create-or-edit-imposto.component';

describe('CreateOrEditImpostoComponent', () => {
  let component: CreateOrEditImpostoComponent;
  let fixture: ComponentFixture<CreateOrEditImpostoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditImpostoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditImpostoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
