import { Component, OnInit } from '@angular/core';
import { HttpService } from '@core/providers/http/http.service'; 

import { ExcelService } from '@shared/services/excel.service';
import * as moment from 'moment';

import { Pagination } from "@shared/models/pagination"; 
import { Filter } from "@shared/models/filter";

@Component({
  selector: 'app-imposto',
  templateUrl: './imposto.component.html',
  styleUrls: ['./imposto.component.css']
})
export class ImpostoComponent implements OnInit {

  public pagination = new Pagination();
  public filter = new Filter();
  
  public imposto: any;
  private loading: boolean = false;

  private items: any = [];

  constructor(private http: HttpService,private excelService: ExcelService) { }

  ngOnInit() {
    this.getPageFilterData(1);
  }

  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "Lista_impostoss-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      return;
    }
    this.pagination.page = page;
    this.listarImpostos();
  }


  /**
   * @name "Listar Impostos"
   * @descriptio "Esta Função permite Listar todos impostos"
   * @author "caniggia.moreira@itgest.pt"
   * @param start 
   * @param end 
   */
  private listarImpostos() {

    this.loading = true
    this.http.__call('imposto/listar', {filter: this.filter, pagination:this.pagination}).subscribe(
      response => {

        this.pagination.lastPage = Object(response).data.lastPage;
        this.pagination.page = Object(response).data.page;
        this.pagination.total = Object(response).data.total;
        this.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.loading = false;

      }
    );
  }

  private setDataImposto(imposto) {
    this.imposto = imposto
  }


}

