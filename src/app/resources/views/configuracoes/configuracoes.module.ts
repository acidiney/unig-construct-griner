import { EquipamentosModule } from "./equipamentos/equipamentos.module";

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatRadioModule,
  DateAdapter,
  MAT_DATE_FORMATS,
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatSelectModule,
  MatOptionModule,
} from "@angular/material";
import {
  FormatDateAdapter,
  FORMAT_DATE_FORMATS,
} from "@shared/directives/date-adapter";
import { NgxLoadingModule } from "ngx-loading";
import { NgxPaginationModule } from "ngx-pagination";
import { NgxImageCompressService } from "ngx-image-compress";
import { ExcelService } from "@shared/services/excel.service";

import { TipoRecursHumanoRoutingModule } from "./tipo-recurso-humano/tipoRecursHumano-routing.module";

import { ConfigsRoutingModule } from "./configs-routing.module";

import { TipoRecursoHumanoListarComponent } from "./tipo-recurso-humano/tipo-recurso-humano-listar/tipo-recurso-humano-listar.component";
import { TipoRecursoHumanoRegistarComponent } from "./tipo-recurso-humano/tipo-recurso-humano-registar/tipo-recurso-humano-registar.component";
import { EmpresaComponent } from "./empresa/empresa.component";
import { ImpostoComponent } from "./imposto/imposto.component";
import { CreateOrEditImpostoComponent } from "./imposto/create-or-edit-imposto/create-or-edit-imposto.component";
import { MoedaComponent } from "./moeda/moeda.component";
import { BancoComponent } from "./banco/banco.component";
@NgModule({
  declarations: [
    TipoRecursoHumanoListarComponent,
    TipoRecursoHumanoRegistarComponent,
    EmpresaComponent,
    ImpostoComponent,
    CreateOrEditImpostoComponent,
    MoedaComponent,
    BancoComponent,
  ],
  imports: [
    CommonModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxLoadingModule.forRoot({
      backdropBorderRadius: "3px",
      backdropBackgroundColour: "rgba(255, 255, 255, 0.78)",
      primaryColour: "#20a8d8",
      secondaryColour: "#20a8d8",
      tertiaryColour: "#20a8d8",
    }),
    TipoRecursHumanoRoutingModule,
    ConfigsRoutingModule,
    EquipamentosModule,
  ],
  providers: [
    {
      provide: DateAdapter,
      useClass: FormatDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: FORMAT_DATE_FORMATS,
    },
    NgxImageCompressService,
    ExcelService,
  ],
})
export class ConfiguracoesModule {}
