import { Component } from "@angular/core";
import { EquipmentStatus } from "@shared/interfaces/EquipmentInterface";
import { Filter } from "@shared/models/filter";
import { Pagination } from "@shared/models/pagination";
import { StoreService } from "@shared/services/store/store.service";

@Component({
  selector: 'app-listar-equipamentos',
  templateUrl: './listar-equipamentos.component.html'
})
export class ListarEquipamentosComponent {
  public pagination = new Pagination();
  public filter = new Filter();

  constructor (private $store: StoreService) {}

  exportAsXLSX(): void {}
  exportAsPDF(): void {}
  imprimirPDF(): void {}

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      return;
    }
    this.pagination.page = page;
  }

  get equipments () {
    return this.$store.equipment.findAll()
      .map((equipment) => ({
        ...equipment,
        equipmentType: this.$store.equipmentType.find(equipment.equipmentTypeId),
        equipmentModel: this.$store.equipmentModel.find(equipment.equipmentModelId),
        brand: this.$store.brand.find(equipment.brandId),
        statusName: this.$store.equipment.translateStatus(equipment.status)
      }))
  }

  get broken () {
    return EquipmentStatus.broken
  }

  deleteEquipment (equipmentId) {
    this.$store.equipment.remove(equipmentId)
  }


}
