import { FormGroup } from "@angular/forms";
import { Component, OnInit, ViewChild } from "@angular/core";
import {
  EquipmentInterface,
  EquipmentStatus,
} from "@shared/interfaces/EquipmentInterface";
import { StoreService } from "@shared/services/store/store.service";

@Component({
  selector: "app-create-equipment-modal",
  templateUrl: "./create-equipment.component.html",
})
export class CreateEquipmentModalComponent {
  equipment: EquipmentInterface;

  @ViewChild("closeBtn", null) private closeBtn: any;

  constructor(private $store: StoreService) {
    this.equipment = this.resetEquipment();
  }

  get brands() {
    return this.$store.brand.findAll();
  }

  get equipmentTypes() {
    return this.$store.equipmentType.findAll();
  }

  get equipmentModels() {
    return this.$store.equipmentModel.findAll();
  }

  private resetEquipment(): EquipmentInterface {
    return {
      brandId: null,
      description: "",
      equipmentModelId: null,
      equipmentTypeId: null,
      isDeleted: false,
      serieNumber: "",
      status: EquipmentStatus.available,
      image: "",
    };
  }

  createEquipment() {
    this.$store.equipment.create(this.equipment);
    this.equipment = this.resetEquipment();

    const closeBtn = document.querySelector(".btn-close") as any;
    closeBtn.click();
  }
}
