import { Component } from "@angular/core";
import {
  EquipmentTypeInterface,
} from "@shared/interfaces/EquipmentInterface";
import { StoreService } from "@shared/services/store/store.service";

@Component({
  selector: "app-create-equipment-type-modal",
  templateUrl: "./create-equipment.component.html",
})
export class CreateEquipmentTypeModalComponent {
  equipmentType: EquipmentTypeInterface;

  constructor(private $store: StoreService) {
    this.equipmentType = this.resetEquipment();
  }

  private resetEquipment(): EquipmentTypeInterface {
    return {
      designation: '',
      isDeleted: false
    };
  }

  createEquipment() {
    this.$store.equipmentType.create(this.equipmentType);
    this.equipmentType = this.resetEquipment();

    const closeBtn = document.querySelector(".btn-close") as any;
    closeBtn.click();
  }
}
