import { Component } from "@angular/core";
import { Filter } from "@shared/models/filter";
import { Pagination } from "@shared/models/pagination";
import { StoreService } from "@shared/services/store/store.service";

@Component({
  selector: 'app-listar-equipamentos-model',
  templateUrl: './listar-equipamentos-type.component.html'
})
export class ListarEquipamentosTypeComponent {
  public pagination = new Pagination();
  public filter = new Filter();

  constructor (private $store: StoreService) {}

  exportAsXLSX(): void {}
  exportAsPDF(): void {}
  imprimirPDF(): void {}

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      return;
    }
    this.pagination.page = page;
  }

  get equipmentTypes () {
    return this.$store.equipmentType.findAll()
  }

  deleteEquipmentModel (equipmentId) {
    this.$store.equipmentType.remove(equipmentId)
  }

}
