import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@core/guards/auth.guard';
import { ListarEquipamentosModelComponent } from './listar-equipamentos-model/listar-equipamentos-model.component';
import { ListarEquipamentosTypeComponent } from './listar-equipamentos-type/listar-equipamentos-type.component';
import { ListarEquipamentosComponent } from './listar-equipamentos/listar-equipamentos.component';

 const routes: Routes = [
  {
    path: '',
    component: ListarEquipamentosComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Equipamentos'
    }
  },
  {
    path: 'modelos',
    component: ListarEquipamentosModelComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Modelos de equipamento'
    }
  },
  {
    path: 'tipos',
    component: ListarEquipamentosTypeComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Tipos de equipamento'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EquipamentosRoutingModule {}
