import { CreateEquipmentTypeModalComponent } from './listar-equipamentos-type/create-equipment-type-modal/create-equipment-modal.component';
import { CreateEquipmentModelModalComponent } from './listar-equipamentos-model/create-equipment-model-modal/create-equipment-modal.component';
import { CommonModule } from '@angular/common';
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FormateDatePipe } from 'app/formate-date.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { EquipamentosRoutingModule } from "./equipamentos-routing.module";
import { ListarEquipamentosModelComponent } from './listar-equipamentos-model/listar-equipamentos-model.component';
import { ListarEquipamentosTypeComponent } from './listar-equipamentos-type/listar-equipamentos-type.component';
import { CreateEquipmentModalComponent } from './listar-equipamentos/create-equipment-modal/create-equipment-modal.component';
import { ListarEquipamentosComponent } from "./listar-equipamentos/listar-equipamentos.component";

@NgModule({
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    NgxPaginationModule,
    EquipamentosRoutingModule,
  ],
  declarations: [
    ListarEquipamentosComponent,
    CreateEquipmentModalComponent,
    ListarEquipamentosModelComponent,
    ListarEquipamentosTypeComponent,
    CreateEquipmentModelModalComponent,
    CreateEquipmentTypeModalComponent,
    FormateDatePipe
  ]
})
export class EquipamentosModule {}
