import { Component } from "@angular/core";
import {
  EquipmentModelInterface,
} from "@shared/interfaces/EquipmentInterface";
import { StoreService } from "@shared/services/store/store.service";

@Component({
  selector: "app-create-equipment-model-modal",
  templateUrl: "./create-equipment.component.html",
})
export class CreateEquipmentModelModalComponent {
  equipmentModel: EquipmentModelInterface;

  constructor(private $store: StoreService) {
    this.equipmentModel = this.resetEquipment();
  }

  get brands() {
    return this.$store.brand.findAll();
  }

  private resetEquipment(): EquipmentModelInterface {
    return {
      designation: '',
      brandId: null,
      isDeleted: false
    };
  }

  createEquipment() {
    this.$store.equipmentModel.create(this.equipmentModel);
    this.equipmentModel = this.resetEquipment();

    const closeBtn = document.querySelector(".btn-close") as any;
    closeBtn.click();
  }
}
