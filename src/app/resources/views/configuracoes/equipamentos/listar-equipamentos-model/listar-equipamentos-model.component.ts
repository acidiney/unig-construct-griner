import { Component } from "@angular/core";
import { EquipmentStatus } from "@shared/interfaces/EquipmentInterface";
import { Filter } from "@shared/models/filter";
import { Pagination } from "@shared/models/pagination";
import { StoreService } from "@shared/services/store/store.service";

@Component({
  selector: 'app-listar-equipamentos-model',
  templateUrl: './listar-equipamentos-model.component.html'
})
export class ListarEquipamentosModelComponent {
  public pagination = new Pagination();
  public filter = new Filter();

  constructor (private $store: StoreService) {}

  exportAsXLSX(): void {}
  exportAsPDF(): void {}
  imprimirPDF(): void {}

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      return;
    }
    this.pagination.page = page;
  }

  get equipmentModels () {
    return this.$store.equipmentModel.findAll()
      .map(eqModel => ({
        ...eqModel,
        brand: this.$store.brand.find(eqModel.brandId)
      }))
  }

  deleteEquipmentModel (equipmentId) {
    this.$store.equipmentModel.remove(equipmentId)
  }

}
