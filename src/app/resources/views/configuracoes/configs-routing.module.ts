import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@core/guards/auth.guard';
import { BancoComponent } from './banco/banco.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { ImpostoComponent } from './imposto/imposto.component';
import { MoedaComponent } from './moeda/moeda.component';

 const routes: Routes = [
  {
    path: 'empresa',
    component: EmpresaComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Banco',
      layout:{
        customLayout: true,
        layoutNavigationTop: false,
      }
    }
  },
  {
    path: 'equipamentos',
    data: {
      title: 'Equipamentos',
    },
    loadChildren: () => import('./equipamentos/equipamentos.module').then(m => m.EquipamentosModule),
  },
  {
    path: 'bancos',
    component: BancoComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Bancos',
      layout:{
        customLayout: true,
        layoutNavigationTop: false,
      }
    }
  },
  {
    path: 'impostos',
    component: ImpostoComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Impostos',
      layout:{
        customLayout: true,
        layoutNavigationTop: false,
      }
    }
  },
  {
    path: 'moedas',
    component: MoedaComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Moedas',
      layout:{
        customLayout: true,
        layoutNavigationTop: false,
      }
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigsRoutingModule {}
