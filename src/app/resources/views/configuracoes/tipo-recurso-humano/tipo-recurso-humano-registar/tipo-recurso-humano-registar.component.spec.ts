import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoRecursoHumanoRegistarComponent } from './tipo-recurso-humano-registar.component';

describe('TipoRecursoHumanoRegistarComponent', () => {
  let component: TipoRecursoHumanoRegistarComponent;
  let fixture: ComponentFixture<TipoRecursoHumanoRegistarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoRecursoHumanoRegistarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoRecursoHumanoRegistarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
