import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';  
import { AuthGuard } from '@core/guards/auth.guard';
import { TipoRecursoHumanoListarComponent } from './tipo-recurso-humano-listar/tipo-recurso-humano-listar.component';
import { TipoRecursoHumanoRegistarComponent } from './tipo-recurso-humano-registar/tipo-recurso-humano-registar.component';
const routes: Routes = [

  {
    path: 'recursos', 
    data: {
      title: 'Tipos Recursos Humanos',
    },
    children: [
      {
        path: 'listar',
        component: TipoRecursoHumanoListarComponent, 
        canActivate: [AuthGuard],
        data: {
          title: 'Listar Tipos Recursos Humanos',
          layout:{
            customLayout: true, 
            layoutNavigationTop: false, 
          }
        }
      },{
        path: 'registar-tipo-recurso-humano',
        component: TipoRecursoHumanoRegistarComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Registar Tipo Recurso Humano',
          layout:{
            customLayout: true, 
            layoutNavigationTop: false, 
          }
        }
      } 
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipoRecursHumanoRoutingModule {}
