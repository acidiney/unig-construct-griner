import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoRecursoHumanoListarComponent } from './tipo-recurso-humano-listar.component';

describe('TipoRecursoHumanoListarComponent', () => {
  let component: TipoRecursoHumanoListarComponent;
  let fixture: ComponentFixture<TipoRecursoHumanoListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoRecursoHumanoListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoRecursoHumanoListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
