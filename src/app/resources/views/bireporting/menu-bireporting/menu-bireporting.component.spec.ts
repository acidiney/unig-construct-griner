import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuBIReportingComponent } from './menu-bireporting.component';

describe('MenuBIReportingComponent', () => {
  let component: MenuBIReportingComponent;
  let fixture: ComponentFixture<MenuBIReportingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuBIReportingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuBIReportingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
