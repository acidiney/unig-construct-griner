import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuBIReportingComponent } from './menu-bireporting/menu-bireporting.component';
import { AuthGuard } from '@core/guards/auth.guard';
const routes: Routes = [
  {
    path: 'operations',
    component: MenuBIReportingComponent,
    //canActivate: [AuthGuard],
    data: {
      title: 'Operaçoes',
      layout:{
        customLayout: true, 
        layoutNavigationTop: false, 
      }
    }
  } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BIReportingRoutingModule {}
