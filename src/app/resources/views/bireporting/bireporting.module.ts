import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BIReportingRoutingModule } from './bireporting-routing.module';
import { MenuBIReportingComponent } from './menu-bireporting/menu-bireporting.component';

@NgModule({
  declarations: [MenuBIReportingComponent],
  imports: [
    CommonModule,
    BIReportingRoutingModule
  ]
})
export class BIReportingModule { }
