import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import {MatRadioModule, DateAdapter, MAT_DATE_FORMATS, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatIconModule, MatButtonModule, MatSelectModule, MatOptionModule } from "@angular/material";
import { FormatDateAdapter, FORMAT_DATE_FORMATS } from "@shared/directives/date-adapter";
import { NgxLoadingModule } from 'ngx-loading'; 

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { LoginComponent } from './login/login.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';


@NgModule({
  imports: [ 
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule, 
    MatSelectModule,
    MatOptionModule,
    MatButtonModule, 
    FormsModule,ReactiveFormsModule, 
    NgxLoadingModule.forRoot({
      backdropBorderRadius: '3px',
      backdropBackgroundColour: 'rgba(255, 255, 255, 0.78)',
      primaryColour: '#20a8d8',
      secondaryColour: '#20a8d8',
      tertiaryColour: '#20a8d8',
    }),
    AuthenticationRoutingModule, 
  ],
  declarations: [LoginComponent, ResetPasswordComponent],
  providers: [ 
    {
      provide: DateAdapter,
      useClass: FormatDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: FORMAT_DATE_FORMATS,
    }
  ],
})
export class AuthenticationModule { }
