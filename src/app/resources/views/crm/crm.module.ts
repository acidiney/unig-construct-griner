import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 

import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import {MatRadioModule, DateAdapter, MAT_DATE_FORMATS, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatIconModule, MatButtonModule, MatSelectModule, MatOptionModule } from "@angular/material";
import { FormatDateAdapter, FORMAT_DATE_FORMATS } from "@shared/directives/date-adapter";
import { NgxLoadingModule } from 'ngx-loading'; 
import { NgxPaginationModule } from "ngx-pagination";
import { ExcelService } from '@shared/services/excel.service';
import { ArchwizardModule } from "angular-archwizard";

import { ClienteRoutingModule } from './clientes/cliente-routing.module';
import { OrcamentoRoutingModule } from './orcamentos/orcamento-routing.module';


import { ClienteListarComponent } from './clientes/cliente-listar/cliente-listar.component';
import { ClienteCreateOrEditComponent } from './clientes/cliente-create-or-edit/cliente-create-or-edit.component';

import { ClienteIncumprimentosComponent } from './clientes/cliente-incumprimentos/cliente-incumprimentos.component';
import { ClienteContaCorrenteComponent } from './clientes/cliente-conta-corrente/cliente-conta-corrente.component';


import { OrcamentoListarComponent } from './orcamentos/orcamento-listar/orcamento-listar.component';
import { OrcamentoCreateComponent } from './orcamentos/orcamento-create/orcamento-create.component';
import { OrcamentoDashboardComponent } from './orcamentos/orcamento-dashboard/orcamento-dashboard.component';


@NgModule({
  declarations: [
    ClienteListarComponent, 
    ClienteCreateOrEditComponent, 
    ClienteIncumprimentosComponent, 
    ClienteContaCorrenteComponent, 
    OrcamentoListarComponent, 
    OrcamentoCreateComponent, 
    OrcamentoDashboardComponent
  ],
  imports: [
    CommonModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule, 
    MatSelectModule,
    MatOptionModule,
    MatButtonModule, 
    FormsModule,ReactiveFormsModule, 
    NgxPaginationModule,
    NgxLoadingModule.forRoot({
      backdropBorderRadius: '3px',
      backdropBackgroundColour: 'rgba(255, 255, 255, 0.78)',
      primaryColour: '#20a8d8',
      secondaryColour: '#20a8d8',
      tertiaryColour: '#20a8d8',
    }),
    ArchwizardModule, 
    OrcamentoRoutingModule,
    ClienteRoutingModule
  ],providers: [ 
    {
      provide: DateAdapter,
      useClass: FormatDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: FORMAT_DATE_FORMATS,
    },
    ExcelService,
  ],
})
export class CRMModule { } 
