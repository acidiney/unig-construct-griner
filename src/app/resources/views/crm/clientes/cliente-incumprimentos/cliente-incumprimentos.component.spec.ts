import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteIncumprimentosComponent } from './cliente-incumprimentos.component';

describe('ClienteIncumprimentosComponent', () => {
  let component: ClienteIncumprimentosComponent;
  let fixture: ComponentFixture<ClienteIncumprimentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteIncumprimentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteIncumprimentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
