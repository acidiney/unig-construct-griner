import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteListarComponent } from './cliente-listar/cliente-listar.component';
 import { ClienteIncumprimentosComponent } from './cliente-incumprimentos/cliente-incumprimentos.component';
import { ClienteContaCorrenteComponent } from './cliente-conta-corrente/cliente-conta-corrente.component';
import { AuthGuard } from '@core/guards/auth.guard';
const routes: Routes = [
  {
    path: 'clientes', 
    data: {
      title: 'Clientes',
    },
    children: [
  {
    path: 'listar-clientes',
    component: ClienteListarComponent,
    //canActivate: [AuthGuard],
    data: {
      title: 'Listar Clientes',
      layout:{
        customLayout: true, 
        layoutNavigationTop: false, 
      }
    }
  },{
    path: 'cc',
    component: ClienteContaCorrenteComponent,
    //canActivate: [AuthGuard],
    data: {
      title: 'Conta Correntes',
      layout:{
        customLayout: true, 
        layoutNavigationTop: false, 
      }
    }
    
  },{
    path: 'incumprimentos',
    component: ClienteIncumprimentosComponent,
    //canActivate: [AuthGuard],
    data: {
      title: 'Incumprimentos',
      layout:{
        customLayout: true, 
        layoutNavigationTop: false, 
      }
    }
  }]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule {}
