import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteContaCorrenteComponent } from './cliente-conta-corrente.component';

describe('ClienteContaCorrenteComponent', () => {
  let component: ClienteContaCorrenteComponent;
  let fixture: ComponentFixture<ClienteContaCorrenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteContaCorrenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteContaCorrenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
