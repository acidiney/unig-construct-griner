import { Component, OnInit } from "@angular/core";
import { ClienteService } from "@shared/services/crm/cliente.service";
import { Pagination } from "@shared/models/pagination";
import { Filter } from "@shared/models/filter";
import { HelperFunctionsService } from '@shared/services/helper-functions.service';
import { Subject } from 'rxjs';
import { debounceTime, finalize } from 'rxjs/operators';
import { Observable } from "rxjs/Rx";
import { HttpParams } from '@angular/common/http';
import { StoreService } from "@shared/services/store/store.service";


@Component({
  selector: 'app-cliente-listar',
  templateUrl: './cliente-listar.component.html',
  styleUrls: ['./cliente-listar.component.css']
})
export class ClienteListarComponent implements OnInit {

  public pagination = new Pagination();
  public filter = new Filter();

  private clientes:any = [];

  loading:boolean = true;
  observableObj: Observable<any>;
  subjectObj = new Subject<number>();
 private typesClient:any = []

  constructor(private client: ClienteService, private $store: StoreService) {}

  ngOnInit() {
    this.client.findAllTypesClient();
    this.filter.orderBy = "created_at";
    this.subjectObj.pipe(debounceTime(1000)).subscribe({
      next: () => this.listarClientes(),
    });
    this.subjectObj.next(1);
  }



  /**
   * @name "Listar facturação"
   * @descriptio "Esta Função permite Listar todas facturações"
   * @author "caniggia.moreira@itgest.pt"
   * @param start
   * @param end
   */
  private listarClientes() {
    this.loading = true;
    setTimeout(() => {
      this.clientes = this.$store.client.findAll()
      this.loading = false
    }, 1000)
  }

  //--------------------------------------------------------------------------

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      return;
    }
    this.pagination.page = page;
    this.subjectObj.next(this.pagination.page);
  }


}
