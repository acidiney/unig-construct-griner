import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteCreateOrEditComponent } from './cliente-create-or-edit.component';

describe('ClienteCreateOrEditComponent', () => {
  let component: ClienteCreateOrEditComponent;
  let fixture: ComponentFixture<ClienteCreateOrEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteCreateOrEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteCreateOrEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
