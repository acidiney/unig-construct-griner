 import { Component, OnInit, Input,Output, EventEmitter,OnChanges, SimpleChange } from '@angular/core';
import { HttpService } from '@core/providers/http/http.service'; 
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'clientCreateOrEdit',
  templateUrl: './cliente-create-or-edit.component.html',
  styleUrls: ['./cliente-create-or-edit.component.css']
})
export class ClienteCreateOrEditComponent implements OnInit, OnChanges {

  @Input() modalClienteCreateOrEdit: string = "modalClienteCreateOrEdit";
  @Input() title: string = "Registar Cliente";
  @Input() cliente: any;

  submitted = false;
  private loading: boolean = false;
  @Input() simpleFormCliente: FormGroup;
  @Input() selectForms: any;

  @Output() private loadListClient = new EventEmitter<any>();

  constructor(private http: HttpService, private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit() { 
  }

  createForm() {
    this.simpleFormCliente = this.formBuilder.group({
      index: [{ value: null, disabled: true }],
      nome: [null, Validators.required],
      contribuente: [null, Validators.required],
      email: [null, Validators.required],
      tipo_identidade_id: [null, Validators.required],
      tipo_cliente_id: [null, Validators.required],
      genero: [null],
      telefone: [null, Validators.required],
      province: [null, Validators.required],
      morada: [null, Validators.required],
      direccao_id: [null, Validators.required],
      direccao: [null],
      observacao: [null],
      gestor_conta: [null, Validators.required],
      gestor_id: [null, Validators.required],
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.simpleFormCliente.controls;
  }

  onReset() {
    this.submitted = false;
    this.simpleFormCliente.reset();
  }


  onSubmit() {

    this.submitted = true;

    // parar aquei se o formulário for inválido
    if (this.simpleFormCliente.invalid) {
      return;
    }
    this.loading = true;
    const index = this.simpleFormCliente.getRawValue().index;
    // TODO: usado para fazer a requisição com a api de criação de objsct or update
    const uri = (index === null ? 'cliente/register' : 'cliente/update/' + index);
    this.createOrEdit(uri, this.simpleFormCliente, (index === null ? true : false));

  }

  createOrEdit(uri: any, formulario: FormGroup, isCreate: boolean) {

    // TODO: usado para fazer a requisição com a api de criação de object
    this.http.__call(uri, formulario.value).pipe(first()).subscribe(
      response => {
        this.submitted = false;
        this.loading = false;
        if (isCreate) {
          if (Object(response).code === 200) { 
            formulario.reset(); 
          }
        }
        if (Object(response).code ==200) { 
          this.loadListClients(Object(response).data);
        }          
      },
      error => {
        this.submitted = false;
        this.loading = false;
      });
  }

  private gestores: any = [];
  view_gest = false;

  private getGestor() {
    this.view_gest = true;
    this.http.__call('cliente/search-gestor', { start: 1, end: 10, search: this.simpleFormCliente.getRawValue().gestor_conta }).subscribe(
      response => {
        this.gestores = Object(response).data.data; 
      }
    );
  }

  private setGestor(gestor: any) {
    this.view_gest = false;
    this.simpleFormCliente.patchValue({
        gestor_id: gestor.id,
        gestor_conta: gestor.nome 
    });
  }


  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {

    if (this.cliente !== undefined ) {
      this.title = "Editar Cliente";
      this.simpleFormCliente.patchValue({
        index: this.cliente.id,
        nome: this.cliente.nome,
        contribuente: this.cliente.contribuente,
        email: this.cliente.email,
        tipo_identidade_id: this.cliente.tipo_identidade_id,
        tipo_cliente_id: this.cliente.tipo_cliente_id,
        genero: this.cliente.genero,
        telefone: this.cliente.telefone,
        province: this.cliente.province,
        morada: this.cliente.morada,
        gestor_conta: this.cliente.gestor,
        direccao_id:  this.cliente.direccao_id,
        observacao: this.cliente.observacao,
        gestor_id: this.cliente.gestor_id
      });
    } else {
      this.title = "Registar Cliente";
    }
  }

  

  public loadListClients(client) { 
    this.loadListClient.emit(client);
  }


}
