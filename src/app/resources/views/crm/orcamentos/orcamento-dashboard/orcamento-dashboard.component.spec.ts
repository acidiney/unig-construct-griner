import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrcamentoDashboardComponent } from './orcamento-dashboard.component';

describe('OrcamentoDashboardComponent', () => {
  let component: OrcamentoDashboardComponent;
  let fixture: ComponentFixture<OrcamentoDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrcamentoDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrcamentoDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
