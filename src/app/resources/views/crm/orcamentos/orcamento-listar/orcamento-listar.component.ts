import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpService } from "@core/providers/http/http.service";
import { ConfigService } from "@shared/services/config.service";
import { ExcelService } from "@shared/services/excel.service";
import * as moment from "moment";
import { Pagination } from "@shared/models/pagination";
import { Filter } from "@shared/models/filter";
import { StoreService } from "@shared/services/store/store.service";
import { ProjectStatus } from "@shared/interfaces/ProjectInterface";
@Component({
  selector: "app-orcamento-listar",
  templateUrl: "./orcamento-listar.component.html",
  styleUrls: ["./orcamento-listar.component.css"],
})
@Component({
  selector: "app-orcamento-listar",
  templateUrl: "./orcamento-listar.component.html",
  styleUrls: ["./orcamento-listar.component.css"],
})
export class OrcamentoListarComponent implements OnInit {
  private orcamentos: any = [];
  private loading: boolean = false;
  public pagination = new Pagination();
  public filter = new Filter();

  constructor(
    private http: HttpService,
    private configService: ConfigService,
    private excelService: ExcelService,
    private $store: StoreService
  ) {
    this.getPageFilterData(1);
  }

  public chooseBadgeColor(status) {
    const badge = {
      [ProjectStatus.done]: "badge-primary",
      [ProjectStatus.paid]: "badge-success",
      [ProjectStatus.started]: "badge-warning",
      [ProjectStatus.pending]: "badge-info",
      [ProjectStatus.rejected]: "badge-danger",
    };

    return badge[status];
  }

  public chooseBadgeLabel(status) {
    const badge = {
      [ProjectStatus.done]: "Concluído",
      [ProjectStatus.paid]: "Pago",
      [ProjectStatus.started]: "Em execução",
      [ProjectStatus.pending]: "Pendente",
      [ProjectStatus.rejected]: "Rejeitado",
    };

    return badge[status];
  }

  ngOnInit() {}
  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile =
      "Lista_orcamentos-" +
      moment(CurrentDate).format("DD") +
      "-" +
      moment(CurrentDate).format("MM") +
      "-" +
      moment(CurrentDate).format("YYYY") +
      " " +
      moment(CurrentDate).format("H") +
      ":" +
      moment(CurrentDate).format("m");
    //this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSX")[0], nameFile);
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

  /**
   * @name "Listar facturação"
   * @descriptio "Esta Função permite Listar todas facturações"
   * @author "caniggia.moreira@itgest.pt"
   * @param start
   * @param end
   */
  private listarOrcamentos() {
    // this.loading = true;

    // this.http
    //   .__call("orcamento/list", {
    //     filter: this.filter,
    //     pagination: this.pagination,
    //   })
    //   .subscribe((response) => {
    //     this.pagination.lastPage = Object(response).data.lastPage;
    //     this.pagination.page = Object(response).data.page;
    //     this.pagination.total = Object(response).data.total;
    //     this.pagination.perPage = Object(response).data.perPage;
    //     this.orcamentos = Object(response).data.data;
    //     this.loading = false;
    //   });
  }

  //--------------------------------------------------------------------------

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      return;
    }
    this.pagination.page = page;
    this.listarOrcamentos();
  }

  private btnImprimirFactura(id) {
    this.configService.imprimirFactura(id, "2ª Via");
  }

  public get budgets() {
    return this.$store.budget.findAll().map((budget) => {
      return {
        ...budget,
        project: this.$store.project.find(budget.projectId),
      };
    });
  }

  public formatDate(date) {
    return this.configService.formatDate(date);
  }

  public formatCurrency(badget = 0): string {
    return this.configService.numberFormat(badget);
  }

  public totalStage(projectId): number {
    return this.$store.budget.findAll(projectId).reduce((curr, next) => {
      return curr + next.totalPrice;
    }, 0);
  }
}
