import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { OrcamentoListarComponent } from "./orcamento-listar/orcamento-listar.component";
import { OrcamentoCreateComponent } from "./orcamento-create/orcamento-create.component";

import { AuthGuard } from "@core/guards/auth.guard";

const routes: Routes = [
  {
    path: "orcamentos",
    data: {
      title: "Orçamentos",
    },
    children: [
      {
        path: "listar-orcamentos",
        component: OrcamentoListarComponent,
        canActivate: [AuthGuard],
        data: {
          title: "Listar Orçamento",
          layout: {
            customLayout: true,
            layoutNavigationTop: false,
          },
        },
      },
      {
        path: "registar",
        component: OrcamentoCreateComponent,
        canActivate: [AuthGuard],
        data: {
          title: "Registar Orçamento",
          layout: {
            customLayout: true,
            layoutNavigationTop: false,
          },
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrcamentoRoutingModule {}
