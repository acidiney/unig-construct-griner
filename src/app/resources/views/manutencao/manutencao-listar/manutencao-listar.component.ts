import { Component, OnInit } from "@angular/core";
import { HttpService } from "@core/providers/http/http.service";
import {
  LevelMaintenance,
  MaintenanceInterface,
  StatusMaintenance,
} from "@shared/interfaces/MaintenanceInterface";
import { Filter } from "@shared/models/filter";
import { Pagination } from "@shared/models/pagination";
import { ConfigService } from "@shared/services/config.service";
import { StoreService } from "@shared/services/store/store.service";

declare const $: any;
@Component({
  selector: "app-manutencao-listar",
  templateUrl: "./manutencao-listar.component.html",
  styles: [
    `
      table td {
        padding: unset 10px;
        vertical-align: middle;
      }
    `,
    `
      .progress {
        border-radius: 4px;
      }
    `,
  ],
})
export class ManutencaoListarComponent implements OnInit {
  public pagination = new Pagination();
  public filter = new Filter();

  constructor(
    public http: HttpService,
    private configService: ConfigService,
    private $store: StoreService
  ) {
    this.getPageFilterData(1);
  }

  public chooseStatusBadgeColor(status: StatusMaintenance): string {
    const badge = {
      [StatusMaintenance.feedback]: "badge-primary",
      [StatusMaintenance.done]: "badge-success",
      [StatusMaintenance.doing]: "badge-warning",
      [StatusMaintenance.pending]: "badge-info",
      [StatusMaintenance.closed]: "badge-secondary",
    };

    return badge[status];
  }

  public chooseLevelBadgeColor(level: LevelMaintenance): string {
    const badge = {
      [LevelMaintenance.normal]: "badge-primary",
      [LevelMaintenance.low]: "badge-secondary",
      [LevelMaintenance.high]: "badge-warning",
      [LevelMaintenance.urgent]: "badge-danger",
    };

    return badge[level];
  }

  public chooseStatusBadgeLabel(status: StatusMaintenance): string {
    const badge = {
      [StatusMaintenance.done]: "Concluído",
      [StatusMaintenance.feedback]: "Esperando feedback",
      [StatusMaintenance.doing]: "Em execução",
      [StatusMaintenance.pending]: "Pendente",
      [StatusMaintenance.closed]: "Fechado",
    };

    return badge[status];
  }

  public chooseLevelBadgeLabel(status: LevelMaintenance): string {
    const badge = {
      [LevelMaintenance.low]: "Baixo",
      [LevelMaintenance.normal]: "Normal",
      [LevelMaintenance.high]: "Alto",
      [LevelMaintenance.urgent]: "Urgente",
    };

    return badge[status];
  }

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();
  }
  exportAsXLSX(): void {}
  exportAsPDF(): void {}
  imprimirPDF(): void {}

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      return;
    }
    this.pagination.page = page;
  }

  public get maintenances() {
    const calculateProgressForClosedSubMaintenances = (
      maintenance: MaintenanceInterface
    ) => {
      const closedSubMaintenances = maintenance.subMaintenances.reduce(
        (curr, next) => {
          if (next.status === StatusMaintenance.done) {
            return curr + 1;
          }
          return curr;
        },
        0
      );

      return this.calculateProgress(
        maintenance.subMaintenances.length,
        closedSubMaintenances
      );
    };

    return this.$store.maintenance.findAll().map((maintenance) => ({
      ...maintenance,
      progress: calculateProgressForClosedSubMaintenances(maintenance),
    }));
  }

  public formatDate(date) {
    return this.configService.formatDate(date);
  }

  public calculateProgress(subMaintenancesLength, closedMaintenances) {
    return Math.floor((closedMaintenances * 100) / subMaintenancesLength);
  }

  public progressColorInterval(progress): string {
    if (progress > 50 && progress <= 80) return "bg-info";
    if (progress > 20 && progress <= 50) return "bg-warning";
    if (progress > 0 && progress <= 20) return "bg-danger";
    return "bg-success";
  }
}
