import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LevelMaintenance, MaintenanceInterface, MaintenanceType, StatusMaintenance } from "@shared/interfaces/MaintenanceInterface";
import { ConfigService } from "@shared/services/config.service";
import { StoreService } from "@shared/services/store/store.service";

@Component({
  selector: "app-manutencao-mostrar",
  templateUrl: `./manutencao-mostrar.component.html`,
  styles: [
    `.pre {
      white-space: pre;
    }`
  ]
})
export class ManutencaoMostrarComponent {
  constructor(private $store: StoreService,
    private $route: Router,
    private configService: ConfigService,
    private $activeRoute: ActivatedRoute) {}

  get maintenanceId(): string {
    return this.$activeRoute.snapshot.params.maintenanceId as string;
  }

  get maintenance(): MaintenanceInterface {
    const maintenance = this.$store.maintenance.find(this.maintenanceId);

    if (!maintenance) {
      this.$route.navigate(['/manutencao'])
    }

    return maintenance
  }

  public formatDate(date) {
    return this.configService.formatDate(date);
  }

  public chooseLevelBadgeLabel(status: LevelMaintenance): string {
    const badge = {
      [LevelMaintenance.low]: "Baixo",
      [LevelMaintenance.normal]: "Normal",
      [LevelMaintenance.high]: "Alto",
      [LevelMaintenance.urgent]: "Urgente",
    };

    return badge[status];
  }

  public chooseLevelBadgeColor(level: LevelMaintenance): string {
    const badge = {
      [LevelMaintenance.normal]: "badge-primary",
      [LevelMaintenance.low]: "badge-secondary",
      [LevelMaintenance.high]: "badge-warning",
      [LevelMaintenance.urgent]: "badge-danger",
    };

    return badge[level];
  }


  public chooseMaintenanceBadgeLabel(status: MaintenanceType): string {
    const badge = {
      [MaintenanceType.preventive]: "Preventiva",
      [MaintenanceType.corrective]: "Correctiva"
    };

    return badge[status];
  }

  public chooseStatusBadgeLabel(status: StatusMaintenance): string {
    const badge = {
      [StatusMaintenance.done]: "Concluído",
      [StatusMaintenance.feedback]: "Esperando feedback",
      [StatusMaintenance.doing]: "Em execução",
      [StatusMaintenance.pending]: "Pendente",
      [StatusMaintenance.closed]: "Fechado",
    };

    return badge[status];
  }

  public chooseStatusBadgeColor(status: StatusMaintenance): string {
    const badge = {
      [StatusMaintenance.feedback]: "badge-primary",
      [StatusMaintenance.done]: "badge-success",
      [StatusMaintenance.doing]: "badge-warning",
      [StatusMaintenance.pending]: "badge-info",
      [StatusMaintenance.closed]: "badge-secondary",
    };

    return badge[status];
  }
}
