import { ManutencaoEquipmentosComponent } from './manutencao-registrar/manutencao-equipments/manutencao-equipamentos.component';
import { ManutencaoRegistrarComponent } from './manutencao-registrar/manutencao-registrar.component';
import { ManutencaoMostrarComponent } from './manutencao-mostrar/manutencao-mostrar.component';
import {  NgxLoadingModule } from 'ngx-loading';
import { FormsModule } from '@angular/forms';
import { ExcelService } from './../../../shared/services/excel.service';
import { ManutencaoRoutingModule } from './manutencao-rounting.module';
import { ManutencaoListarComponent } from './manutencao-listar/manutencao-listar.component';
import { CommonModule } from '@angular/common';
import { NgModule } from "@angular/core";
import { NgxPaginationModule } from 'ngx-pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ManutencaoSidebar } from './manutencao-registrar/manutencao-sidebar/manutencao-sidebar.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgxLoadingModule.forRoot({
      backdropBorderRadius: '3px',
      backdropBackgroundColour: 'rgba(255, 255, 255, 0.78)',
      primaryColour: '#20a8d8',
      secondaryColour: '#20a8d8',
      tertiaryColour: '#20a8d8',
    }),
    ManutencaoRoutingModule
  ],
  declarations: [
    ManutencaoSidebar,
    ManutencaoEquipmentosComponent,
    ManutencaoListarComponent,
    ManutencaoMostrarComponent,
    ManutencaoRegistrarComponent
  ],
  providers: [
    ExcelService
  ]
})
export class ManutencaoModule {}
