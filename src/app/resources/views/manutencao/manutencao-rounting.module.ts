import { ManutencaoRegistrarComponent } from './manutencao-registrar/manutencao-registrar.component';
import { ManutencaoMostrarComponent } from './manutencao-mostrar/manutencao-mostrar.component';
import { ManutencaoListarComponent } from "./manutencao-listar/manutencao-listar.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthGuard } from "@core/guards/auth.guard";

const routes: Routes = [
  {
    path: "",
    canActivate: [AuthGuard],
    component: ManutencaoListarComponent,
  },
  {
    path: "registrar",
    canActivate: [AuthGuard],
    data: {
      title: 'Criar Manutenção'
    },
    component: ManutencaoRegistrarComponent,
  },
  {
    path: ":maintenanceId",
    canActivate: [AuthGuard],
    data: {
      title: 'Ver Manutenção'
    },
    component: ManutencaoMostrarComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManutencaoRoutingModule {}
