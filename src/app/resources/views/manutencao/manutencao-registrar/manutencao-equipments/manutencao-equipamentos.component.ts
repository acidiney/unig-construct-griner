import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import {
  LevelMaintenance,
  MaintenanceEquipmentInterface,
  MaintenanceType,
  StatusMaintenance,
} from "@shared/interfaces/MaintenanceInterface";
import { StoreService } from "@shared/services/store/store.service";
import { IDropdownSettings } from "ng-multiselect-dropdown";

interface DropdownPattern {
  id: string;
  description: string;
}

export interface EquipmentMaintenanceDto {
  equipment: Array<DropdownPattern>;
  type: Array<DropdownPattern>;
  assign: Array<DropdownPattern>;
  date: Date;
  recurrency: Array<DropdownPattern>;
}

@Component({
  selector: "app-manutencao-equipamentos",
  templateUrl: "./manutencao-equipamentos.component.html",
})
export class ManutencaoEquipmentosComponent {
  equipmentMaintenance: EquipmentMaintenanceDto;

  description: string;
  dropdownSettings: IDropdownSettings = {};

  @Input("maintenance") public maintenance: EquipmentMaintenanceDto[];
  @Input("officeId") private officeId: string;
  @Input("projectId") private projectId: string;

  @Output("remove-equipment")
  private removeEquipmentEvt = new EventEmitter<number>();
  @Output("add-equipment")
  private addEquipmentEvt = new EventEmitter<EquipmentMaintenanceDto>();

  constructor(private $store: StoreService, private $router: Router) {
    this.equipmentMaintenance = this.resetEquipmentMaintenance();
    this.dropdownSettings = {
      idField: "id",
      textField: "description",
      selectAllText: "Marcar Todos",
      unSelectAllText: "Desmarcar Todos",
      itemsShowLimit: 3,
      enableCheckAll: true,
      allowSearchFilter: true,
      singleSelection: true,
    };
  }

  get equipments() {
    if (!this.officeId) return [];

    const project = this.$store.project.find(this.projectId);
    return (
      project &&
      project.constructions
        .find((c) => c.id === this.officeId)
        .equipments.filter(
          (eq) =>
            !this.maintenance.find(
              (eqMaintenance) =>
                eqMaintenance.equipment[0] &&
                eqMaintenance.equipment[0].id === eq.id
            )
        )
    );
  }

  get multiSelectionSetting() {
    return {
      ...this.dropdownSettings,
      singleSelection: false,
    };
  }

  get maintenanceTypes() {
    return [
      {
        id: "preventive",
        description: "Preventiva",
      },
      {
        id: "corrective",
        description: "Correctiva",
      },
    ];
  }

  get recorrencies() {
    return [
      {
        id: "345678909876",
        description: "Uma vez",
      },
      {
        id: "345678909876a",
        description: "Semana",
      },
      {
        id: "345678909876d",
        description: "Mês",
      },
      {
        id: "345678909876cd",
        description: "Trimestre",
      },
      {
        id: "345678909876cdx",
        description: "Semestre",
      },
      {
        id: "345678909876cdxxo",
        description: "Anual",
      },
    ];
  }

  get works() {
    return [
      {
        id: "34567876km",
        description: "João Silva",
      },
      {
        id: "34509367876km",
        description: "Flávio Carpinteiro",
      },
    ];
  }

  resetEquipmentMaintenance(): EquipmentMaintenanceDto {
    return {
      assign: [],
      date: null,
      equipment: [],
      recurrency: [],
      type: [],
    };
  }

  removeEquipment(index: number) {
    this.removeEquipmentEvt.emit(index);
  }

  addEquipment() {
    this.addEquipmentEvt.emit(this.equipmentMaintenance);
    this.equipmentMaintenance = this.resetEquipmentMaintenance();
  }

  createAMaintenance() {
    const subMaintenances: MaintenanceEquipmentInterface[] = [];

    for (let main of this.maintenance) {
      main.equipment.forEach((eq) => {
        subMaintenances.push({
          equipment: this.$store.equipment.find(eq.id),
          assignedTo: main.assign.map(a => a.description).join(','),
          maintenanceType: MaintenanceType[main.type[0].id],
          startDate: main.date,
          status: StatusMaintenance.pending,
          recursiveInterval: main.recurrency[0].description
        });
      });
    }

    const project = this.$store.project.find(this.projectId);
    const construction = this.$store.project.findContruction(
      this.projectId,
      this.officeId
    );

    this.$store.maintenance.create({
      description: this.description,
      construction,
      project,
      level: LevelMaintenance.normal,
      status: StatusMaintenance.pending,
      startDate: null,
      createdBy: "Super Admin",
      subMaintenances,
    });

    const lastMaintenance = this.$store.maintenance.findAll()[this.$store.maintenance.findAll().length -1 ]

    const closeBtn = document.querySelector(".btn-close") as any;
    closeBtn.click()

    if (lastMaintenance) {
      this.$router.navigate(['/manutencao/' + lastMaintenance.id])
    }
  }
}
