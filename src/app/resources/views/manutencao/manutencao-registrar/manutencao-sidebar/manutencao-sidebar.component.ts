import { Component, Output, EventEmitter } from "@angular/core";
import { StoreService } from "@shared/services/store/store.service";
import { IDropdownSettings } from "ng-multiselect-dropdown";

export interface FilterEquipment {
  projectId: string,
  officeId: string
}

@Component({
  selector: "app-manutecao-registrar-sidebar",
  templateUrl: "./manutencao-sidebar.component.html",
})
export class ManutencaoSidebar {
  maintenanceType: Array<{id: null, description: null}> = [];
  selectedProject = [];
  selectedEquipments = [];
  officeId: string = null;
  dropdownSettings: IDropdownSettings = {};

  @Output('submit') private filterEquipmentsEvt = new EventEmitter<FilterEquipment>()

  constructor(private $store: StoreService) {
    this.dropdownSettings = {
      idField: "id",
      textField: "description",
      selectAllText: "Marcar Todos",
      unSelectAllText: "Desmarcar Todos",
      itemsShowLimit: 3,
      enableCheckAll: true,
      allowSearchFilter: true,
      singleSelection: true
    };
  }

  get maintenanceTypes () {
    return [
      {
        id: 'internal',
        description: 'Interno'
      },
      {
        id: 'external',
        description: 'Externo'
      }
    ]
  }

  get projects() {
    return this.$store.project.findAll();
  }

  get offices() {
    if (this.selectedProject.length) {
      return this.$store.project.find(this.selectedProject[0]["id"])
        .constructions;
    }

    if (!this.maintenanceType.length) {
      return [];
    }

    if (this.maintenanceType[0].id !== "internal") {
      return []
    }

    return this.$store.project.find("internal").constructions;
  }

  get placeholder () {
    return this.maintenanceType.length ?
      this.maintenanceType[0].id === 'internal'
      ? 'Manutenção em oficinas' : 'Selecione o projecto'
      : 'Selecione primeiro o tipo de manutenção'
  }

  cleanSelection() {
    if (this.maintenanceType.length && this.maintenanceType[0].id !== "external") this.selectedProject = [];
  }

  selectedOffice(officeId) {
    this.officeId = officeId;
  }

  selectMaintenanceProjectAndOffice () {
    this.filterEquipmentsEvt.emit({
      projectId: this.selectedProject[0].id,
      officeId: this.officeId
    });
  }
}
