import { Component } from "@angular/core";
import { MaintenanceInterface } from "@shared/interfaces/MaintenanceInterface";
import { StoreService } from "@shared/services/store/store.service";
import { EquipmentMaintenanceDto } from "./manutencao-equipments/manutencao-equipamentos.component";
import { FilterEquipment } from "./manutencao-sidebar/manutencao-sidebar.component";

@Component({
  selector: "app-manutencao-registrar",
  templateUrl: "./manutencao-register.component.html",
})
export class ManutencaoRegistrarComponent {
  maintenance: EquipmentMaintenanceDto[] = [];

  projectId: string = null;
  officeId: string = null;

  resetMaintenance(): MaintenanceInterface {
    return {
      construction: null,
      createdBy: "Super Admin",
      description: null,
      subMaintenances: null,
      level: null,
      project: null,
      startDate: null,
      status: null,
    };
  }

  selectFilter (evt: FilterEquipment) {
    this.projectId = evt.projectId,
    this.officeId = evt.officeId
  }

  removeItemFromMaintenanceStage (id) {
    this.maintenance = this.maintenance.filter((main) => !main.equipment.some((eq) => eq.id === id))
  }

  addEquipmentMaintenanceToStage(equipments) {
    this.maintenance.push(equipments);
  }
}
