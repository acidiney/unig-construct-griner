 
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '@core/authentication/auth.service'; 
import { HttpService } from '@core/providers/http/http.service';
import { ConfigService } from '@shared/services/config.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html', 
})
export class DashboardComponent implements OnInit, OnDestroy {

  private dash = {
    clienteCount: '0',
    facturaCount: '0',
    produtoCount: '0',
    documentoCount: '0',


    facturaSumTotalSemImpostoHoje: 0,
    facturaSumTotalSemImpostoMes: 0,
    facturaSumTotalSemImpostoOntem: 0,
    facturaSumTotalSemImpostoMesAnterior: 0,
    facturaSumTotalSemImpostoGeral: 0,

    facturaSumTotalComImpostoHoje: 0,
    facturaSumTotalComImpostoMes: 0,
    facturaSumTotalComImpostoOntem: 0,
    facturaSumTotalComImpostoMesAnterior: 0,
    facturaSumTotalComImpostoGeral: 0
  }
  constructor(private auth: AuthService, private http: HttpService, private configService: ConfigService) {
  }

  ngOnInit() {

     this.dashboard(); 
  }

  ngOnDestroy(): void {
    document.body.className = ' ';
  }

  /**
   * @name "Estatistica dashboard"
   * @descriptio "Estatistica dashboard"
   * @author "caniggia.moreira@itgest.pt"
   * @param start 
   * @param end 
   */
  private dashboard() {
    this.configService.loaddinStarter('start');
    this.http.call_get('dashboard/listar').subscribe(
      response => { 
        this.dash.clienteCount = this.configService.numberFormat(Object(response).data.clienteCount);
        this.dash.produtoCount = this.configService.numberFormat(Object(response).data.produtoCount);
        this.dash.facturaCount = this.configService.numberFormat(Object(response).data.facturaCount);
        this.dash.documentoCount = this.configService.numberFormat(Object(response).data.documentoCount); 
      }
    );
  }


}
