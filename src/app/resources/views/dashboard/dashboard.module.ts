import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';


// import { ModalModule } from '../modal';

@NgModule({
  imports: [
    // ModalModule,
    FormsModule,
    DashboardRoutingModule, 
  ],
  declarations: [ DashboardComponent ]
})
export class DashboardModule { }
 