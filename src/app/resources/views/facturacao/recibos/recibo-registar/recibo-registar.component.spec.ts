import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReciboRegistarComponent } from './recibo-registar.component';

describe('ReciboRegistarComponent', () => {
  let component: ReciboRegistarComponent;
  let fixture: ComponentFixture<ReciboRegistarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReciboRegistarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReciboRegistarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
