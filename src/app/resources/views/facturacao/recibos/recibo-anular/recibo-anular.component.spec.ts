import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReciboAnularComponent } from './recibo-anular.component';

describe('ReciboAnularComponent', () => {
  let component: ReciboAnularComponent;
  let fixture: ComponentFixture<ReciboAnularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReciboAnularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReciboAnularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
