import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReciboListarComponent } from './recibo-listar.component';

describe('ReciboListarComponent', () => {
  let component: ReciboListarComponent;
  let fixture: ComponentFixture<ReciboListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReciboListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReciboListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
