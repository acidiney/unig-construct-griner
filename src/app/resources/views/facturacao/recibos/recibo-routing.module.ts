import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; 
import { ReciboRegistarComponent } from './recibo-registar/recibo-registar.component';
import { ReciboListarComponent } from './recibo-listar/recibo-listar.component';
import { AuthGuard } from '@core/guards/auth.guard';

const routes: Routes = [
  {
    path: 'recibos', 
    data: {
      title: 'Recibos',
    },
    children: [
  {
    path: 'consultar-recibo',
    component: ReciboListarComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Consultar Recibo',
      layout:{
        customLayout: true, 
        layoutNavigationTop: false, 
      }
    }
  },{
    path: 'registar-recibo',
    component: ReciboRegistarComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Gerar Recibo',
      layout:{
        customLayout: true, 
        layoutNavigationTop: false, 
      }
    } 
  }
]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReciboRoutingModule {}
