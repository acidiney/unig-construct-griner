import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 

import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import {MatRadioModule, DateAdapter, MAT_DATE_FORMATS, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatIconModule, MatButtonModule, MatSelectModule, MatOptionModule } from "@angular/material";
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { FormatDateAdapter, FORMAT_DATE_FORMATS } from "@shared/directives/date-adapter";
import { NgxLoadingModule } from 'ngx-loading'; 
import { NgxPaginationModule } from "ngx-pagination";

import { ArchwizardModule } from "angular-archwizard";

import { ProgressBarModule } from "angular-progress-bar"; 
import { ExcelService } from '@shared/services/excel.service';

import { FacturaRoutingModule } from './facturas/factura-routing.module';
import { ReciboRoutingModule } from './recibos/recibo-routing.module';

import { FacturacaoCreateComponent } from './facturas/facturacao-create/facturacao-create.component';
import { FacturacaoListarComponent } from './facturas/facturacao-listar/facturacao-listar.component';
import { AnularFacturaComponent } from './facturas/anular-factura/anular-factura.component';
import { ReciboListarComponent } from './recibos/recibo-listar/recibo-listar.component';
import { ReciboRegistarComponent } from './recibos/recibo-registar/recibo-registar.component';
import { ReciboAnularComponent } from './recibos/recibo-anular/recibo-anular.component';
import { EmitirNotaCreditoComponent } from './facturas/emitir-nota-credito/emitir-nota-credito.component';

@NgModule({
  declarations: [ReciboListarComponent, FacturacaoCreateComponent, FacturacaoListarComponent, AnularFacturaComponent, ReciboRegistarComponent, ReciboAnularComponent, EmitirNotaCreditoComponent],
  imports: [
    CommonModule,
    FacturaRoutingModule,
    ReciboRoutingModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule, 
    MatSelectModule,
    MatOptionModule,
    MatButtonModule, 
    FormsModule,ReactiveFormsModule, 
    NgxPaginationModule,
    NgxLoadingModule.forRoot({
      backdropBorderRadius: '3px',
      backdropBackgroundColour: 'rgba(255, 255, 255, 0.78)',
      primaryColour: '#20a8d8',
      secondaryColour: '#20a8d8',
      tertiaryColour: '#20a8d8',
    }),
    NgxSkeletonLoaderModule.forRoot(),
    ArchwizardModule,
    ProgressBarModule
  ],providers: [ 
    {
      provide: DateAdapter,
      useClass: FormatDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: FORMAT_DATE_FORMATS,
    },
    ExcelService
  ],
})
export class FacturacaoModule { }
