import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { HttpService } from '@core/providers/http/http.service';
import { ConfigService } from '@shared/services/config.service';
import { Ng2IzitoastService } from 'ng2-izitoast';
@Component({
  selector: "app-emitir-nota-credito",
  templateUrl: "./emitir-nota-credito.component.html",
  styleUrls: ["./emitir-nota-credito.component.css"],
})
export class EmitirNotaCreditoComponent implements OnInit {
  private factura_id: number;
  private factura: any = null;
  private loading: boolean = false;

  private sucesso: boolean = false;

  private series: any = [];
  private serie: number = null;
  private observacao: string;
  total: number = 0;
  totalComImposto: number = 0;
  totalSemImposto: number = 0;
  private imprimir: any;

  valida_code: number = 0;
  message: string = "";

  private line: any;

  private item: any = {
    id: null,
    quantidade: null,
  };
 

  constructor(
    private http: HttpService,
    private configService: ConfigService,
    private route: ActivatedRoute,
    private _route: Router,
   private iziToast: Ng2IzitoastService
  ) {
    this.route.paramMap.subscribe((params) => {
      this.factura_id = +params.get("id");
    });
  }

  ngOnInit() {
    this.getFactura(this.factura_id);
    this.getSeries();
  }

  ngOnDestroy() {}

  private getFactura(factura_id) {
    this.loading = true;
    this.http
      .__call("factura/findFactFromNc/" + factura_id, null)
      .subscribe((response) => {
        this.valida_code = Object(response).code;
         this.message = Object(response).message;
        if (Object(response).code === 200) {
          this.factura = Object(response).data;
          this.factura.produtos.forEach((element) => {
            Object.assign(element, { checked: false });
          });
        }
        this.loading = false;
      });
  }

  /**
   * @name "Listar series"
   * @descriptio "Esta Função permite Listar todas series"
   * @author "caniggia.moreira@itgest.pt"
   * @param start
   * @param end
   */
  private getSeries() {
    this.loading = true;

    this.series = [];
    this.http
      .__call("serie/selectBoxSeries", { documentos: ["NC"] })
      .subscribe((response) => {
        this.series = Object(response).data;
        this.loading = false;
      });
  }

  setProduto(p: any) {
    this.line = p;
    this.item.id = p.id;
    this.item.quantidade = p.quantidade;
  }

  getCalcularLine() {
    /* if (this.item.quantidade > this.line.qtd_fixo ) {
        this.configService.showAlert("A nova quantidade não pode ser maior que a quantidade original", "alert-danger", true);
      return;
    }*/
    if (
      this.item.quantidade <= 0 ||
      isNaN(this.item.quantidade) ||
      Number.isInteger(this.item.quantidade) == false ||
      this.item.quantidade > this.line.qtd_fixo
    ) { 
      this.iziToast.info({
        title: 'Info!',
        message: "Não foi informado uma quantidade valida",
      });
      
      return;
    }

    for (let index = 0; index < this.factura.produtos.length; index++) {
      const l = this.factura.produtos[index];
      if (l.id === this.item.id) {
        l.quantidade = this.item.quantidade;

        l.valor_desconto =
          l.valor_desconto == 0
            ? 0
            : parseFloat(
                (l.valor * (l.valor / l.valor_desconto / 100)).toFixed(2)
              );

        var desc = l.valor - l.valor_desconto;

        l.valorImposto = parseFloat(
          (l.quantidade * (desc * (l.imposto.valor / 100))).toFixed(2)
        );

        l.linhaTotalSemImposto = parseFloat((desc * l.quantidade).toFixed(2));

        l.total = l.linhaTotalSemImposto + l.valorImposto;

        this.factura.produtos.splice(index, 1);
        this.factura.produtos.splice(index, 0, l);
      }
    }
    this.calcularTotal();
    console.log(this.factura.produtos);
  }

  private finalizarNotaCredito() {
    this.validationLineCheckbox();

    this.configService.loaddinStarter("start");
    if (this.serie == undefined) { 
      this.iziToast.info({
        title: 'Info!',
        message: "É obrigatório fornecer uma serie",
      });
      
      return;
    } else if (this.validation === 0) { 
      this.iziToast.info({
        title: 'Info!',
        message: "É obrigatório selecionar pelo menos uma linha da factura",
      });
      return;
    } else {
      var produts = [];
      for (let index = 0; index < this.factura.produtos.length; index++) {
        const l = this.factura.produtos[index];
        if (l.checked === true) {
          produts.push(l);
        }
      }
      this.loading = true;
      this.http
        .__call("facturas", {
          produtos: produts,
          documento: "Nota de Crédito",
          cliente: this.factura.cliente.id,
          total: this.total,
          serie_id: this.serie,
          totalComImposto: this.totalComImposto,
          totalSemImposto: this.totalSemImposto,
          observacao: this.observacao,
          numero_origem_factura: this.factura.factura.factura_sigla,
          data_origem_factura: this.factura.factura.created_at,
          moeda: null,
          contas_cliente: {conta_id:null, servico_id:null},
          is_iplc: this.factura.factura.is_iplc,
        })
        .subscribe((res) => {
          if (Object(res).code == 200) {
            this.sucesso = true;
            this.imprimir = Object(res).data.id;
            this.configService.showAlert(
              Object(res).message,
              "alert-success",
              true
            );
          }
          this.loading = false;
        });
    }
    this.configService.loaddinStarter("stop");
  }

  private calcularTotal() {
    this.total = 0;
    this.totalSemImposto = 0;
    this.totalComImposto = 0;
    //Calcula o Total da Factura
    for (let index = 0; index < this.factura.produtos.length; index++) {
      if (this.factura.produtos[index].checked === true) {
        this.totalSemImposto += this.factura.produtos[
          index
        ].linhaTotalSemImposto;
        this.totalComImposto += this.factura.produtos[index].valorImposto;
        this.total += this.factura.produtos[index].total;
      }
    }
  }

  private linesChecked: any = [];

  setLineCheckbox(produt: any) {
    this.factura.produtos.forEach((element) => {
      if (element.id === produt.id) {
        if (element.checked == false) {
          Object.assign(element, { checked: true });
        } else {
          Object.assign(element, { checked: false });
        }
      }
    });
    this.calcularTotal();
  }

  validation: number = 0;

  validationLineCheckbox() {
    this.validation = 0;
    this.factura.produtos.forEach((element) => {
      if (element.checked === true) {
        this.validation++;
      }
    });
  }

  back() {
    this._route.navigate(["/facturacao/facturas/listar-facturas"]);
  }
}

