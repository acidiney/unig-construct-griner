import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacturacaoCreateComponent } from './facturacao-create.component';

describe('FacturacaoCreateComponent', () => {
  let component: FacturacaoCreateComponent;
  let fixture: ComponentFixture<FacturacaoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacturacaoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacturacaoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
