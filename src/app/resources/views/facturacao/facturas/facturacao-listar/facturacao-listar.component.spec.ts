import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacturacaoListarComponent } from './facturacao-listar.component';

describe('FacturacaoListarComponent', () => {
  let component: FacturacaoListarComponent;
  let fixture: ComponentFixture<FacturacaoListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacturacaoListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacturacaoListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
