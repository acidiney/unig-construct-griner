import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; 
import { FacturacaoCreateComponent } from './facturacao-create/facturacao-create.component';
import { FacturacaoListarComponent } from './facturacao-listar/facturacao-listar.component';
import { EmitirNotaCreditoComponent } from './emitir-nota-credito/emitir-nota-credito.component';

import { AuthGuard } from '@core/guards/auth.guard';

const routes: Routes = [
  {
    path: 'facturas', 
    data: {
      title: 'Facturas',
    },
    children: [
  {
    path: 'listar-facturas',
    component: FacturacaoListarComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Listar Facturas',
      layout:{
        customLayout: true, 
        layoutNavigationTop: false, 
      }
    } 
  },{
    path: 'registar-factura',
    component: FacturacaoCreateComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Registar Factura',
      layout:{
        customLayout: true, 
        layoutNavigationTop: false, 
      }
    } 
  }, 
  {
    path: 'emitir-nota-de-credito/:id',
    component: EmitirNotaCreditoComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Emitir Note de Crédito',
      layout:{
        customLayout: true, 
        layoutNavigationTop: false, 
      }
    } 
  }
]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacturaRoutingModule {}
