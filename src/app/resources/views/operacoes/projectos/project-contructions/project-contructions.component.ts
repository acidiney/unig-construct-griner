import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Filter } from "@shared/models/filter";
import { Pagination } from "@shared/models/pagination";
import { StoreService } from "@shared/services/store/store.service";
import { environment } from "@environments/environment";

@Component({
  selector: 'app-project-contructions',
  templateUrl: './project-constructions.component.html'
})

export class ProjectContructionsComponent {
  public pagination = new Pagination();
  public filter = new Filter();

  constructor(
    private $store: StoreService,
    private $route: ActivatedRoute
  ) {}

  private get projectId() {
    return this.$route.snapshot.params.projectId as string;
  }

  public get project() {
    return this.$store.project.find(this.projectId);
  }

  public get editorUrl () {
    return environment.editorUrl
  }

}
