import { EquipmentStatus } from './../../../../../../shared/interfaces/EquipmentInterface';
import { ConfigService } from '@shared/services/config.service';
import { ConstructionInterface, ProjectInterface } from '@shared/interfaces/ProjectInterface';
import { Component, Input } from "@angular/core";
import { StoreService } from '@shared/services/store/store.service';
import { ProvinceInterface } from '@shared/interfaces/ProvinceInterface';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-create-construction',
  templateUrl: './create-construction.component.html'
})
export class CreateConstructionComponent {
  @Input('project') project: ProjectInterface;

  public files: File[] = []
  public construction: ConstructionInterface;
  public selectedEquipments = []

  dropdownSettings: IDropdownSettings = {};

  constructor (
    private $store: StoreService,
    private configService: ConfigService
  ) {
    this.construction = this.resetConstruction()
    this.dropdownSettings = {
      idField: 'id',
      textField: 'description',
      selectAllText: 'Marcar Todos',
      unSelectAllText: 'Desmarcar Todos',
      itemsShowLimit: 3,
      enableCheckAll: true,
      allowSearchFilter: true
    };
  }

  public formatCurrency(badget = 0): string {
    return this.configService.numberFormat(badget);
  }

  resetConstruction () : ConstructionInterface {
    return {
      address: {
        city: '',
        state: '',
        street: ''
      },
      badget: 0,
      description: '',
      endDate: null,
      equipments: [],
      startDate: null
    }
  }

  get provinces(): ProvinceInterface[] {
    return this.$store.province.findAll();
  }

  get cities() {
    const province = this.$store.province.find(
      this.construction.address.state
    );

    return province ? province.cities : [];
  }

  get equipments() {
    return this.$store.equipment.findAll()
      .filter((equipment) => equipment.status === EquipmentStatus.available)
  }

  createConstruction () {
    this.$store.project.addConstructionToProject(this.project.id, this.construction)
    this.construction = this.resetConstruction()


    const closeBtn = document.querySelector(".btn-close") as any;
    closeBtn.click();
  }

  onSelectAll (event) {
    console.log(event)
  }

  onItemSelect(item: any) {
    console.log(item);
  }

  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);

    const formData = new FormData();

    for (var i = 0; i < this.files.length; i++) {
      formData.append("file[]", this.files[i]);
    }
  }

  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }
}
