import { Router } from '@angular/router';
import { Component, OnInit } from "@angular/core";
import { StoreService } from "@shared/services/store/store.service";
import { ClientInterface } from "@shared/interfaces/ClientInterface";
import { ProvinceInterface } from "@shared/interfaces/ProvinceInterface";
import {
  ProjectInterface,
  ProjectStatus,
} from "@shared/interfaces/ProjectInterface";

@Component({
  selector: "app-projecto-registar",
  templateUrl: "./projecto-registar.component.html",
  styleUrls: ["./projecto-registar.component.css"],
})
export class ProjectoRegistarComponent implements OnInit {
  files: File[] = [];

  public isLoading = false;
  public clients: ClientInterface[] = [];
  public createProject: ProjectInterface = {
    address: {
      city: "none",
      state: "none",
      street: "",
    },
    badget: 0,
    client: null,
    description: "",
    designation: "",
    endDate: null,
    id: "",
    startDate: null,
    status: ProjectStatus.pending,
  };

  public selectedClient: string = null

  constructor(private $store: StoreService, private $router: Router) {}

  loadAllAvailableClients() {
    setTimeout(() => {
      this.clients = this.$store.client.findAll();
    }, 1000);
  }

  get provinces(): ProvinceInterface[] {
    return this.$store.province.findAll();
  }

  get cities() {
    const province = this.$store.province.find(
      this.createProject.address.state
    );

    return province ? province.cities : [];
  }

  saveProject () {
    this.isLoading = true;
    setTimeout(() => {
      this.$store.project.create({
        ...this.createProject,
        client: this.$store.client.find(this.selectedClient)
      })
      this.isLoading = false
      this.$router.navigate(['/operacoes/projectos/listar-projectos'])
    }, 1000);
  }

  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);

    const formData = new FormData();

    for (var i = 0; i < this.files.length; i++) {
      formData.append("file[]", this.files[i]);
    }

    /*this.http.post('http://localhost:8001/upload.php', formData)
        .subscribe(res => {
           console.log(res);
           alert('Uploaded Successfully.');
        })*/
  }

  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  ngOnInit() {
    this.loadAllAvailableClients();
  }
}
