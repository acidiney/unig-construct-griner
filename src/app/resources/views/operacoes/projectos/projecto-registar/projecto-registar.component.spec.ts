import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectoRegistarComponent } from './projecto-registar.component';

describe('ProjectoRegistarComponent', () => {
  let component: ProjectoRegistarComponent;
  let fixture: ComponentFixture<ProjectoRegistarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectoRegistarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectoRegistarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
