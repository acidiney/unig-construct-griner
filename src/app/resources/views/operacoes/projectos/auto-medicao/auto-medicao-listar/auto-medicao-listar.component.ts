import { Component, OnInit } from '@angular/core';
import { ConfigService } from "@shared/services/config.service";
import { Pagination } from "@shared/models/pagination";
import { Filter } from "@shared/models/filter";

@Component({
  selector: 'app-auto-medicao-listar',
  templateUrl: './auto-medicao-listar.component.html',
  styleUrls: ['./auto-medicao-listar.component.css']
})
export class AutoMedicaoListarComponent implements OnInit {

  public pagination = new Pagination();
  public filter = new Filter();
  private auto_medicaos:any = [
    {
      id: 1,
      projecto: 'Administração pública',
      obra: "1 - Reabilitação do cine Africa",
      data_licitacao:"2020-03-01",
      data_adjudicacao_obra:"2020-04-15",
      data_inicio_trabalho:"2020-06-30",
      data_conclucao_trabalho:"2020-03-01",
      valor_proposta:98435.71,
      valor_contrato:98435.71,
      data_criacao:"2020-03-01"
    },
    {
      id: 2,
      projecto: 'Administração pública',
      obra: "Shopping do Cazenga",
      data_licitacao:"2020-03-01",
      data_adjudicacao_obra:"2020-04-15",
      data_inicio_trabalho:"2020-06-30",
      data_conclucao_trabalho:"2020-03-01",
      valor_proposta:196004.56,
      valor_contrato:196004.56,
      data_criacao:"2020-03-01"
    },
    {
      id: 3,
      projecto: 'ITGest',
      obra: "Escritorio nova vida",
      data_licitacao:"2020-03-01",
      data_adjudicacao_obra:"2020-04-15",
      data_inicio_trabalho:"2020-06-30",
      data_conclucao_trabalho:"2020-03-01",
      valor_proposta:87334.87,
      valor_contrato:87334.87,
      data_criacao:"2020-03-01"
    }

  ]
  constructor(public configService: ConfigService) { }

  ngOnInit() {
  }

}
