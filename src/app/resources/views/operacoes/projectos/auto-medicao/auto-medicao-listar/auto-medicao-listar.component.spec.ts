import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoMedicaoListarComponent } from './auto-medicao-listar.component';

describe('AutoMedicaoListarComponent', () => {
  let component: AutoMedicaoListarComponent;
  let fixture: ComponentFixture<AutoMedicaoListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoMedicaoListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoMedicaoListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
