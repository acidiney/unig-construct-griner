import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoMedicaoEncerrarAutoComponent } from './auto-medicao-encerrar-auto.component';

describe('AutoMedicaoEncerrarAutoComponent', () => {
  let component: AutoMedicaoEncerrarAutoComponent;
  let fixture: ComponentFixture<AutoMedicaoEncerrarAutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoMedicaoEncerrarAutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoMedicaoEncerrarAutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
