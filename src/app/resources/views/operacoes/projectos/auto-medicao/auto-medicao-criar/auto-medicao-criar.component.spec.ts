import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoMedicaoCriarComponent } from './auto-medicao-criar.component';

describe('AutoMedicaoCriarComponent', () => {
  let component: AutoMedicaoCriarComponent;
  let fixture: ComponentFixture<AutoMedicaoCriarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoMedicaoCriarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoMedicaoCriarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
