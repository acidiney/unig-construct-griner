import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "@core/guards/auth.guard";

import { ProjectoListarComponent } from "./projecto-listar/projecto-listar.component";
import { ProjectoRegistarComponent } from "./projecto-registar/projecto-registar.component";
import { ProjectoMostrarComponent } from './projecto-mostrar/projecto-mostrar.component'

import { PlantaRegistarProjectoComponent } from './planta/planta-registar-projecto/planta-registar-projecto.component';
import { PlantaListarProjectoComponent } from './planta/planta-listar-projecto/planta-listar-projecto.component';
import { GanttProjectComponent } from './gantt-project/gantt-project.component';
import { GaleryPlantasComponent } from './planta/galery-plantas/galery-plantas.component';
import { ProjectoMateriaisComponent } from "./projecto-materiais/projecto-materiais.component";

import { AutoMedicaoListarComponent } from './auto-medicao/auto-medicao-listar/auto-medicao-listar.component';
import { AutoMedicaoCriarComponent } from './auto-medicao/auto-medicao-criar/auto-medicao-criar.component';

const routes: Routes = [
  {
    path: "projectos",
    data: {
      title: "Projectos",
    },
    children: [
      {
        path: "mostrar-projectos/:projectId",
        component: ProjectoMostrarComponent,
        canActivate: [AuthGuard],
        data: {
          title: "Mostrar Projecto",
          layout: {
            customLayout: true,
            layoutNavigationTop: false,
          },
        },
      },
      {
        path: "mostrar-projectos/:projectId/configurar-materiais",
        component: ProjectoMateriaisComponent,
        canActivate: [AuthGuard],
        data: {
          title: "Configurar materiais do projecto",
          layout: {
            customLayout: true,
            layoutNavigationTop: false,
          },
        },
      },
      {
        path: "listar-projectos",
        component: ProjectoListarComponent,
        canActivate: [AuthGuard],
        data: {
          title: "Listar Projectos",
          layout: {
            customLayout: true,
            layoutNavigationTop: false,
          },
        },
      },
      {
        path: "registar-projecto",
        component: ProjectoRegistarComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Registrar projecto',
          layout:{
            customLayout: true,
            layoutNavigationTop: false,
          }
        }
      },{
        path: 'plantas',
        //component: GaleryPlantasComponent,
        canActivate: [AuthGuard],
        data: {
          title: "Plantas",
        },
        children: [
          {
            path: 'listar/:id',
            component: PlantaListarProjectoComponent,
            canActivate: [AuthGuard],
            data: {
              title: "Listar Plantas",
              layout: {
                customLayout: true,
                layoutNavigationTop: false,
              },
            },
          },
          {
            path: 'registar/:id',
            component: PlantaRegistarProjectoComponent,
            canActivate: [AuthGuard],
            data: {
              title: "Registar Planta",
              layout: {
                customLayout: true,
                layoutNavigationTop: false,
              },
            },
          },
        ],
      },{
        path: 'medicao',
        //component: GaleryPlantasComponent,
        canActivate: [AuthGuard],
        data: {
          title: "Auto de Medição",
        },
        children: [
          {
            path: 'listar',
            component: AutoMedicaoListarComponent,
            canActivate: [AuthGuard],
            data: {
              title: "Listar Auto de Medição",
              layout: {
                customLayout: true,
                layoutNavigationTop: false,
              },
            },
          },
          {
            path: 'registar',
            component: AutoMedicaoCriarComponent,
            canActivate: [AuthGuard],
            data: {
              title: "Registar Auto de Medição",
              layout: {
                customLayout: true,
                layoutNavigationTop: false,
              },
            },
          },
        ],
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectoRoutingModule {}
