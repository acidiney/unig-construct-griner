import { ConfigService } from "@shared/services/config.service";
import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { StoreService } from "@shared/services/store/store.service";
import { ProjectStatus } from "@shared/interfaces/ProjectInterface";

@Component({
  selector: "app-project-mostrar",
  templateUrl: "./projecto-mostrar.component.html",
})
export class ProjectoMostrarComponent {
  constructor(
    private $store: StoreService,
    private $route: ActivatedRoute,
    private configService: ConfigService
  ) {}

  private get projectId() {
    return this.$route.snapshot.params.projectId as string;
  }

  public get project() {
    return this.$store.project.find(this.projectId);
  }

  public formatCurrency(badget = 0): string {
    return this.configService.numberFormat(badget);
  }

  public chooseBadgeColor(status) {
    const badge = {
      [ProjectStatus.done]: "badge-primary",
      [ProjectStatus.paid]: "badge-success",
      [ProjectStatus.started]: "badge-warning",
      [ProjectStatus.pending]: "badge-info",
      [ProjectStatus.rejected]: "badge-danger",
    };

    return badge[status];
  }

  public chooseBadgeLabel(status) {
    const badge = {
      [ProjectStatus.done]: "Concluído",
      [ProjectStatus.paid]: "Pago",
      [ProjectStatus.started]: "Em execução",
      [ProjectStatus.pending]: "Pendente",
      [ProjectStatus.rejected]: "Rejeitado",
    };

    return badge[status];
  }

  public formatDate(date) {
    return this.configService.formatDate(date);
  }

  public isAValidRange(date) {
    const nextDate = new Date(date);
    const currentDate = new Date();

    const utc1 = Date.UTC(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate());
    const utc2 = Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());

    return utc2 > utc1;
  }

  public projectIsSkidding(date, status) {
    return this.isAValidRange(date) && status === ProjectStatus.started;
  }

  public howManyDaysLate(date) {
    const nextDate = new Date(date);
    const currentDate = new Date();

    const utc1 = Date.UTC(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate());
    const utc2 = Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());

    return Math.ceil((utc2 - utc1) / (1000 * 60 * 60 * 24));
  }


}
