import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ConfigService } from "@shared/services/config.service";

import { ExcelService } from "@shared/services/excel.service";
import * as moment from "moment";

import { Pagination } from "@shared/models/pagination";
import { Filter } from "@shared/models/filter";
import { ProjectInterface } from "@shared/interfaces/ProjectInterface";
import { StoreService } from "@shared/services/store/store.service";

@Component({
  selector: "app-projecto-listar",
  templateUrl: "./projecto-listar.component.html",
  styleUrls: ["./projecto-listar.component.css"],
})
export class ProjectoListarComponent implements OnInit {
  public pagination = new Pagination();
  public filter = new Filter();

  constructor(
    private _route: Router,
    private $store: StoreService,
    public configService: ConfigService,
    private excelService: ExcelService
  ) {
    this.filter.orderBy = "none";
  }

  public produto: any;
  public loading: boolean = false;

  public projects: ProjectInterface[] = [];
  ngOnInit() {
    this.getPageFilterData(1);
  }
  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile =
      "Lista_Projectos-" +
      moment(CurrentDate).format("DD") +
      "-" +
      moment(CurrentDate).format("MM") +
      "-" +
      moment(CurrentDate).format("YYYY") +
      " " +
      moment(CurrentDate).format("H") +
      ":" +
      moment(CurrentDate).format("m");
    this.excelService.exportAsExcelFile(
      document.getElementsByClassName("exportAsXLSXCliente")[0],
      nameFile
    );
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

  /**
   *
   * @param start
   * @param end
   * @param search
   */
  private listarProjectos() {
    this.loading = true;

    setTimeout(() => {
      this.projects = this.$store.project.findAll();
      this.loading = false;
    }, 1000);
  }

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      this.pagination.page = 1;
      return;
    }
    this.pagination.page = page;
    this.listarProjectos();
  }
  registerProduto(){
    this._route.navigate(["/operacoes/projectos/registar-projecto"]);
  }
  linkGantt(projectID){
    this._route.navigate(["/operacoes/projectos/gantt/"+projectID]);
  }
  linkPlanta(projectID){
    this._route.navigate(["/operacoes/projectos/plantas/listar/"+projectID]);
  }
  linkFase(projectID){
    this._route.navigate(["/operacoes/projectos/gantt/"+projectID]);
  }
 
  createOrUpdate(e){

}
}
