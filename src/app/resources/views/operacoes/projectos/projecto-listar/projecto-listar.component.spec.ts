import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectoListarComponent } from './projecto-listar.component';

describe('ProjectoListarComponent', () => {
  let component: ProjectoListarComponent;
  let fixture: ComponentFixture<ProjectoListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectoListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectoListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
