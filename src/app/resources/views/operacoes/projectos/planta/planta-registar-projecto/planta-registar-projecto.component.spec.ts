import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantaRegistarProjectoComponent } from './planta-registar-projecto.component';

describe('PlantaRegistarProjectoComponent', () => {
  let component: PlantaRegistarProjectoComponent;
  let fixture: ComponentFixture<PlantaRegistarProjectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlantaRegistarProjectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantaRegistarProjectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
