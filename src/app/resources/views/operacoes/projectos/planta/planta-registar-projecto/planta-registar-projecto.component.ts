import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-planta-registar-projecto',
  templateUrl: './planta-registar-projecto.component.html',
  styleUrls: ['./planta-registar-projecto.component.css']
})
export class PlantaRegistarProjectoComponent implements OnInit {

   
  planta = {
    project_id:null,
    title:null,
    description:null
  }
  files:any = [];
  constructor() { }

  ngOnInit() {
  }

  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);

    const formData = new FormData();

    for (var i = 0; i < this.files.length; i++) { 
      formData.append("file[]", this.files[i]);
    }

    /*this.http.post('http://localhost:8001/upload.php', formData)
    .subscribe(res => {
       console.log(res);
       alert('Uploaded Successfully.');
    })*/
}

onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
}

}
