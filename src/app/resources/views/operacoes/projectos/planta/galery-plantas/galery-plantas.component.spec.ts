import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaleryPlantasComponent } from './galery-plantas.component';

describe('GaleryPlantasComponent', () => {
  let component: GaleryPlantasComponent;
  let fixture: ComponentFixture<GaleryPlantasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaleryPlantasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaleryPlantasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
