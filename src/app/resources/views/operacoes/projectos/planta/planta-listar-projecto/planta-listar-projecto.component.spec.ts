import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantaListarProjectoComponent } from './planta-listar-projecto.component';

describe('PlantaListarProjectoComponent', () => {
  let component: PlantaListarProjectoComponent;
  let fixture: ComponentFixture<PlantaListarProjectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlantaListarProjectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantaListarProjectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
