import { Component, OnInit } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
  selector: 'app-planta-listar-projecto',
  templateUrl: './planta-listar-projecto.component.html',
  styleUrls: ['./planta-listar-projecto.component.css']
})
export class PlantaListarProjectoComponent implements OnInit {

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  
  constructor() { }

  ngOnInit(): void {

    this.galleryOptions = [ 
      {   
        "thumbnailsMargin":30,
        "imageDescription": true,
        "previewKeyboardNavigation": true,
        "previewCloseOnClick": true, "previewCloseOnEsc": true,
        "previewZoom": true, "previewRotate": true,
        "previewFullscreen": true, "previewForceFullscreen": false,
        "image": false, 
        "thumbnailsRemainingCount": true, 
         "width": "1400px", "height": "400px",
         
      },
      { "breakpoint": 1000, "width": "100%", "thumbnailsColumns": 1 } 
        /*{ 
          "image": false, 
          "thumbnailsRemainingCount": true, 
          "height": "150px", 
        },
        { 
          "breakpoint": 500, 
          "width": "100%", "height": "200px",
          "thumbnailsColumns": 10,
          preview: false
        },
        /*{
            width: '700px',
            height: '800px',
            thumbnailsColumns: 4,
            imageAnimation: NgxGalleryAnimation.Slide
        },
        // max-width 800
        {
            breakpoint: 800,
            width: '100%',
            height: '600px',
            imagePercent: 80,
            thumbnailsPercent: 20,
            thumbnailsMargin: 20,
            thumbnailMargin: 20
        },
        // max-width 400
        {
            breakpoint: 400,
            preview: false
        }*/
    ];

    this.galleryImages = [
      {
        small: 'assets/images/plantas/opcao-com-85m2.jpg',
        medium: 'assets/images/plantas/opcao-com-85m2.jpg',
        big: 'assets/images/plantas/opcao-com-85m2.jpg',
        description:"planta 1"
        },{
          small: 'assets/images/plantas/planta-casa-58m-2-2263747.jpg',
          medium: 'assets/images/plantas/planta-casa-58m-2-2263747.jpg',
          big: 'assets/images/plantas/planta-casa-58m-2-2263747.jpg',
          description:"planta 2"
      } ,{
        small: 'assets/images/plantas/20170505planta-de-casa-35-e1493982598522.jpg',
        medium: 'assets/images/plantas/20170505planta-de-casa-35-e1493982598522.jpg',
        big: 'assets/images/plantas/20170505planta-de-casa-35-e1493982598522.jpg',
        description:"planta 3"
        },{
          small: 'assets/images/plantas/Courtyard_House_LifeSpaces_Group.jpg',
          medium: 'assets/images/plantas/Courtyard_House_LifeSpaces_Group.jpg',
          big: 'assets/images/plantas/Courtyard_House_LifeSpaces_Group.jpg',
          description:"planta 4"
      },
      {
        small: 'assets/images/plantas/opcao-com-85m2.jpg',
        medium: 'assets/images/plantas/opcao-com-85m2.jpg',
        big: 'assets/images/plantas/opcao-com-85m2.jpg',
        description:"planta 1"
        },{
          small: 'assets/images/plantas/planta-casa-58m-2-2263747.jpg',
          medium: 'assets/images/plantas/planta-casa-58m-2-2263747.jpg',
          big: 'assets/images/plantas/planta-casa-58m-2-2263747.jpg',
          description:"planta 2"
      } ,{
        small: 'assets/images/plantas/20170505planta-de-casa-35-e1493982598522.jpg',
        medium: 'assets/images/plantas/20170505planta-de-casa-35-e1493982598522.jpg',
        big: 'assets/images/plantas/20170505planta-de-casa-35-e1493982598522.jpg',
        description:"planta 3"
        },{
          small: 'assets/images/plantas/Courtyard_House_LifeSpaces_Group.jpg',
          medium: 'assets/images/plantas/Courtyard_House_LifeSpaces_Group.jpg',
          big: 'assets/images/plantas/Courtyard_House_LifeSpaces_Group.jpg',
          description:"planta 4"
      }
    ];
}

}
