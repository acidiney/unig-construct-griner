import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ConfigService } from "@shared/services/config.service";
import { StoreService } from "@shared/services/store/store.service";

@Component({
  selector: "app-projecto-materiais",
  templateUrl: "./projecto-materias.component.html",
})
export class ProjectoMateriaisComponent {
  public selectedStage: string = null;
  public selectedTask: string = null;
  public addResourceForTask = {
    quantity: 1,
    resource: null,
  };
  public selectedResourceType = null;
  public selectedResource: string = null;

  constructor(
    private $store: StoreService,
    private $route: ActivatedRoute,
    private configService: ConfigService
  ) {}

  public get estimates() {
    return this.$store.budget.findAll(this.projectId);
  }

  private get projectId() {
    return this.$route.snapshot.params.projectId as string;
  }

  public get project() {
    return this.$store.project.find(this.projectId);
  }

  public get budgets() {
    return this.$store.stage.findAll(this.projectId);
  }

  public get spaces() {
    const budget = this.$store.stage.find(this.selectedStage);

    return budget ? budget.spaces : [];
  }

  public get space() {
    return this.spaces.find((space) => space.id === this.selectedTask);
  }

  public formatNumber(number) {
    return new Intl.NumberFormat().format(number);
  }

  public get resourceTypes() {
    return this.$store.resource.findAll();
  }

  public get resources() {
    const resourceType = this.$store.resource.find(this.selectedResourceType);

    return resourceType ? resourceType.resources : [];
  }

  public createResource() {
    const relatedResources = this.$store.resource.findRelatedResource(
      this.selectedResource
    );

    const resourceSelected = this.resources.find(
      (resource) => resource.id === this.selectedResource
    );

    const store = (resource) => {
      this.$store.budget.create({
        id: "",
        quantity: this.addResourceForTask.quantity,
        resourceTypeId: this.selectedResourceType,
        resourceId: resource.id,
        resource: resource.name,
        unitPrice: resource.badget,
        totalPrice: this.addResourceForTask.quantity * resource.badget,
        projectId: this.projectId,
      });
    };

    store(resourceSelected);

    for (let related of relatedResources) {
      store(related);
    }
  }

  public formatCurrency(badget = 0): string {
    return this.configService.numberFormat(badget);
  }

  public deleteResource (estimateId: string) {
    this.$store.budget.remove(estimateId)
  }

  public get totalStage(): number {
    return this.estimates.reduce((curr, next) => {
      return curr + next.totalPrice
    }, 0)
  }
}
