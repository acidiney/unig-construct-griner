import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtigoCompostoListarComponent } from './artigo-composto-listar.component';

describe('ArtigoCompostoListarComponent', () => {
  let component: ArtigoCompostoListarComponent;
  let fixture: ComponentFixture<ArtigoCompostoListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtigoCompostoListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtigoCompostoListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
