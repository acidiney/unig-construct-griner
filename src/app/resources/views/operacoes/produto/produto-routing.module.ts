import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@core/guards/auth.guard';

import { ProdutoListarComponent } from './produto-listar/produto-listar.component';
import { ProdutoRegistarComponent } from './produto-registar/produto-registar.component';
import { AssociarArtigoComponent } from './associar-artigo/associar-artigo.component';
import { ArtigoCompostoRegistarComponent } from './artigo-composto-registar/artigo-composto-registar.component';
import { ArtigoCompostoListarComponent } from './artigo-composto-listar/artigo-composto-listar.component';
const routes: Routes = [

  {
    path: 'produtos', 
    data: {
      title: 'Produto',
    },
    children: [
      {
        path: 'listar-produtos',
        component: ProdutoListarComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Listar Produtos & Serviços',
          layout:{
            customLayout: true, 
            layoutNavigationTop: false, 
          }
        }
      },{
        path: 'registar-produto',
        component: ProdutoRegistarComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Registar Produto & Serviço',
          layout:{
            customLayout: true, 
            layoutNavigationTop: false, 
          }
        }
      },
      {
        path: 'composicao', 
        data: {
          title: 'Artigos & Serviços Composto',
        },
        children: [
          {
            path: 'criar',
            component: ArtigoCompostoListarComponent,
            canActivate: [AuthGuard],
            data: {
              title: 'Criar Artigos & Serviços Compostos',
              layout:{
                customLayout: true, 
                layoutNavigationTop: false, 
              }
            }
          },{
            path: 'listar',
            component: ArtigoCompostoRegistarComponent,
            canActivate: [AuthGuard],
            data: {
              title: 'Listar Artigos & Serviços Compostos',
              layout:{
                customLayout: true, 
                layoutNavigationTop: false, 
              }
            }
          },{
            path: 'associar',
            component: AssociarArtigoComponent,
            canActivate: [AuthGuard],
            data: {
              title: 'Associar Artigos & Serviços Compostos',
              layout:{
                customLayout: true, 
                layoutNavigationTop: false, 
              }
            }
          }
        ]
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProdutoRoutingModule {}
