import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoArtigoComponent } from './tipo-artigo.component';

describe('TipoArtigoComponent', () => {
  let component: TipoArtigoComponent;
  let fixture: ComponentFixture<TipoArtigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoArtigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoArtigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
