import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtigoCompostoRegistarComponent } from './artigo-composto-registar.component';

describe('ArtigoCompostoRegistarComponent', () => {
  let component: ArtigoCompostoRegistarComponent;
  let fixture: ComponentFixture<ArtigoCompostoRegistarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtigoCompostoRegistarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtigoCompostoRegistarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
