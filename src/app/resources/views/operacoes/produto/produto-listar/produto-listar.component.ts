 
import { Component, OnInit } from '@angular/core'; 
import { Router } from "@angular/router";
import { HttpService } from '@core/providers/http/http.service';
import { ConfigService } from '@shared/services/config.service';

import { ExcelService } from '@shared/services/excel.service';
import * as moment from 'moment';


import { Pagination } from "@shared/models/pagination"; 
import { Filter } from "@shared/models/filter";  

@Component({
  selector: 'app-produto-listar',
  templateUrl: './produto-listar.component.html',
  styleUrls: ['./produto-listar.component.css']
})

export class ProdutoListarComponent implements OnInit {
 
  public pagination = new Pagination();
  public filter = new Filter();

  constructor(private http: HttpService,private configService: ConfigService, private excelService: ExcelService, private _route: Router) { }
  
  public produto: any;
 

  private loading: boolean = false;
   

 
  private items:any = [];
  ngOnInit() { 
    this.getPageFilterData(1);
  }
  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "Lista_Produtos-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

/**
 * 
 * @param start 
 * @param end 
 * @param search 
 */
  private listarProdutos() {  
    this.loading = true
    this.http.__call('artigo/listar', {filter: this.filter, pagination:this.pagination}).subscribe(
      response => {
        this.pagination.lastPage = Object(response).data.lastPage;
        this.pagination.page = Object(response).data.page;
        this.pagination.total = Object(response).data.total;
        this.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.loading = false
      }
    );
  }

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      this.pagination.page = 1;
      return;
    }
    this.pagination.page = page;
    this.listarProdutos();
  } 

  
  private setDataProduto(produto) {
    this.produto = produto
  }
  registerProduto(){
    this._route.navigate(["/operacoes/produtos/registar-produto"]);
  }

  
 
 
}