import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdutoRegistarComponent } from './produto-registar.component';

describe('ProdutoRegistarComponent', () => {
  let component: ProdutoRegistarComponent;
  let fixture: ComponentFixture<ProdutoRegistarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdutoRegistarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdutoRegistarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
