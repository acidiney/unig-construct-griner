 

import { Component, OnInit, Input,Output, EventEmitter,OnChanges, SimpleChange } from '@angular/core';
import { HttpService } from '@core/providers/http/http.service';
import { ConfigService } from '@shared/services/config.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'associar-artigo',
  templateUrl: './associar-artigo.component.html',
  styleUrls: ['./associar-artigo.component.css']
})
export class AssociarArtigoComponent implements OnInit {

  @Input() modal: string = "modalprodutoCreateOrEdit";
  @Input() title: string = "Registar Produto & Serviço";
  @Input() produto:any;

  imponstos = [
    { id: 0, descricao: null }
  ]


  submitted = false;
  private loading: boolean = false;
  @Input() simpleFormproduto: FormGroup; 

  @Output() private loadListproduto = new EventEmitter<any>();

  private produtos:any = [
    {
      principal: 'Betão',
      secundario: 'Cimento'
    },{
      principal: 'Betão',
      secundario: 'Água'
    },{
      principal: 'Betão',
      secundario: 'Bugalho'
    }
  ];

  constructor(private http: HttpService, private configService: ConfigService, private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {  
  }

  createForm() {
    this.simpleFormproduto = this.formBuilder.group({ 
      produto_id: ['', Validators.required], 
      produto_filho_id: [null, Validators.required]      
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.simpleFormproduto.controls;
  }

  onReset() {
    this.submitted = false;
    this.simpleFormproduto.reset();
  }


  onSubmit() {

    this.submitted = true;

    // parar aquei se o formulário for inválido
    if (this.simpleFormproduto.invalid) {
      return;
    }
    this.loading = true;
    const index = this.simpleFormproduto.getRawValue().index;
    // TODO: usado para fazer a requisição com a api de criação de objsct or update
    const uri = (index === null ? 'artigo/create' : 'artigo/update/' + index);
    this.createOrEdit(uri, this.simpleFormproduto, (index === null ? true : false));

  }

  createOrEdit(uri: any, formulario: FormGroup, isCreate: boolean) {

    // TODO: usado para fazer a requisição com a api de criação de object
    this.http.__call(uri, formulario.value).pipe(first()).subscribe(
      response => {
        this.submitted = false;
        this.loading = false;
        if (isCreate) {
          formulario.reset();
          this.loadListprodutos(Object(response).data);
        }  
        
        if (Object(response).code ==200) {
          this.loadListprodutos(Object(response).data);
        }
      },
      error => {
        this.submitted = false;
        this.loading = false;
      });
  }


 

  public loadListprodutos(produto) { 
    this.loadListproduto.emit(produto);
  }

  add(){
    this.produtos.push({
      principal: this.produto.nome,
      secudario: this.simpleFormproduto.getRawValue().produto_filho_id
    })
  }

}

