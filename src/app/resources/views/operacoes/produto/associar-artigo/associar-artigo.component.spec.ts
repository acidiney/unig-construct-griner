import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssociarArtigoComponent } from './associar-artigo.component';

describe('AssociarArtigoComponent', () => {
  let component: AssociarArtigoComponent;
  let fixture: ComponentFixture<AssociarArtigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociarArtigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssociarArtigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
