import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from '@core/providers/http/http.service';
import { ConfigService } from '@shared/services/config.service';
 
import * as moment from 'moment';

import { Pagination } from "@shared/models/pagination"; 
import { Filter } from "@shared/models/filter";  

@Component({
  selector: 'app-lista-depositos',
  templateUrl: './lista-depositos.component.html',
  styleUrls: ['./lista-depositos.component.css']
})
export class ListaDepositosComponent implements OnInit {
  private loading: boolean = false;

  public pagination = new Pagination();
  public filter = new Filter();

  search;
   orderBy;
  searchData;
  mostrarResultado;


  private items: any = [];

  constructor(private http: HttpService, private configService: ConfigService,  ) { }

  ngOnInit() {
    this.getPageFilterData(1);
  }

  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "Lista_Depositos-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    //this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }



  private listarDepositos() {
    this.loading = true
    this.configService.loaddinStarter('start');

    this.http.__call('deposito/listagem', {filter: this.filter, pagination:this.pagination}).subscribe(

      response => {
        this.pagination.lastPage = Object(response).data.lastPage;
        this.pagination.page = Object(response).data.page;
        this.pagination.total = Object(response).data.total;
        this.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');
        this.loading = false;

      }
    );
  }

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      return;
    }
    this.pagination.page = page;
    this.listarDepositos();
  }





}
