import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaDepositosComponent } from './lista-depositos.component';

describe('ListaDepositosComponent', () => {
  let component: ListaDepositosComponent;
  let fixture: ComponentFixture<ListaDepositosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaDepositosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaDepositosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
