import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositoCaixaComponent } from './deposito-caixa.component';

describe('DepositoCaixaComponent', () => {
  let component: DepositoCaixaComponent;
  let fixture: ComponentFixture<DepositoCaixaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositoCaixaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositoCaixaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
