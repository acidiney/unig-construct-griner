 
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from '@core/providers/http/http.service';
import { ConfigService } from '@shared/services/config.service';
import { ReportDiarioVendasService } from '@shared/services/report-at/caixa/movimento/report-diario-vendas.service';
import { DiarioVendasService } from '@shared/services/report-at/vendas/diario-vendas.service';
//import { ExcelService } from '@shared/services/excel.service';
import * as moment from 'moment';

import { Pagination } from "@shared/models/pagination"; 
import { Filter } from "@shared/models/filter";  

@Component({
  selector: 'app-movimento-caixa',
  templateUrl: './movimento-caixa.component.html',
  styleUrls: ['./movimento-caixa.component.css']
})
export class MovimentoCaixaComponent implements OnInit {
  
  private caixas: any = [];  
  private loading: boolean = false;

  public pagination = new Pagination();
  public filter = new Filter();

  constructor(private http: HttpService, private configService: ConfigService, private DiarioVendasService: DiarioVendasService) { }

  ngOnInit() {
    this.getPageFilterData(1); 
  }
  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "Lista_Movimentos_Caixa-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    //this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }
 

  /**
   * @name "Listar facturação"
   * @descriptio "Esta Função permite Listar todas facturações"
   * @author "caniggia.moreira@itgest.pt"
   * @param start
   * @param end
   */
  private listarMovimentoCaixa() {
  this.loading = true
    
    this.configService.loaddinStarter('start');
    this.http.__call('caixa/movimentoCaixa', {filter: this.filter, pagination:this.pagination}).subscribe(
      response => {
        this.pagination.lastPage = Object(response).data.lastPage;
        this.pagination.page = Object(response).data.page;
        this.pagination.total = Object(response).data.total;
        this.pagination.perPage = Object(response).data.perPage;
        this.caixas = Object(response).data.data;
        this.configService.loaddinStarter('stop');
        this.loading = false;
      }
    );
  }

  private btnImprimirMovimetocaixa(user_id:number, abertura:any, data_abertura:any, caixa_id:number) {

   // this.ReportDiarioVendasService.imprimirDiarioVendaPorCaixa();
   this.DiarioVendasService.DiarioVendas(user_id, abertura, data_abertura, caixa_id);
  }

  //--------------------------------------------------------------------------

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      return;
    }
    this.pagination.page = page;
    this.listarMovimentoCaixa();
  }
  
}
