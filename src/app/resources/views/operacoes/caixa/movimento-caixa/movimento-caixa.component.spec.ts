import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimentoCaixaComponent } from './movimento-caixa.component';

describe('MovimentoCaixaComponent', () => {
  let component: MovimentoCaixaComponent;
  let fixture: ComponentFixture<MovimentoCaixaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimentoCaixaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimentoCaixaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
