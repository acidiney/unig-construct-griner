import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@core/guards/auth.guard';

import { AberturaComponent } from './abertura/abertura.component';
import { FechoComponent } from './fecho/fecho.component';
import { ListaDepositosComponent } from './lista-depositos/lista-depositos.component';
import { DepositoCaixaComponent } from './deposito-caixa/deposito-caixa.component';
import { MovimentoCaixaComponent } from './movimento-caixa/movimento-caixa.component';

const routes: Routes = [

  {
    path: 'caixa', 
    data: {
      title: 'Caixa',
    },
    children: [
      {
        path: 'abertura',
        component: AberturaComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Abertura de Caixa',
          layout:{
            customLayout: true, 
            layoutNavigationTop: false, 
          }
        }
      },{
        path: 'fecho',
        component: FechoComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Fecho do Caixa',
          layout:{
            customLayout: true, 
            layoutNavigationTop: false, 
          }
        }
      },{
        path: 'registar-deposito',
        component: DepositoCaixaComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Abertura de Caixa',
          layout:{
            customLayout: true, 
            layoutNavigationTop: false, 
          }
        }
      },{
        path: 'depositos',
        component: ListaDepositosComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Listar Despositos',
          layout:{
            customLayout: true, 
            layoutNavigationTop: false, 
          }
        }
      },
      {
        path: 'movimentos',
        component: MovimentoCaixaComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'Movimentos de Caixa',
          layout:{
            customLayout: true, 
            layoutNavigationTop: false, 
          }
        }
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaixaRoutingModule {}
