import { CreateConstructionComponent } from './projectos/project-contructions/create-construction/create-construction.component';
import { ProjectContructionsComponent } from './projectos/project-contructions/project-contructions.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule, DateAdapter, MAT_DATE_FORMATS, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatIconModule, MatButtonModule, MatSelectModule, MatOptionModule } from "@angular/material";
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { FormatDateAdapter, FORMAT_DATE_FORMATS } from "@shared/directives/date-adapter";
import { NgxLoadingModule } from 'ngx-loading';
import { NgxPaginationModule } from "ngx-pagination";
import { ExcelService } from "@shared/services/excel.service";

import { CaixaRoutingModule } from "./caixa/caixa-routing.module";
import { ProdutoRoutingModule } from "./produto/produto-routing.module";
import { ProjectoRoutingModule } from "./projectos/projecto-routing.module";

import { AberturaComponent } from "./caixa/abertura/abertura.component";
import { FechoComponent } from "./caixa/fecho/fecho.component";
import { ListaDepositosComponent } from "./caixa/lista-depositos/lista-depositos.component";
import { DepositoCaixaComponent } from "./caixa/deposito-caixa/deposito-caixa.component";
import { MovimentoCaixaComponent } from "./caixa/movimento-caixa/movimento-caixa.component";
import { ProdutoListarComponent } from "./produto/produto-listar/produto-listar.component";
import { ProdutoRegistarComponent } from "./produto/produto-registar/produto-registar.component";
import { ProjectoListarComponent } from "./projectos/projecto-listar/projecto-listar.component";
import { ProjectoRegistarComponent } from "./projectos/projecto-registar/projecto-registar.component";
import { PlantaRegistarProjectoComponent } from "./projectos/planta/planta-registar-projecto/planta-registar-projecto.component";
import { PlantaListarProjectoComponent } from "./projectos/planta/planta-listar-projecto/planta-listar-projecto.component";
import { NgxGalleryModule } from "ngx-gallery";
import { NgxDropzoneModule } from "ngx-dropzone";

import { MultiSelectAllModule } from "@syncfusion/ej2-angular-dropdowns";


import {
  TextBoxAllModule,
  NumericTextBoxAllModule,
} from "@syncfusion/ej2-angular-inputs";

import { DropDownListAllModule } from "@syncfusion/ej2-angular-dropdowns";

import { CheckBoxAllModule } from "@syncfusion/ej2-angular-buttons";

import { GanttAllModule } from "@syncfusion/ej2-angular-gantt";

import { GanttModule } from "@syncfusion/ej2-angular-gantt";
import { GanttProjectComponent } from "./projectos/gantt-project/gantt-project.component";
import { TipoArtigoComponent } from "./produto/tipo-artigo/tipo-artigo.component";
import { AssociarArtigoComponent } from "./produto/associar-artigo/associar-artigo.component";
import { ArtigoCompostoRegistarComponent } from "./produto/artigo-composto-registar/artigo-composto-registar.component";
import { ArtigoCompostoListarComponent } from "./produto/artigo-composto-listar/artigo-composto-listar.component";
import { ProjectoMateriaisComponent } from "./projectos/projecto-materiais/projecto-materiais.component";
import { GaleryPlantasComponent } from './projectos/planta/galery-plantas/galery-plantas.component';
import { ProjectoMostrarComponent } from './projectos/projecto-mostrar/projecto-mostrar.component';
import { AutoMedicaoListarComponent } from './projectos/auto-medicao/auto-medicao-listar/auto-medicao-listar.component';
import { AutoMedicaoCriarComponent } from './projectos/auto-medicao/auto-medicao-criar/auto-medicao-criar.component';
import { AutoMedicaoEncerrarAutoComponent } from './projectos/auto-medicao/auto-medicao-encerrar-auto/auto-medicao-encerrar-auto.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
  @NgModule({
  declarations: [
    AberturaComponent,
    GaleryPlantasComponent,
    ProjectoMostrarComponent,
    FechoComponent,
    ListaDepositosComponent,
    DepositoCaixaComponent,
    MovimentoCaixaComponent,
    ProdutoListarComponent,
    ProdutoRegistarComponent,
    ProjectoListarComponent,
    ProjectoRegistarComponent,
    PlantaRegistarProjectoComponent,
    PlantaListarProjectoComponent,
    GanttProjectComponent,
    TipoArtigoComponent,
    AssociarArtigoComponent,
    ArtigoCompostoRegistarComponent,
    ArtigoCompostoListarComponent,
    ProjectoMateriaisComponent,
    ProjectContructionsComponent,
    CreateConstructionComponent,
    AutoMedicaoListarComponent,
    AutoMedicaoCriarComponent,
    AutoMedicaoEncerrarAutoComponent
  ],
  imports: [
    CommonModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgxLoadingModule.forRoot({
      backdropBorderRadius: "3px",
      backdropBackgroundColour: "rgba(255, 255, 255, 0.78)",
      primaryColour: "#20a8d8",
      secondaryColour: "#20a8d8",
      tertiaryColour: "#20a8d8",
    }),
    NgxGalleryModule,
    NgxDropzoneModule,
    NgxSkeletonLoaderModule.forRoot(),
    GanttModule,
    GanttAllModule,
    DropDownListAllModule,
    CheckBoxAllModule,
    TextBoxAllModule,
    NumericTextBoxAllModule,
    MultiSelectAllModule,
    CaixaRoutingModule,
    ProdutoRoutingModule,
    ProjectoRoutingModule,
  ],
  providers: [
    {
      provide: DateAdapter,
      useClass: FormatDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: FORMAT_DATE_FORMATS,
    },
    ExcelService,
  ],
})
export class OperacoesModule {}
