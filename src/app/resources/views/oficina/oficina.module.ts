import { CreateFrotaModalComponent } from './frotas/create-frota-modal/create-frota-modal.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { FrotasComponent } from './frotas/frotas.component';
import { NgModule } from "@angular/core";
import { OficinaRoutingModule } from './oficina-routing.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [
    FrotasComponent,
    CreateFrotaModalComponent
  ],
  imports: [
    OficinaRoutingModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    CommonModule
  ]
})
export class OficinaModule {

}
