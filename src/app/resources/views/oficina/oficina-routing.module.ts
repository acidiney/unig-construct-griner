import { FrotasComponent } from './frotas/frotas.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthGuard } from "@core/guards/auth.guard";

const routes: Routes = [
  {
    path: "frotas",
    data: {
      title: "Lista de frotas",
    },
    canActivate: [AuthGuard],
    component: FrotasComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OficinaRoutingModule {}
