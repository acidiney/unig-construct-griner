import { Component } from "@angular/core";
import { EquipmentStatus } from "@shared/interfaces/EquipmentInterface";
import { StoreService } from "@shared/services/store/store.service";
import { IDropdownSettings } from "ng-multiselect-dropdown";

@Component({
  selector: "app-create-frota-modal",
  templateUrl: "./create-frota-modal.component.html",
})
export class CreateFrotaModalComponent {
  fleet = this.reset()

  projectType = "internal";

  dropdownSettings: IDropdownSettings = {};
  constructor(private $store: StoreService) {
    this.dropdownSettings = {
      idField: "id",
      textField: "description",
      selectAllText: "Marcar Todos",
      unSelectAllText: "Desmarcar Todos",
      itemsShowLimit: 3,
      enableCheckAll: true,
      allowSearchFilter: true,
      singleSelection: true,
    };
  }

  get brands() {
    return this.$store.brand
      .findAll()
      .filter((brand) => brand.isCarToo)
      .map((b) => ({
        id: b.id,
        description: b.name,
      }));
  }


  get models() {
    if (!this.fleet.brand.length) return [];

    return this.$store.fleetModel
      .findAllByBrand(this.fleet.brand[0].id)
      .map((m) => ({
        id: m.id,
        description: m.designation,
      }));
  }

  get types() {
    return this.$store.fleetType.findAll().map((t) => ({
      id: t.id,
      description: t.designation,
    }));
  }

  get projects() {
    return this.$store.project.findAll();
  }

  get offices() {
    if (this.projectType === 'external' && this.fleet.project.length) {
      return this.$store.project.find(this.fleet.project[0].id).constructions;
    }

    if (!this.fleet.project.length && this.projectType === 'external')
      return []

    return this.$store.project.find("internal").constructions;
  }

  get placeholder() {
    return this.projectType.length
      ? this.projectType === "internal"
        ? "Manutenção em oficinas"
        : "Selecione o projecto"
      : "Selecione primeiro o tipo do";
  }

  cleanSelection() {
    if (this.projectType.length && this.projectType !== "external")
      this.fleet.project = [];
  }

  selectedOffice(officeId) {
    this.fleet.construction = officeId;
  }

  alterProjectType() {
    if (this.projectType !== "internal") {
      this.projectType = "internal";
      return;
    }

    this.projectType = "external";
  }

  saveVehicle () {

    this.$store.fleet.create({
      officeId: this.fleet.construction,
      fleetModelId: this.fleet.model[0].id,
      fleetTypeId: this.fleet.type[0].id,
      gps: {
        latitude: 0,
        longitude: 0
      },
      isDeleted: false,
      status: this.projectType === 'internal' ? EquipmentStatus.available :  EquipmentStatus.inuse,
      projectId: !this.fleet.project.length ? 'internal' : this.fleet.project[0].id,
      licensePlate: this.fleet.licensePlate,
      yearManufacture: this.fleet.yearManufacture,
    })

    this.fleet = this.reset()

    // close modal
    const closeBtn = document.querySelector(".btn-close") as any;
    closeBtn.click()
  }

  reset () {
    return {
      brand: [],
      model: [],
      type: [],
      project: [],
      construction: null,
      licensePlate: "",
      yearManufacture: null,
    };
  }
}
