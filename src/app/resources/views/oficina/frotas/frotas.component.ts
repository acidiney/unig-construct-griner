import { Component } from '@angular/core'
import { HttpService } from '@core/providers/http/http.service';
import { Filter } from '@shared/models/filter';
import { Pagination } from '@shared/models/pagination';
import { StoreService } from '@shared/services/store/store.service';

@Component({
  selector: 'app-oficina-frotas',
  templateUrl: './frotas.component.html'
})
export class FrotasComponent {
  public pagination = new Pagination();
  public filter = new Filter();

  constructor(
    public http: HttpService,
    private $store: StoreService
  ) {
    this.getPageFilterData(1);
  }

  exportAsXLSX(): void {}
  exportAsPDF(): void {}
  imprimirPDF(): void {}

  getPageFilterData(page: number) {
    if (this.pagination.perPage == null) {
      return;
    }
    this.pagination.page = page;
  }

  public get fleets() {
    return this.$store.fleet.findAll().map((fleet) => ({
      ...fleet,
      fleetType: this.$store.fleetType.find(fleet.fleetTypeId),
      fleetModel: this.$store.fleetModel.find(fleet.fleetModelId),
      project: this.$store.project.find(fleet.projectId),
      construction: this.$store.project.findContruction(fleet.projectId, fleet.officeId)
    }));
  }

  brandName (brandId) {
    return this.$store.brand.find(brandId) && this.$store.brand.find(brandId).name
  }

  translateStatus (status) {
    return this.$store.equipment.translateStatus(status)
  }
}
