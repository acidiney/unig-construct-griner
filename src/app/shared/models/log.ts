import { Deserializable } from './deserializable.model';
import { User } from './user'; 

export class Log implements Deserializable{
  _id: number;  
  user: User;  
  auditable_id: number;
  auditable: string;
  event: string;
  url: string; 
  ip: string;
  created_at: string;
  _v: string;
  old_data: any; 
  new_data: any; 
  
  deserialize(input): this {
    return Object.assign(this, input);
  } 
}
