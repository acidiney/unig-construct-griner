import { Deserializable } from './deserializable.model';
import { Permission } from './permission';

export class Factura implements Deserializable {
  id: number;
  slug: string;
  name: string; 

  deserialize(input): this {
    return Object.assign(this, input);
  }
}
