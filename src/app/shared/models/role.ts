import { Deserializable } from './deserializable.model';
import { Permission } from './permission';

export class Role implements Deserializable {
  id: number;
  slug: string;
  name: string;
  description: string;
  permissions: Permission[];

  deserialize(input): this {
    return Object.assign(this, input);
  }
}
