import { debounce, filter, map, debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Deserializable } from './deserializable.model';

export class Permission {
  id: number;
  slug: string;
  name: string;
  description: string;

  //novas variaves e instancias
  result: Observable<any>;
  queryField = new FormControl();
  FIELDS: any;
  http: any;
  total: any;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  ngOninit(){
    this.queryField.valueChanges
    .pipe(
      map(value=> value.trim()),
      filter(value=> value.length > 1),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(value =>this.http.get(this.SEARCH_URL, {
        para: {
          seach:value,
          fields: this.FIELDS
        }
      })),
      tap((res: any) => this.total = res.total),
      map((res: any) => res.result)
    ).subscribe();
  }

  
  SEARCH_URL(SEARCH_URL: any, arg1: { para: { seach: any; fields: any; }; }): any {
    throw new Error('Method not implemented.');
  }

}
