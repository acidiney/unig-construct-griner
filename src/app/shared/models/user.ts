import { Role } from '@shared/models/role';
// src/app/models/user.model.ts

import { Deserializable } from './deserializable.model';
export class User implements Deserializable {
  id: number;
  username: string;
  password: string;
  name: string;
  email: string;
  is_active: boolean;
  role_id: number;
  role_name?: string;
  user_id?: string;
  roles?: Role;
  created_at?: string;
  updated_at?: string;

  deserialize(input): this {
    // Assign input to our object BEFORE deserialize our  to prevent already deserialized  from being overwritten.
    // Atribua entrada ao nosso objeto ANTES de desserializar nossos Objects para impedir que object já desserializados sejam substituídos.
    Object.assign(this, input);
    return this;
  }
}
