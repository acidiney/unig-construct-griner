import { Deserializable } from './deserializable.model';
import { Permission } from './permission';

export class Cliente implements Deserializable {
  id: number;
  slug: string;
  name: string; 

  deserialize(input): this {
    return Object.assign(this, input);
  }
}
