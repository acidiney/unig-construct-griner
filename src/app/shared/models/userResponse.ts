import {User} from './user';
import {Token} from './token';

export class UserData {
  auth: Token;
  user: User;
  empresa: any;
  role: any;
  permissions: any;
}

export class UserResponse {
  code: number;
  message: string;
  data: UserData;
}
