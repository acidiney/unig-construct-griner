export class Filter {
  search: string = "";
  orderBy: string = null;
  typeOrderBy: string = "DESC";
  typeFilter: string = "";
  subFilter:any;

  deserialize(input): this {
    return Object.assign(this, input);
  }

}
