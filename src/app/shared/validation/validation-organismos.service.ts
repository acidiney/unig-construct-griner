import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs'; 
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ValidationOrganismosService {
  errorMessages: any;
  formRegras: any;
   

  constructor(private http: HttpClient) {
    
    this.formRegras = {
      nonEmpty: '^[a-zA-Z0-9]+([_ -]?[a-zA-Z0-9])*$',
      nameMin: 5,
      siglaMin: 2,
      dataPattern: '^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|' +
        '(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|' +
        '[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|' +
        '[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]' +
        '|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])' +
        '|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$'
    };

    this.errorMessages = {
      name: {
        required: 'O nome é obrigatório',
        minLength: `O sobrenome tem de ter no mínimo ${this.formRegras.nameMin} caracteres`
      },
      sigla: {
        required: 'A sigla é obrigatória',
        minLength: `Asigla tem de ter no mínimo ${this.formRegras.sobrenomeMin} caracteres`
      },
      accept: {
        requiredTrue: 'You have to accept our Terms and Conditions'
      },
    };

  }

  // Headers
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  } 
 

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
