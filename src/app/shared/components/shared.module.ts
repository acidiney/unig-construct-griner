import { AppButtonComponent } from '@shared/components/app-button-component/app-button.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AppButtonComponent],
  exports: [AppButtonComponent],
})
export class SharedModule {}
