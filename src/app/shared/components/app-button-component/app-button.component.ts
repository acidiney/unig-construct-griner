import {
  Component,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'app-button',
  styles: [
    `
      button {
        cursor: pointer;
      }
    `,
  ],
  templateUrl: './app-button.component.html',
})
export class AppButtonComponent {
  @Input() isLoading: boolean;
  @Input() disable: boolean;
  @Input() loadingText: string;
  @Input() loadingTextColor: string = 'text-light';
  @Input() buttonClass: string;
  @Input() type: string = 'submit';

  @Output() click: EventEmitter<Function> = new EventEmitter();

  handleClick() {
    this.click.emit();
  }
}
