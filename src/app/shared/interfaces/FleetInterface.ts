import { EquipmentStatus } from "./EquipmentInterface";

export interface FleetInterface {
  id?: string;
  fleetModelId: string;
  fleetTypeId: string;
  status: EquipmentStatus;
  isDeleted: boolean;
  createdAt?: Date;
  licensePlate?: string;
  yearManufacture: number;
  gps: GPSInterface,
  officeId?: string;
  projectId: string;
}

export interface GPSInterface {
  latitude: number,
  longitude: number
}

export interface FleetModelInterface {
  id?: string;
  brandId: string;
  designation: string;
  isDeleted: boolean;
  createdAt?: Date;
}

export interface FleetTypeInterface {
  id?: string
  designation: string;
  isDeleted: boolean;
  createdAt?: Date;
}
