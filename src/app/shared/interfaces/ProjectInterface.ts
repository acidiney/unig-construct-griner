import { AddressInterface } from "./AddressInterface";
import { ClientInterface } from "./ClientInterface";
import { EquipmentInterface } from './EquipmentInterface'

export interface ProjectInterface {
  id: string;
  client: ClientInterface;
  status: ProjectStatus;
  startDate: Date;
  endDate: Date;
  address: AddressInterface;
  designation: string;
  description: string;
  badget: number;
  internal?: boolean;
  constructions?: ConstructionInterface[];
}


export enum ProjectStatus {
  pending,
  rejected,
  started,
  done,
  paid
};

export class ConstructionInterface {
  id?: string;
  address: AddressInterface;
  badget: number;
  equipments: EquipmentInterface[];
  startDate: Date;
  endDate: Date;
  description: string;
}
