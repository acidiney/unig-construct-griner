import { FormGroup } from '@angular/forms';

export interface Methods {
    list(): any;
    show(id: number): any;
    store(form: FormGroup,param): any;
    update(form: FormGroup, params): any;
    delete(id: number): void;
}
