import { AddressInterface } from "./AddressInterface";

export interface ClientInterface {
  id: string;
  name: string;
  isDeleted: boolean;
  address: AddressInterface;
  documentId: string; // NIF
  email: string;
  phoneNumber: string
}
