export interface EquipmentInterface {
  id?: string;
  equipmentTypeId: string;
  equipmentModelId: string;
  brandId: string;
  status: EquipmentStatus;
  description: string;
  serieNumber: string;
  isDeleted: boolean;
  createdAt?: Date;
  image?: string;
}

export enum EquipmentStatus {
  broken,
  available,
  inuse
}

export interface EquipmentModelInterface {
  id?: string;
  brandId: string;
  designation: string;
  isDeleted: boolean;
  createdAt?: Date;
}

export interface EquipmentTypeInterface {
  id?: string;
  designation: string;
  isDeleted: boolean;
  createdAt?: Date;
}

export interface BrandInterface {
  id?: string;
  name: string;
  logo: string;
  site?: string;
  isDeleted: boolean;
  createdAt?: Date;
  isCarToo?: boolean
}
