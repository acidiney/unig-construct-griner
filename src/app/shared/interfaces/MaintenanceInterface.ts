import { EquipmentInterface } from '@shared/interfaces/EquipmentInterface';
import { ConstructionInterface, ProjectInterface } from "./ProjectInterface"

export class MaintenanceInterface {
  id?: string
  level: LevelMaintenance
  status: StatusMaintenance
  startDate: Date
  createdBy: string
  project: ProjectInterface
  construction: ConstructionInterface
  description: string
  subMaintenances: MaintenanceEquipmentInterface[]
}

export class MaintenanceEquipmentInterface {
  id?: string
  startDate: Date
  equipment: EquipmentInterface
  status: StatusMaintenance
  assignedTo: string
  maintenanceType: MaintenanceType
  recursiveInterval?: string
}

export enum MaintenanceType {
  corrective,
  preventive
}

export enum LevelMaintenance {
  low,
  normal,
  high,
  urgent
}

export enum StatusMaintenance {
  pending,
  doing,
  done,
  feedback,
  closed
}
