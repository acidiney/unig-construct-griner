export interface SpaceInterface {
  id: string;
  handle: any;
  name: string;
  face: number;
  area: number;
  width: number;
  height: number;
}

export interface ImageSetting {
  height: number;
  width: number;
  x: number;
  y: number;
  z: number;
  r: number;
  opacity: number;
  naturalWidth: number;
  naturalHeight: number;
}

export interface StageImage {
  name: string;
  source: string;
  setting: ImageSetting;
}

export interface StageInterface {
  id: string;
  name: string;
  image: StageImage[];
  spaces: SpaceInterface[];
  projectId: string;
}
