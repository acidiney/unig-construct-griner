export interface AddressInterface {
  state: string;
  city: string;
  street: string
}
