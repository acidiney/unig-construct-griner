export interface CityInterface {
  name: string;
}

export interface ProvinceInterface  {
  id?: string;
  name: string;
  cities: CityInterface[]
}
