import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { map, debounceTime } from 'rxjs/operators';
import { ApiService } from '@core/providers/http/api.service';
import { Role } from '@shared/models/role';
import { Methods } from '@shared/interfaces/methods';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})

export class RoleService implements Methods {

  public params = new HttpParams();
  constructor(private http: ApiService) {}
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite listar todas roles'
   * @return Observable
   */
  public list(
    search: string = null,
    page = null): Observable<any> {

    if ( search ) {
      return this.http
      .get(
        'roles?search=' + search + '&page=' + page
      ).pipe(map((templates) => (templates.object)));
    }

    return this.http.get(`roles`, this.params).pipe(debounceTime(500), map(data => data.object) );
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite visualizar os dados de uma determinada role'
   * @param id
   */
  show(id: number): Observable<Role> {
    return this.http.get('roles/' + id).pipe(map(role => new Role().deserialize(role.object)));
  }


  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite registar roles'
   * @param form
   * @returns
   */

  store(form: FormGroup): Observable<Role> {
    const role = new Role().deserialize(form.value);
    return this.http.post('roles', role).pipe(map( roleObjs => new Role().deserialize(roleObjs.object)));
  }
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite actualizar os dados de uma determinada role'
   * @param form
   * @param params
   */

  update(form: FormGroup, params): Observable<Role>{
    const role = new Role().deserialize(form.value);
    return this.http.put('roles/' + params, role).pipe(map(roleObjs => new Role().deserialize(roleObjs.object)));
  }
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite eliminar role'
   * @param id
   */
  delete(id: number) {
    return this.http.delete('roles/' + id );
  }
}
