import { FleetState } from './modules/fleet.state';
import { ResourceState } from "./modules/resources.state";
import { Injectable } from "@angular/core";

import { StageState } from "./modules/stage.state";
import { ProjectState } from "./modules/projects.state";
import { ClientState } from "./modules/client.state";
import { ProvinceState } from "./modules/province.state";
import { ProjectStatus } from "@shared/interfaces/ProjectInterface";
import { BudgetState } from "./modules/budget.state";
import { MaintenanceState } from "./modules/maintenance.state";
import { EquipmentState } from "./modules/equipment.state";
import {
  LevelMaintenance,
  MaintenanceType,
  StatusMaintenance,
} from "@shared/interfaces/MaintenanceInterface";
import { nanoid } from "nanoid";
import { EquipmentModelState } from "./modules/equipment-model.state";
import { EquipmentTypeState } from "./modules/equipment-type.state";
import { BrandState } from "./modules/brand.state";
import { EquipmentStatus } from "@shared/interfaces/EquipmentInterface";
import { FleetModelState } from './modules/fleet-model.state';
import { FleetTypeState } from './modules/fleet-type.state';

@Injectable({
  providedIn: "root",
})
class StoreService {
  constructor(
    public stage: StageState,
    public project: ProjectState,
    public client: ClientState,
    public province: ProvinceState,
    public resource: ResourceState,
    public budget: BudgetState,
    public brand: BrandState,
    public maintenance: MaintenanceState,
    public equipment: EquipmentState,
    public equipmentModel: EquipmentModelState,
    public equipmentType: EquipmentTypeState,
    public fleet: FleetState,
    public fleetModel: FleetModelState,
    public fleetType: FleetTypeState

  ) {
    this.createFakeBrand();
    this.createFakeEquipmentModel();
    this.createFakeEquipmentType();
    this.createFakeEquipments();
    this.createFakeProject();
    this.createFakeFleetTypes();
    this.createFakeFleetModels();
    this.createFakeFleets();
    this.createFakeMaintenance();
  }

  private createFakeMaintenance() {
    this.maintenance.create({
      id: "2l7484qVqtXFkhybveXJQ",
      startDate: new Date("2022-01-30"),
      createdBy: "Super Admin",
      description: "O Projecto X, precisa de manutenções urgentes",
      project: this.project.find("e3UiFaZACvqvv8FhPrLz5"),
      level: LevelMaintenance.urgent,
      status: StatusMaintenance.pending,
      subMaintenances: [
        {
          id: nanoid(),
          assignedTo: "João Silva",
          equipment: this.equipment.findAll()[3],
          startDate: new Date("2021-03-01"),
          status: StatusMaintenance.doing,
          maintenanceType: MaintenanceType.corrective,
        },
      ],
      construction: null,
    });
  }

  private createFakeProject() {
    this.project.create({
      address: {
        city: "Luanda",
        state: "Luanda",
        street: "1º de maio",
      },
      id: "e3UiFaZACvqvv8FhPrLz5",
      badget: 600000,
      client: this.client.find("Uakgb_J5m9g-0JDMbcJqL"),
      description: "Um projecto de testes",
      designation: "Projecto X",
      startDate: new Date("2020/03/06"),
      endDate: new Date("2020/06/12"),
      status: ProjectStatus.started,
      constructions: [
        {
          id: nanoid(),
          address: {
            city: "Luanda",
            state: "Luanda",
            street: "1º de maio",
          },
          badget: 600000,
          description: "Construção Casa XPTO",
          startDate: new Date("2020/03/06"),
          endDate: new Date("2020/06/12"),
          equipments: [
            ...this.equipment
              .findAll()
              .filter(
                (equipment) => equipment.status !== EquipmentStatus.broken
              ),
          ],
        },
      ],
    });

    this.project.create({
      address: {
        city: "Luanda",
        state: "Luanda",
        street: "1º de maio",
      },
      id: "internal",
      badget: 0,
      client: this.client.find("Uakgb_J5m9g-0JDMbcJqL"),
      description: "Projecto interno",
      designation: "Internal",
      startDate: new Date(),
      endDate: new Date(),
      status: ProjectStatus.started,
      internal: true,
      constructions: [
        {
          id: nanoid(),
          address: {
            city: "Luanda",
            state: "Luanda",
            street: "1º de maio",
          },
          badget: 0,
          description: "Oficina do Rangel",
          startDate: new Date(),
          endDate: new Date(),
          equipments: [
            ...this.equipment
              .findAll()
              .filter(
                (equipment) => equipment.status === EquipmentStatus.broken
              ),
          ],
        },
        {
          id: nanoid(),
          address: {
            city: "Talatona",
            state: "Luanda",
            street: "Rua xyz",
          },
          badget: 0,
          description: "Sede",
          startDate: new Date(),
          endDate: new Date(),
          equipments: [],
        },
        {
          id: nanoid(),
          address: {
            city: "Talatona",
            state: "Luanda",
            street: "Nova Vida, Rua XYZ",
          },
          badget: 0,
          description: "Armazém do Nova Vida",
          startDate: new Date(),
          endDate: new Date(),
          equipments: [],
        },
      ],
    });
  }

  private createFakeBrand() {
    this.brand.create({
      isDeleted: false,
      logo: "",
      name: "Manegotti",
    });

    this.brand.create({
      isDeleted: false,
      logo: "",
      name: "Bosch",
    });

    this.brand.create({
      isDeleted: false,
      logo: "",
      name: "Kipor",
    });

    this.brand.create({
      isDeleted: false,
      logo: "",
      name: "Pratic AIR",
    });

    this.brand.create({
      isDeleted: false,
      logo: "",
      name: "Hyundai",
      isCarToo: true
    });

    this.brand.create({
      isDeleted: false,
      logo: "",
      name: "Toyota",
      isCarToo: true
    });

    this.brand.create({
      isDeleted: false,
      logo: "",
      name: "Suzuki",
      isCarToo: true
    });
  }

  private createFakeEquipmentModel() {
    this.equipmentModel.create({
      isDeleted: false,
      brandId: this.brand.findAll()[0].id,
      designation: "Modelo Teste",
    });
  }

  private createFakeEquipmentType() {
    this.equipmentType.create({
      isDeleted: false,
      designation: "Corte & Concreto",
    });

    this.equipmentType.create({
      isDeleted: false,
      designation: "Gerador & Compressores",
    });
  }

  private createFakeEquipments() {
    this.equipment.create({
      brandId: this.brand.findAll()[0].id,
      description: "Betoneira 400 Litros",
      equipmentModelId: this.equipmentModel.findAll()[0].id,
      equipmentTypeId: this.equipmentType.findAll()[0].id,
      serieNumber: "8906094230069",
      isDeleted: false,
      status: EquipmentStatus.available,
      image:
        "https://www.dialoc.com.br/wp-content/uploads/2017/05/betoneira-01-400x400.jpg",
    });

    this.equipment.create({
      brandId: this.brand.findAll()[1].id,
      description: "Martelo 16Kg",
      equipmentModelId: this.equipmentModel.findAll()[0].id,
      equipmentTypeId: this.equipmentType.findAll()[0].id,
      serieNumber: "09067722369",
      isDeleted: false,
      status: EquipmentStatus.available,
      image:
        "https://www.dialoc.com.br/wp-content/uploads/2017/05/martelo-demolidor-gsh-16-28-220v-bosch-02-400x400.jpg",
    });

    this.equipment.create({
      brandId: this.brand.findAll()[2].id,
      description: "Gerador de Energia 62,5 KVA",
      equipmentModelId: this.equipmentModel.findAll()[0].id,
      equipmentTypeId: this.equipmentType.findAll()[1].id,
      serieNumber: "5695572562447",
      isDeleted: false,
      status: EquipmentStatus.available,
      image:
        "https://www.dialoc.com.br/wp-content/uploads/2017/05/gerador-62kva-1024x1024.jpg",
    });

    this.equipment.create({
      brandId: this.brand.findAll()[3].id,
      description: "Compressor CSI 7,4",
      equipmentModelId: this.equipmentModel.findAll()[0].id,
      equipmentTypeId: this.equipmentType.findAll()[1].id,
      serieNumber: "68726769864766935",
      isDeleted: false,
      status: EquipmentStatus.broken,
      image:
        "https://www.dialoc.com.br/wp-content/uploads/2017/05/comrpessor-74-400x400.jpg",
    });
  }

  private createFakeFleetModels() {
    this.fleetModel.create({
      designation: 'H-1',
      brandId: this.brand.findAll()[4].id,
      isDeleted: false,
    })
  }
  private createFakeFleetTypes() {
    this.fleetType.create({
      designation: 'Carrinha',
      isDeleted: false
    })

    this.fleetType.create({
      designation: 'Yace',
      isDeleted: false
    })

    this.fleetType.create({
      designation: 'Caminhão',
      isDeleted: false
    })
  }
  private createFakeFleets(){
    this.fleet.create({
      fleetModelId: this.fleetModel.findAll()[0].id,
      gps: {
        longitude: -23.286384,
        latitude: 7.94858555
      },
      fleetTypeId: this.fleetType.findAll()[0].id,
      licensePlate: 'LD-987-780',
      officeId: this.project.find('internal').constructions[2].id,
      projectId: 'internal',
      status: EquipmentStatus.inuse,
      yearManufacture: 2015,
      isDeleted:false
    })

    this.fleet.create({
      fleetModelId: this.fleetModel.findAll()[0].id,
      gps: {
        longitude: -23.286384,
        latitude: 7.94858555
      },
      fleetTypeId: this.fleetType.findAll()[0].id,
      licensePlate: 'LD-987-780',
      officeId: this.project.find('e3UiFaZACvqvv8FhPrLz5').constructions[0].id,
      projectId: 'e3UiFaZACvqvv8FhPrLz5',
      status: EquipmentStatus.broken,
      yearManufacture: 2015,
      isDeleted:false
    })

    this.fleet.create({
      fleetModelId: this.fleetModel.findAll()[0].id,
      gps: {
        longitude: -23.286384,
        latitude: 7.94858555
      },
      fleetTypeId: this.fleetType.findAll()[0].id,
      licensePlate: 'LD-987-780',
      officeId: this.project.find('internal').constructions[1].id,
      projectId: 'internal',
      status: EquipmentStatus.available,
      yearManufacture: 2015,
      isDeleted:false
    })
  }
}

export { StoreService };
