import { nanoid } from "nanoid";
import { Injectable } from "@angular/core";
import { ClientInterface } from "@shared/interfaces/ClientInterface";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

@Injectable({
  providedIn: "root",
})
export class ClientState implements StateModuleInterface<ClientInterface> {
  private state: ClientInterface[] = [
    {
      documentId: "00b0010",
      name: "Flávio Carpinteiro",
      address: {
        city: "Luanda",
        state: "Luanda",
        street: "Casa nº XYZ, 1º de maio",
      },
      email: "geral@cliente.co.ao",
      id: "Uakgb_J5m9g-0JDMbcJqL",
      phoneNumber: "222-xxx-xxx",
      isDeleted: true,
    },
  ];

  create(client: ClientInterface) {
    this.state.push({
      ...client,
      id: nanoid(),
    });
  }

  findAll(): ClientInterface[] {
    return this.state;
  }

  find(clientId: string): ClientInterface {
    return this.state.find((client) => client.id === clientId);
  }

  remove (clientId: string) : void {
    this.state = this.state.filter(client => client.id !== clientId)
  }
}
