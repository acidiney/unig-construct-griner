import { nanoid } from "nanoid";
import { Injectable } from "@angular/core";
import { StageInterface } from "@shared/interfaces/StageInterface";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

@Injectable({
  providedIn: "root",
})
class StageState implements StateModuleInterface<StageInterface> {
  private state?: StageInterface[] = [
    {
      id: nanoid(),
      name: 'Story 1',
      image: null,
      projectId: 'e3UiFaZACvqvv8FhPrLz5',
      spaces: [
        {
          area: 3636,
          face: null,
          id: nanoid(),
          name: 'Quarto 1',
          handle: null,
          height: 66,
          width: 60
        },
        {
          area: 3636,
          face: null,
          id: nanoid(),
          name: 'Suite',
          handle: null,
          height: 66,
          width: 60
        },
        {
          area: 2772,
          face: null,
          id: nanoid(),
          name: 'Space 1 - 1',
          handle: null,
          height: 66,
          width: 42
        },
        {
          area: 10602,
          face: null,
          id: nanoid(),
          name: 'Space 1 - 2',
          handle: null,
          height: 33,
          width: 54
        },
        {
          area: 1782,
          face: null,
          id: nanoid(),
          name: 'Space 1 - 3',
          handle: null,
          height: 24,
          width: 54
        },
        {
          area: 1296,
          face: null,
          id: nanoid(),
          name: 'Space 1 - 4',
          handle: null,
          height: 54,
          width: 54
        },
        {
          area: 2916,
          face: null,
          id: nanoid(),
          name: 'Space 1 - 5',
          handle: null,
          height: 174,
          width: 120
        },
        {
          area: 6399,
          face: null,
          id: nanoid(),
          name: 'Space 1 - 6',
          handle: null,
          height: 117,
          width: 147
        },
        {
          area: 4563,
          face: null,
          id: nanoid(),
          name: 'Space 1 - 7',
          handle: null,
          height: 129,
          width: 45
        }
      ]
    }
  ];

  public create(stage: StageInterface) {
    this.state.push({
      ...stage,
      id: nanoid(),
    });
  }

  public findAll(projectId: string): StageInterface[] {
    return this.state.filter((stage) => stage.projectId === projectId);
  }

  public find(stageId: string): StageInterface {
    return this.state.find((stage) => stage.id === stageId);
  }

  remove (stageId: string) : void {
    this.state = this.state.filter(stage => stage.id !== stageId)
  }
}

export { StageState };
