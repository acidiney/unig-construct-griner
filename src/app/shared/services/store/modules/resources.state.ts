import { nanoid } from "nanoid";
import { Injectable } from "@angular/core";
import { ClientInterface } from "@shared/interfaces/ClientInterface";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

export interface ResourceTypeInterface {
  id?: string;
  name: string;
  resources: ResourceInterface[]
}

export interface ResourceInterface {
  name: string,
  id?: string,
  badget: number,
  unit: string
  relatedId?: string,
}

@Injectable({
  providedIn: "root",
})
export class ResourceState implements StateModuleInterface<ResourceTypeInterface> {
  private state: ResourceTypeInterface[] = [
    {
      id: nanoid(),
      name: 'Recursos Humanos',
      resources: [
        {
          id: '0123456789ABCDEFGHIJKLMNO', // nanoid(),
          name: 'Pedreiro',
          badget: 1000,
          unit: 'h'
        }
      ]
    },
    {
      id: nanoid(),
      name: 'Parede',
      resources: [
        {
          id: nanoid(),
          name: 'Betão',
          badget: 100,
          unit: 'unit',
          relatedId: '0123456789ABCDEFGHIJKLMNO'
        },
        {
          id: nanoid(),
          name: 'Cimento',
          badget: 1000,
          unit: 'kg'
        }
      ]
    }
  ];

  create(client: ResourceTypeInterface) {
    this.state.push({
      ...client,
      id: nanoid(),
    });
  }

  findAll(): ResourceTypeInterface[] {
    return this.state;
  }

  find(resourceId: string): ResourceTypeInterface {
    return this.state.find((resource) => resource.id === resourceId);
  }

  findRelatedResource (relatedResourceId: string): ResourceInterface[] {
    const relatedResources = this.state.map((resourceType) => {
      return resourceType.resources.filter((resource) => resource.relatedId = relatedResourceId)
    })[0]

    return relatedResources
  }

  remove (resourceId: string) : void {
    this.state = this.state.filter(resource => resource.id !== resourceId)
  }
}
