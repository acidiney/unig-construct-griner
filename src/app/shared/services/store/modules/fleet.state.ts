import { FleetInterface } from '@shared/interfaces/FleetInterface';
import { nanoid } from "nanoid";
import { Injectable } from "@angular/core";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

@Injectable({
  providedIn: "root",
})
export class FleetState
  implements StateModuleInterface<FleetInterface> {
  private state: FleetInterface[] = [];

  findAll(): FleetInterface[] {
    return this.state;
  }

  find(fleetId: string): FleetInterface {
    return this.state.find((fleet) => fleet.id === fleetId);
  }

  create(fleet: FleetInterface): void {
    this.state.push({
      id: fleet.id || nanoid(),
      ...fleet,
      createdAt: new Date(),
    });
  }

  remove(fleetId: string): void {
    this.state = this.state.filter((fleet) => fleet.id !== fleetId);
  }
}
