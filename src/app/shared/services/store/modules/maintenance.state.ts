import { nanoid } from "nanoid";
import { Injectable } from "@angular/core";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";
import {
  MaintenanceEquipmentInterface,
  MaintenanceInterface,
  StatusMaintenance,
} from "@shared/interfaces/MaintenanceInterface";

@Injectable({
  providedIn: "root",
})
class MaintenanceState implements StateModuleInterface<MaintenanceInterface> {
  private state?: MaintenanceInterface[] = [];

  public create(maintenance: MaintenanceInterface) {
    this.state.push({
      ...maintenance,
      id: maintenance.id || nanoid(),
    });
  }

  maintenanceIndex(maintenanceId: string): number {
    return this.state.findIndex(
      (maintenance) => maintenance.id === maintenanceId
    );
  }

  updateStatus(maintenanceId: string, status: StatusMaintenance): void {
    const index = this.maintenanceIndex(maintenanceId);
    this.state[index].status = status;
  }

  addEquipmentsToMaintenance(
    maintenanceId: string,
    equipment: MaintenanceEquipmentInterface
  ): void {
    const index = this.maintenanceIndex(maintenanceId);

    this.state[index].subMaintenances.push(equipment);
  }

  updateEquipmentStatusFromMaintenance(
    maintenanceId: string,
    subMaintenanceId: string,
    status: StatusMaintenance
  ): void {
    const maintenanceIndex = this.maintenanceIndex(maintenanceId);

    const equipmentIndex = this.state[maintenanceIndex].subMaintenances.findIndex(
      (sub) => sub.id === subMaintenanceId
    );

    this.state[maintenanceIndex].subMaintenances[equipmentIndex].status = status;
  }

  public findAll(maintenanceId?: string): MaintenanceInterface[] {
    if (!maintenanceId) {
      return this.state;
    }
    return this.state.filter((maintenance) => maintenance.id === maintenanceId);
  }

  public find(maintenanceId: string): MaintenanceInterface {
    console.log(maintenanceId);
    return this.state.find((maintenance) => maintenance.id === maintenanceId);
  }

  public remove(maintenanceId: string): void {
    this.state = this.state.filter(
      (maintenance) => maintenance.id !== maintenanceId
    );
  }

  public removeEquipmentFromMaintenance(
    maintenanceId: string,
    equipmentId: string
  ): void {
    const index = this.maintenanceIndex(maintenanceId);

    this.state[index].subMaintenances = this.state[index].subMaintenances.filter(
      (sub) => sub.id !== equipmentId
    );
  }
}

export { MaintenanceState };
