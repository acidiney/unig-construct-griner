import { nanoid } from 'nanoid';
import { Injectable } from "@angular/core";
import { BrandInterface } from "@shared/interfaces/EquipmentInterface";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

@Injectable({
  providedIn: "root"
})
export class BrandState implements StateModuleInterface<BrandInterface> {
  private state: BrandInterface[] = []

  findAll () : BrandInterface[] {
    return this.state
  }

  find (brandId: string) : BrandInterface {
    return this.state.find(brand => brand.id === brandId)
  }

  create (brand: BrandInterface) : void {
    this.state.push({
      id: brand.id || nanoid(),
      ...brand,
      createdAt: new Date()
    })
  }

  remove (brandId: string) : void {
    this.state = this.state.filter(brand => brand.id !== brandId)
  }

}
