import { nanoid } from "nanoid";
import { Injectable } from "@angular/core";
import {
  EquipmentInterface,
  EquipmentStatus,
} from "@shared/interfaces/EquipmentInterface";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

@Injectable({
  providedIn: "root",
})
export class EquipmentState
  implements StateModuleInterface<EquipmentInterface> {
  private state: EquipmentInterface[] = [];

  findAll(): EquipmentInterface[] {
    return this.state;
  }

  find(equipmentId: string): EquipmentInterface {
    return this.state.find((equipment) => equipment.id === equipmentId);
  }

  create(equipment: EquipmentInterface): void {
    this.state.push({
      id: equipment.id || nanoid(),
      ...equipment,
      createdAt: new Date(),
    });
  }

  remove(equipmentId: string): void {
    this.state = this.state.filter((equipment) => equipment.id !== equipmentId);
  }

  translateStatus(status: EquipmentStatus) {
    const translated = {
      [EquipmentStatus.available]: "Disponível",
      [EquipmentStatus.broken]: "Precisa de atenção",
      [EquipmentStatus.inuse]: "Em uso",
    };

    return translated[status];
  }
}
