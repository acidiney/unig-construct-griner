import { nanoid } from 'nanoid';
import { Injectable } from "@angular/core";
import { EquipmentModelInterface } from "@shared/interfaces/EquipmentInterface";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

@Injectable({
  providedIn: "root"
})
export class EquipmentModelState implements StateModuleInterface<EquipmentModelInterface> {
  private state: EquipmentModelInterface[] = []

  findAll () : EquipmentModelInterface[] {
    return this.state
  }

  find (equipmentId: string) : EquipmentModelInterface {
    return this.state.find(equipment => equipment.id === equipmentId)
  }

  create (equipmentModel: EquipmentModelInterface) : void {
    this.state.push({
      id: equipmentModel.id || nanoid(),
      ...equipmentModel,
      createdAt: new Date()
    })
  }

  remove (equipmentModelId: string) : void {
    this.state = this.state.filter(equipmentModel => equipmentModel.id !== equipmentModelId)
  }

}
