import { nanoid } from "nanoid";
import { Injectable } from "@angular/core";
import { StageInterface } from "@shared/interfaces/StageInterface";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

interface BudgetInterface {
  id?: string;
  quantity: number;
  resourceTypeId?: string;
  resourceId: string;
  resource: string;
  unitPrice: number;
  totalPrice: number;
  projectId: string;
}

@Injectable({
  providedIn: "root",
})
class BudgetState implements StateModuleInterface<BudgetInterface> {
  private state?: BudgetInterface[] = [
    {
      id: "CHyJaxXsKYhHKyos95oE2",
      quantity: 1,
      resourceTypeId: "Sm5JdgBJDYbQ5FAnaXoID",
      resourceId: "0123456789ABCDEFGHIJKLMNO",
      resource: "Pedreiro",
      unitPrice: 1000,
      totalPrice: 1000,
      projectId: "e3UiFaZACvqvv8FhPrLz5",
    },
  ];

  public create(budget: BudgetInterface) {
    this.state.push({
      ...budget,
      id: nanoid(),
    });
  }

  public findAll(projectId?: string): BudgetInterface[] {
    if (!projectId) {
      return this.state
    }
    return this.state.filter((budget) => budget.projectId === projectId);
  }

  public find(budgetId: string): BudgetInterface {
    return this.state.find((budget) => budget.id === budgetId);
  }

  public remove(budgetId: string) {
    this.state = this.state.filter((budget) => budget.id !== budgetId);
  }
}

export { BudgetState };
