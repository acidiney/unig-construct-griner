import { FleetTypeInterface } from '@shared/interfaces/FleetInterface';
import { nanoid } from 'nanoid';
import { Injectable } from "@angular/core";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

@Injectable({
  providedIn: "root"
})
export class FleetTypeState implements StateModuleInterface<FleetTypeInterface> {
  private state: FleetTypeInterface[] = []

  findAll () : FleetTypeInterface[] {
    return this.state
  }

  find (fleetTypeId: string) : FleetTypeInterface {
    return this.state.find(fleetType => fleetType.id === fleetTypeId)
  }

  create (fleetType: FleetTypeInterface) : void {
    this.state.push({
      id: fleetType.id || nanoid(),
      ...fleetType,
      createdAt: new Date()
    })
  }

  remove (fleetTypeId: string) : void {
    this.state = this.state.filter(fleetType => fleetType.id !== fleetTypeId)
  }

}
