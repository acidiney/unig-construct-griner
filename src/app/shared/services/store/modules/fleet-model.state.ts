import { nanoid } from 'nanoid';
import { Injectable } from "@angular/core";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";
import { FleetModelInterface } from '@shared/interfaces/FleetInterface';

@Injectable({
  providedIn: "root"
})
export class FleetModelState implements StateModuleInterface<FleetModelInterface> {
  private state: FleetModelInterface[] = []

  findAll () : FleetModelInterface[] {
    return this.state
  }

  findAllByBrand (brandId) : FleetModelInterface[] {
    return this.state.filter(model => model.brandId === brandId)
  }

  find (fleetModelId: string) : FleetModelInterface {
    return this.state.find(fleetModel => fleetModel.id === fleetModelId)
  }

  create (fleetModel: FleetModelInterface) : void {
    this.state.push({
      id: fleetModel.id || nanoid(),
      ...fleetModel,
      createdAt: new Date()
    })
  }

  remove (fleetModelId: string) : void {
    this.state = this.state.filter(fleetModel => fleetModel.id !== fleetModelId)
  }

}
