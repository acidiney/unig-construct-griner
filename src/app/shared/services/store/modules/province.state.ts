import { nanoid } from 'nanoid';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ProvinceInterface } from "@shared/interfaces/ProvinceInterface";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

@Injectable({
  providedIn: "root",
})
class ProvinceState implements StateModuleInterface<ProvinceInterface> {
  private state?: ProvinceInterface[] = [];

  constructor(private http: HttpClient) {
    if (!this.findAll.length) {
      this.loadAddress();
    }
  }

  private loadAddress() {
    this.http
      .get<{ name: string; counties: string[] }[]>(
        "https://buscador.ao/provinces"
      )
      .subscribe((provinces) => {
        const provincesMapped = provinces.map((province) => ({
          name: province.name,
          cities: province.counties.map((city) => ({
            name: city,
          })),
        }));

        this.createMany(provincesMapped);
      });
  }

  public create(province: ProvinceInterface) {
    this.state.push({
      id: nanoid(),
      ...province,
    });
  }

  private createMany(provinces: ProvinceInterface[]) {
    this.state = provinces;
  }

  public findAll(): ProvinceInterface[] {
    return this.state;
  }

  public find(provinceName: string): ProvinceInterface {
    return this.state.find((province) => province.name === provinceName);
  }

  remove (provinceId: string) : void {
    this.state = this.state.filter(province => province.id !== provinceId)
  }
}

export { ProvinceState };
