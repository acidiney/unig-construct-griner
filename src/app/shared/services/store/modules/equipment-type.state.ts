import { nanoid } from 'nanoid';
import { Injectable } from "@angular/core";
import { EquipmentTypeInterface } from "@shared/interfaces/EquipmentInterface";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

@Injectable({
  providedIn: "root"
})
export class EquipmentTypeState implements StateModuleInterface<EquipmentTypeInterface> {
  private state: EquipmentTypeInterface[] = []

  findAll () : EquipmentTypeInterface[] {
    return this.state
  }

  find (equipmentTypeId: string) : EquipmentTypeInterface {
    return this.state.find(equipmentType => equipmentType.id === equipmentTypeId)
  }

  create (equipmentType: EquipmentTypeInterface) : void {
    this.state.push({
      id: equipmentType.id || nanoid(),
      ...equipmentType,
      createdAt: new Date()
    })
  }

  remove (equipmentTypeId: string) : void {
    this.state = this.state.filter(equipmentType => equipmentType.id !== equipmentTypeId)
  }

}
