import { ConstructionInterface } from "./../../../interfaces/ProjectInterface";
import { nanoid } from "nanoid";
import { Injectable } from "@angular/core";
import { ProjectInterface } from "@shared/interfaces/ProjectInterface";
import { StateModuleInterface } from "@shared/interfaces/state-module.interface";

@Injectable({
  providedIn: "root",
})
export class ProjectState implements StateModuleInterface<ProjectInterface> {
  private state: ProjectInterface[] = [];

  create(project: ProjectInterface) {
    this.state.push({
      ...project,
      id: project.id.length ? project.id : nanoid(),
    });
  }

  findAll(): ProjectInterface[] {
    return this.state.filter((project) => !project.internal);
  }

  findContruction(projectId, constructionId) {
    const project = this.find(projectId);

    return project.constructions.find((office) => office.id === constructionId);
  }

  find(projectId: string): ProjectInterface {
    return this.state.find((project) => project.id === projectId);
  }

  addConstructionToProject(
    projectId: string,
    construction: ConstructionInterface
  ) {
    const projectIndex = this.state.findIndex(
      (project) => project.id === projectId
    );

    this.state[projectIndex].constructions.push({
      id: nanoid(),
      ...construction,
    });
  }

  remove(projectId: string): void {
    this.state = this.state.filter((project) => project.id !== projectId);
  }
}
