import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { map, debounceTime } from 'rxjs/operators';
import { ApiService } from '@core/providers/http/api.service';
import { HelperFunctionsService } from '@shared/services/helper-functions.service';
import { Cliente } from '@shared/models/cliente';
import { Methods } from '@shared/interfaces/methods';
import { HttpParams } from '@angular/common/http';

export class A  {

}
@Injectable({
  providedIn: 'root',
})

export class ClienteService extends HelperFunctionsService implements Methods {
 
  constructor(public http: ApiService) {
    super(http);
  }
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite listar todas clientes'
   * @param httpParams 
   * @return Observable
   */ 
  public list(httpParams: HttpParams = new HttpParams()): Observable<Cliente> { 
    return this.http.get(`clientes`, httpParams).pipe(debounceTime(500), map(response => response.data));
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite visualizar os dados de uma determinada cliente'
   * @param id
   */
  show(id: number,httpParams: HttpParams = new HttpParams()): Observable<Cliente> {
    return this.http.get('clientes/' + id, httpParams).pipe(map(cliente => new Cliente().deserialize(cliente.object)));
  }


  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite registar clientes'
   * @param form
   * @returns
   */

  store(form: FormGroup): Observable<Cliente> {
    const cliente = new Cliente().deserialize(form.value);
    return this.http.post('clientes', cliente).pipe(map( roleObjs => new Cliente().deserialize(roleObjs.object)));
  }
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite actualizar os dados de uma determinada cliente'
   * @param form
   * @param params
   */

  update(form: FormGroup, params): Observable<Cliente>{
    const cliente = new Cliente().deserialize(form.value);
    return this.http.put('clientes/' + params, cliente).pipe(map(roleObjs => new Cliente().deserialize(roleObjs.object)));
  }
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite eliminar cliente'
   * @param id
   */
  delete(id: number) {
    return this.http.delete('clientes/' + id );
  }
}
