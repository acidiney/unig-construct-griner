import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, debounceTime } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { ApiService } from '@core/providers/http/api.service';
import { Methods } from '@shared/interfaces/methods';
import {HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})

export class LogService implements Methods {

  public params = new HttpParams();
  constructor(private http: ApiService) { }

  /**
   * @author 'caniggiamoreira@gmail.com'
   * @description 'Permite listar todos os los'
   * @return Observable
   */
  public list(
    search: string = null,
    start: string = null,
    end: string = null,
    page = null): Observable<any> {

    if ( search ) {
      return this.http
      .get(
        'logs?search=' + search + '&start=' + start + '&end=' + end
      ).pipe(map((organisms) => (organisms.object)));
    }

    return this.http.get(`logs`, this.params).pipe(debounceTime(500), map(data => data.object) );
  }

  /**
   * @author 'caniggiamoreira@gmail.com'
   * @description 'Permite visualizar os dados de um log'
   * @param id
   */
  show(id: any): Observable<any> {
    return this.http.get('logs/' + id).pipe(debounceTime(500), map(data => data.object) );
  }

  store() { }
  update() { }
  delete() { }
}
