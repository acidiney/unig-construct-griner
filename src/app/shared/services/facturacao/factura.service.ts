import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { map, debounceTime } from 'rxjs/operators';
import { ApiService } from '@core/providers/http/api.service';
import { HelperFunctionsService } from '@shared/services/helper-functions.service';
import { Factura } from '@shared/models/factura';
import { Methods } from '@shared/interfaces/methods';
import { HttpParams } from '@angular/common/http';

export class A  {

}
@Injectable({
  providedIn: 'root',
})

export class FacturaService extends HelperFunctionsService implements Methods {
 
  constructor(public http: ApiService) {
    super(http);
  }
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite listar todas facturas'
   * @param httpParams 
   * @return Observable
   */ 
  public list(httpParams: HttpParams = new HttpParams()): Observable<Factura> { 
    return this.http.get(`facturas`, httpParams).pipe(debounceTime(500), map(response => response.data));
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite visualizar os dados de uma determinada factura'
   * @param id
   */
  show(id: number, httpParams: HttpParams = new HttpParams()): Observable<Factura> {
    return this.http.get('facturas/' + id, httpParams).pipe(map(factura => new Factura().deserialize(factura.object)));
  }


  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite registar facturas'
   * @param form
   * @returns
   */

  store(form: FormGroup): Observable<Factura> {
    const factura = new Factura().deserialize(form.value);
    return this.http.post('facturas', factura).pipe(map( roleObjs => new Factura().deserialize(roleObjs.object)));
  }
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite actualizar os dados de uma determinada factura'
   * @param form
   * @param params
   */

  update(form: FormGroup, params, subPath:String=''): Observable<Factura>{
    const factura = new Factura().deserialize(form.value);
    return this.http.put('facturas/'+subPath+params, factura).pipe(map(roleObjs => new Factura().deserialize(roleObjs.object)));
  }
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite eliminar factura'
   * @param id
   */
  delete(id: number) {
    return this.http.delete('facturas/' + id );
  }

  anular(form: FormGroup, params): Observable<any>{
    return this.update(form, params,"anular/");
  }

}
