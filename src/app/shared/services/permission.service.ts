import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { map, debounceTime} from 'rxjs/operators';
import { ApiService } from '@core/providers/http/api.service';
import { Permission } from '@shared/models/permission';
import { Methods } from '@shared/interfaces/methods';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})

export class PermissionService implements Methods {
  public params = new HttpParams();
  constructor(private http: ApiService) {}

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite listar todas permission'
   * @return Observable
   */
  public list(
    search: string = null,
    page = null): Observable<any> {
    if ( search ) {
      return this.http
      .get(
        'permissions?search=' + search + '&page=' + page
      ).pipe(map((permissions) => (permissions.object)));
    }

    return this.http.get(`permissions`, this.params).pipe(debounceTime(500), map(data => data.object) );
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite visualizar os dados de uma determinada permission'
   * @param id
   */
  show(id: number): Observable<Permission> {
    return this.http.get('permissions/' + id).pipe(map(permission => new Permission().deserialize(permission.object)));
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite registar permissions'
   * @param form
   * @returns
   */

  store(form: FormGroup): Observable<Permission> {
    const permission = new Permission().deserialize(form.value);
    return this.http.post('permissions', permission).pipe(map(permissionObj => new Permission().deserialize(permissionObj.object)));
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite actualizar os dados de uma determinada permission'
   * @param form
   * @param params
   */

  update(form: FormGroup, params): Observable<Permission> {
    const permission = new Permission().deserialize(form.value);
    return this.http.put('permissions/' + params, permission).pipe(map(permissionObj => new Permission().deserialize(permissionObj.object)));
  }
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite eliminar permission'
   * @param id
   */
  delete(id: number) {
    return this.http.delete('permissions/' + id);
  }
}
