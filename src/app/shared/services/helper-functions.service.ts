import { Injectable } from '@angular/core'; 
import { finalize } from 'rxjs/operators';
import { ApiService } from '@core/providers/http/api.service';  

export class ObjectData {
    objects: any =  [];
    loading: string = 'Carregando...';
}

@Injectable({
  providedIn: 'root'
})
export class HelperFunctionsService { 

  public types_client = new ObjectData();
  constructor(private api: ApiService) {} 
  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite listar todas findAllTypeClient'
   * @return Observable
   */
  public findAllTypesClient(){ 
    this.types_client.objects = [];
    this.types_client.loading = 'Carregando...';
      this.api.get('cliente/tipoCliente/listar').pipe(finalize(() => {this.types_client.loading ="Tipo Identidade"})).subscribe(response => { 
        this.types_client.objects = Object(response).data;
        console.log(this.types_client);
      }, error=>{ 
          this.types_client.loading ="Tipo Identidade";
          console.log(error);
      }); 
  }

}
