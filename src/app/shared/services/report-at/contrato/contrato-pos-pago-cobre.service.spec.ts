import { TestBed } from '@angular/core/testing';

import { ContratoPosPagoCobreService } from './contrato-pos-pago-cobre.service';

describe('ContratoPosPagoCobreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContratoPosPagoCobreService = TestBed.get(ContratoPosPagoCobreService);
    expect(service).toBeTruthy();
  });
});
