import { TestBed } from '@angular/core/testing';

import { DiarioVendasService } from './diario-vendas.service';

describe('DiarioVendasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DiarioVendasService = TestBed.get(DiarioVendasService);
    expect(service).toBeTruthy();
  });
});
