import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from '@core/providers/http/http.service';
import { ConfigService } from '@shared/services/config.service';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';


@Injectable({
  providedIn: 'root'
})
export class FacturaPosPagoIplcService {

  public pdfEvent = new EventEmitter<Object>();

  constructor() { }

  /*
  public facturaIPLCPDF(id, via) {
    this.http.__call('factura/gerarFactura/' + id, null).subscribe(
      response => {  
        const dados = Object(response).data; 
        this.imprimirFacturaIPLCPDF(dados.factura, dados.produtos, dados.cliente, dados.user, via, dados.linha_pagamentos, dados.servico_conta);
      }
    ); 
  }
  */

  public imprimirFacturaIPLCPDF(factura: any, produtos: any[], cliente: any, user: any, original: any, pagamento: any, servico_conta: any, tecnologia: any) {

    var doc = new jsPDF();

    var imgData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAN0AAACVCAYAAADcz/8bAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAxOToxMDoyOCAxMDowNzo1NBYtb+cAAFx/SURBVHhe7b0HnF1HdT9+3n113/YqrXrvsuRe5G7HxjYlBDAQisEkBAghtDi0hOT3TyAhlD8QEsCBFHpoNmCMjY2L3OUiq1pdK62k7eXt6++W3znzPfP2ScggQF5s/e5Xn9HM3nfv9DnnzJmZM5GAQSFChJgyOOqHCBFiihAOuhAhphjhoAsRYooRDroQIaYY4aALEWKKEQ66ECGmGOGgCxFiihEOuhAhphjhoAsRYooRDroQIaYY4aALEWKKEQ66ECGmGOGgCxFiihEOuhAhphjhoAsRYooRDroQIaYY4aALEWKKEQ66ECGmGCfNXMOpYvUhEkS4MPqHBT86HuRV+GX+xH5k6ViE/0Wr4WOrJ1KTTO1v8hyYfOhoBvyySxStmLAT9cnjf4JA0/EpweG4Ccf4L0FU3rEZjTg1aVUTqsKR96qP8b3JRzBZpppsGQTOMQ9OIUQmG+OkIhx0x8J20GdD9ecAAxRBOEHN59U64WfVxxEdKI4MHnTmSHWgyqCo6eAKbiQEPB4I9vfo5O+Tg7926GPQ8HA3PiAv1v5toRHwoLS/B3ZAB+xHMKgjQYwCfzJdQSQcdL8xbAuHCBFiihByumNxPE7HHMRyKGY36rs1HCrGzoatSPkcwdV6lvywiHk0avNu83EsXbXf6LtHiZRFdrZ89qGkZ+Ng0TVAvNXmrk3yFEPI6UKEOEXw/zSnO16eA1E2aNgSOnliZ0uR6lzJr74n87BSEX9VKvjd92I0PgqukRnzaGhoAuFszviFcpHKbtmES6WC8QPmnomEpuN4nIJrwsWxQePPnd1BV117qQnHYjZ14U1QnuTyPvkVPB8aRNw7numlfAFpL1k2l7pntJlwImk8TselchnvJoIEfw9FTVTnjKm6BCXSwsmPheWIUfI8GwaEQzxXXGIqESpSfgvUVprNn/jH5rW2k3jsW/ZfQv/jgcSDBGOFRobQQTOZPPXsHzfhAz1ttHVzvwnv2rXP+J4bo2wG6ci3uZwOIB5sgki8TIkUEognMejqGsoUOIjTj4zx/3i3uWHY+Ndcewa9573vNOFDvT5tfAppPrj+iPE3PDxApVyjCWcLo8YfHt1L07rx7IqrzqUly7tMePlqDL6Zc6JMFJBO5lAdPfFQrwnv3oU4fR5PbR0YoTPnx2jJqmYTXrakyfhdbR41N9WbcMQoYphIOY5xL3Q8V4MuFC9DhJhi/D/B6SRvvj+pdIhGj1Z2+EzOPVHHM3Jln4pFhA/15o0/PODT2BDoU+9+sLzMmM/cDmLVoXGXOZhyrVjC+IJ0usH4qVSKwykTTkTxeyIaI/tqRKW3SDxPJRfcq70rwlyp04QXLQQnKUz4dM9de014++Yi7XwGXHffXnCqSqWBmpo6TLhzmqaXilI8gfKW3TyLklkTTiRRNw1NcWpsSJtwnEXKUr7OhAf78Htvzwjt27/LhCuVA9TUiXZubDpg/L98eyv91fv+zISt8ub52Bd+GzxXnO4FNOhsBZx4OnagSeVNijsBd7ySCdm5TLFY5HkVxL/xXJqyOcyRDveOGH98xOcOjxGSz8J3ggaKp/AepYtVnWB9PTpwjAeSq1MdWeviYY9wRcuRd6hQQD6Kbsb4za1Es+ZhgM2b30AxjX4U0iXd+/MjtG0z5neRyAzK5zGwIhG8mKqPUxDD4K+Ukc/MuBAcpBl4ERobxe8ljFMuN9eHytHF4h5+F4PSdZFfpg1cWMwJy34fzwFF7CU6+1yIp5/8+DpatWK+CVu6JtsCOFP44wWMULwMEeIUwe+V08nyV5WBHRfygo1XfbOOZmnF5H4LuzPCNzsoTJDfxXsOT/DLZbCdkZERpujgcPE45Drhgr4P0WhgqIlGR0GyhwbB6TzmEFEH3CQRBycL+JWSKkUcjicIEFexCK7he5Im8iTKvbKKn0EF+Yi5FSoWoEDp7IJIOGtOE4ttJmjKcbgPLK63F98e6k3SxDjKVFfXypwSBVWmZMrlxJFmsQjlxygzprLms1LJ8jfKtSp4FmVq3qyJdjS6NKMb5Zw5G/UxewHR9FkmSK2dLtU1IP3586cbv05F498JUpk1fsB5MtxSwrpuKlzHtjWAv4LjdKBj3/xtEXK6ECFOEfxeOF2VEgllq+4AmeReYIEC4Qqg8hRgzkU+v+djss/srbpaFtjdGVGZPyGuCZ7PCEaHSzyXAYVPJlNUn8a8qVTCN9lcnvI5zK8K+XrmjJgrVTxwohhzj2gM7zpmz6Qk7Zs1LkEs7pttkQK3hPK4lThlM/h9bLRklhAEXR2Wlfn00588ZYJz56wxfjrdRoUKuHDFK5l8CcpaB06ilfwiVPayLljWNcF8Ht9MZHJcH3i3UganqlRcamrGnLGpfYjmzkP+Zs5CHc6c1kHz5oFrtXYkyIlqPXuIxy16XA7Enxv3aGJM05qA3zdU5LIjH6k6xN3Q5FB9I9qgvjHCDnmpT4H7djW3U1OTKpQS8h7aSeoUAf7bRz4q2kaRWJKcmCpq5G/tLrb7oB8gnig/1Me/E04pRYpdYOaZPTtUZNUXVMvKcQY66OxiLIuRno8BIinGYhhAZR+NODTk047t0DAePtBn/Mb6FItjOlAZyQTetRpLyXsiiWc+lTheHUwxZKSzq4EatOMkUmjtdH2MO4wJcnYrk4Pfh3gm/bBcwrOJcemg+L61Fb+LQuPLX7jLhLPjEC+bGudTJIryRGMpHjBIv1DG4Pdcj4kCBm+pPMSpHjLhaAz10dhUT63tIBjTWluNP3dmmmbNR0anz0hQXRp5cvEJlXJxGuxH3fZw3T2z+7AJD/RBoTLUX6LBAQww0diOjkIszWaQt8GBGOdJ8xeASDixHBMirBPWNZeoCToXLh9+b2/KMqHBeuH1r3wZLdRBn06iPdpbk1SvA5iS2u5MdAPfyrKy8RrlcLSNgoi0me1XUl7UM/+h/m+OULwMEeIUwe9JkQKKFBGOogqISY7HzhIY8ZXT+aq2FooWJeyw8ChBhw7ju6eeGDL+7p3j5AdQdjSmwLHamh0W3SDa1NXHqa0d3MSuIni+iKR4N5UuVblFOg0OUZeOUkIpKjIo/7tcCnwTMVzYPrcc2+EQqK0IO3bCb8U/rnjq2Q/Kv2cn2E7PgQr1HQGXzuVkmQP5dCsou6xQtLWCK3bPTlDXTMTV1oF02tuauXwmSAnNrmTbKlpGRnzq2Yf4D+wH9zrU49Hhw5r+oEPDGcTpKcceGS1T2UV9+H6auS7KWSqg8vxCE4u6qIdSGWJsEExQNAmOGETHmBWDa/ouJI9E/BBd9+IzTfj1bzyHFs1HWnVx5GOkn/Po4tnMbrTBrHnS5raPBcz4wF0j9ohTwO+LE4jSxTbX74BTSryUrieIyE756vkxO+i4oPYRi5+ej8p1ougkRUrR3p34/tbvjND99/aYcHsnBlpHd0Az5mJQzp+B+c+sac0sekG8dGIVKpYh+vgBOkZTcx21t+PdpsY4dwpUtq/5dN0yxaLIVCSCZzhvZsucNANLYH0RN30PnVHW6OwWKY5NfY9iDvJpz9ANjbi0dTu2YR3pG6YWFRHTPC8StDQ1UXO6xYQrFR4gI6ibgT6snYnoF4+DuMRjmDvu3pnh3xF/3xGHBnhgCTwfaY9PlHj+qAOJ5092Tc9XIihLmdmcEgonyWXC85IcqGXIep7V3AaeltHM8fCNzM8cXYiP8uxUkIwVaM5cxNPePUKXXDHNhM87TykGz/X7juDdx9Y/YvzL1q2lVWtQpqUrmiiu/cEn1AGTJIq4KLvpP1W9wG+PULwMEeIUwe+V05HhGr/8naX8QmmspvPICKjOf/7XLvrOV8Ddtm4rU3cXRIrX34hdEedf0kR1DRBJOhpBzZvrE5TLYyNx2Z2gZlVmWDGzqTHBaYFyO8K1qmfGkHZEqKZyOKNxrUIpoRVrasHv29PXKCO+s6fNobjV7x2kJ+uJyh+o6BWYmiN9h5DP793SQxsfAXd2/Jk0PgJqf7gXnHt8rEj1XBZBTNcgs1mXyhVwgDGugrxqVxOqWCpxOpEo8hnxcyzCoiwxB99XihWq6I4VMRthTDowkDOiTKnEkgPCgQtpo1hi3u7iPTmB4KpEUcvlq32ARKRUrY6IooyOmQGde/FqE466kECKw0T7Dmw04fMubKL3vP9sE16zDHXDuai2WyISM3n9TXC8/htyuhAhThH8XjidneTKXCeo0nbLFWIs+4Nac/ZooypIvv61g8Z/8IE0KdOiRDJLf3nTchO+4lpLZXOUn0ACMd2TGHN8yhXwUQfP/WZ0YyOyPSOHeQGocMzneKxy50RhTpUfW37+uzqvEN9ySo1bFBW6zhdYuylcck/WIRkec167DhgllO0z/7qB7vop9l7On3kOecpNynoGTt6PJaAocSvgGhGnQm2d4AbNrUlKN6BO6hsRZ7JONmMjT8k6jxrS4Bb1uq80ztQ+nQSnjEk55KwPw5asxBzRrndaFPMuc1jMtTITZRodg8JofBh5O7hzmPoHwN16D+do30HkdTyHWEt5SQNl75h9gfFndJ5G/YfQhv39j9Ly1fj+rz600vjXvmgmNWG6SzGWlOIR24dODFPJ6X4/g05Zv2tENhUtdNDFuIMV8+h4D907QT+7FYqFvT2IP+/H6LQz0ImuvLqDFi3Bc7eEhvXKEUo6mHBXfDwjnnRPm4YWaWqKcvroJH6gclGkwjnCs1jAHaw66Cx1qK384wgHRjQ9tvw13xzveym7c7TY5QcJrkdoClnW5ByhTqyotOOZHP34B/tNOJVopZYWdKzOLgwKOdTQ1onB0tiKdCIxnxobdQCxp9IjEyTEGTemJiykDEjT5l9qpayKFlmv9lQVWtE1zoLZ3qZx6Q5tj993OP+CeEJOU2Aga9IkJbRWJ0Z5UB5URdDBgyAeu3cXaPtWEJc773na+JnsNMoNom48s63tGRPunoVzf1/+jzfTdVdjk4HnV6qE++hWObaNJnG8ARaKlyFCnCI4iZxOKN+vowz4HTRSaCqHmMsIojr+ZS3pwftA5Z54oED5MaiTI3UQV+atKdNlV2IHbjw5QaWMKhZ0XSedqOe8IE7fwfpQx7RGpva61sS/BcrhHEc5CWfLcpUocxoxNQdo1Yi6v8qtasuoNMtU4dFlP2qZwERzzPciUlY5HdIWBZKvnM6JRMnXuglUERHxU0zlVWySaDT68XHUTS5boXxR1/ns0R4WtT3d7B0N4uRq2CvBT8YSZI9A5St55VyyHQ7PyuUolbVuK+yXWJIQ6GoIVQrlajgWg3JGtp5ZhVM07lM0hrR8lTwa0h61dyJ/bZ1JmjFLzwB24vvWNpk6mCD1DKA8R0aytGszwps2HKCONrTNdddAgbZ4catZ7hFEuA7tDiGrIMM6MDjuZA8UH+VxVJlVi5DThQhxiuDkcTqjEFHKUOUKMqaPRy2QpC/KApX0x8ZBfe77RS8d7sU3I6MVSqRAEecuAOlbtryF5zP4pshUNqZHehrqQD98f4JleuyMaG2Furm9o5U5COIPglJVQWGWAowvxnUQjvIEvJpjS+lqKV51ycCnsh7TcSLJKlWsxs3cyypI5LfJGJSEM9ewFV81elS7oGvCiEt1F/wszlxDy8nu7nu3m/CPb8Ep7oSznDkTuHRRD8jK9lbXxTcBp5nVjcp6soe5lFM9bDvOfkE3UZf09HyZOZtXAReQOafvKkewyzpuhd9BnPG4/sYcOqb5jMYiXGVIoFJdGshVpQxR8xvlFaMuDk7X3BChxia8u2AlpJV1V9TRwoWQelpb66lbd6o0anX+MpD/CmE5JfCTPI+FAq26/CP1q8sMk5ZxJvFccbqTPOiORW2mJWyTwnPXj5BuqqctWyBSHuypUCaDjpPljrNgMTbDds9AJQdeicUk/C4iWFQ7eWM94g4oQ/X1qMC2duzoEFTzJ6cDEGLfipFyng7fyymCycpGmrJLw/4uph0EiUSMxShkvrYKrZRitpjZQS3/Vd/RNEV7qY/EMjMC3BmUiIjIC5GdBwYTCoGkE4miY0o8992Hzcn/fTM0uwk6i8pFxF/STeFepEDFPMpeFlMUedSXKjyNBtQSnGzRYxHSBMnVzdY+D0oZeAJ511VrY65uE8ubNUDb9pP1OSnKSYT2Od6LpyYoplbPfB7xnvYBWacUBJEc9wuYrSDd1N1QX6Z0E+qjqztC8+ZjoM6ah4G0ctVM7iN4tmRuMy1e0G7CYn5eEJXTIaoZtqdUoKDSApsdRyinRShehghxiuAkKlKYA1TZtoX8baMXH79HlIOMZwIa1fNZA/2gzKMsDYyrqNnc3kz1zaCYUQfvpeJMn/SIR4zZQUo17BHmcALZaNHejiWDpB7DASw1Fspmnz+rbPKsEEWMIGBRyRJCx4jIx8YFjghIefD3ZG2I6shygBOlqLVcQzgT/B/9ACLU3bf3MydDPiK6zuYxZ7ccRBQ7CRXNrUpf0jZ7YBnxSJbFa4St3RURv4oFcINyyTMivaBQxu/9o63chpO/C4oFDmvestl89ZT6ZOmlTbUNWDSI6FJDTPMWTfJ7MSiHnADiZYy5vavc2+VnfgRrdl4Ep/tjyTx/j7zNbhikc1dCwXLJJecZ/1WvPZOaMNtgzoo6jIokobtggqj0GfRLm8/nv3j5rIPOPhOxDkmVVUQ5cqRIsTgGyMgQnj2zo4+am3HWKlmfICdhOwEqvD6ZYNkcHUvONMa0cXwflTdvThcXCg26dcuA8RN1SaprQDwUKfIAxkDOjKMzTEyIlSyE+/tHaHgQWjLbceQQ6+q1i034vAuWGT8WK/IARDzZ8RRt34r1s1GhGozOrk4uB8rW0JioEgBrIKlQDHgeiw6V09ETsPiTz6OcQ4OjLALqAFciI4MmYHFRMG9xC5191hITziIa2vBYL8//EH9LF0SoVNph0Qz1leS6SyS1w9k+z75VssZ47KBE7NuAQHuI9FHbW1Ta5jlgQPkc2riic9x8rsJ1iDIN9meotxdrrbt2I+9PbfRpYAiZHue6Jwd5LagYW5B9ZVYlGsdASDa1UaoOoqTMz8u6EZ6i8INohsp5tPfaOU104eoFJmxPYJx2WiNdcflSE25pURGd56PW6BLFG7mYSmie40Gn1R0iRIipwnOsSGFo9EKlJyZAVTIT4CTpdAcTNMiHTz+NiXM+H2ExAEeNg2iE1B4QJROgfA3pGJV0DUkOe8fiECs7OkHB21uaaWwY1O+H34WNyIce3kp/cB24wmmnz6xyk/37oIjIZMZpbEyNAB0co94exFkqaN6jFZq3EEqZt//59cZfvKCTqbUJ0oYHsvTtb/7YhCe0bJdffglTVMgzzW1xzifyFE/i2dBAHe3cAQ4wNABRyYnUUb/uzug92Fddf7MmIuKpCg2N7jThF798If3LZ280YVXcGvzmtLlWGqntCpPi59HhY6EcycCyx19Ny7O5gAZGwAlHx5nTqSJpYAh1t3f/GHNDaGYffgDtsrMnTa6uITLLYw6u3CpuOWKZxVq8u3RaI336H15nwitWglNueOAARfQ40ouvg9SSkG9VOnO4M1ktskXI6UKEOEVwEjmdcA9LGTCW5VBjROcwomnftxfq30QS3K2trZ2e2Q7KfkQPLYppuVhSKabZzYBwPIbfE/wsqTsfIlSm5lZwrenTMW9x+F8ejIr+6z82G/8b3/kxfeyTbzThSy+aR4VjCLsQNGtYSE7E2KmpXc4QZc3u/TphV87b3NBGPXtBeQePiPIA85l6teQsy4u6fGWOvhTLKGdJTzyXg2R1o3C5iDqKRZq4nnT+xnmz38vUQyDzsIFhcMe5C8fpDW/GZt8EPjGwVFSXwcDEqoyKI63Ou+0ct4ZTiW0RnadW8WuIvV2LPBqinLE5qY3Avit+bdjC5q2Omwbf79iHwj/x1Dg99TTKfv+D22jHHuy5LKrBp2iywSxBCfziEF177UwT/ud/eoPxB3sidN9t20z4vDOxnHDFVZ0sxWCXTMRJsVBmKxId4/mvSCGZGNsGA0snP1qt8v4jBTp8CB2vvQ3iY5Yn4DueQWdOp7HwKTYk42k0RCReoYgusibieFbHUcd14l0sTNDMOfi9qx0pyZXDntp8/MlPIG6sf2gjXfoiNMKM2XGuTIycCdWSykJyNgcFSCHvUS6LKrEWwqKxPM1eAKXIxRetM/5D9/fQzu1YW1x38UqKRzHY3BLi7u3J0aFemCfITGRp+gyIp3WNaNhYQ4zFXHT4jN7uE3j1nD7qMD8RVBU5ro46P3CpUICy4PSzonTDm1eZcEwHjtSAPa9nlUl4qjADxDa37eDs21d8rrejNmczzCC17zLsmqLCszbhj0LtZoBJWOWLgXY7z5zKh1gZiaDAjsP50JPt5GhfqkFPX4U2bsF05I5fbDX++od2Uo8Sxoki12ESipo3vv4K41+57kra8gB+H9i/x/g3fXAtLV+NsplBbgwa4S9BKF6GCHGK4KRxOjKbcjGGqye/nRjlVNR78okjTP3AoVIpULGDBwdYnMKzOrX9IRrieL2KHDw5jqq9kqjuLGhMJ8jTrUpeZZyWLoNioj6lbIG5mL0E4/YfgVM9tGE7nXsxjgMtXt5s7PcLBtQIUKnkMQcBFx4bKdMRPbdVUvlO1oBmzwGnu+IKUM7//PJdXFpw7D9+09mUyUBs9CtIZ+MTPbRnFzid7GJZthzrRqkm1E28IcXiIrjjmCoVhPPa67XGR1waHUWc+Qn4bkWsQoPCX3ZVK737/eeacFJ35YhO33I4X+8JZ3ItDaJheLUQ5YF97Kh4BthvpK5tF7G+AGGkY79Tn7nl0b0Kz7VVzZ/VlJj9RdTOiaPLQo6chaugDatfOSwGWiNExi4puJKuVlBv/wR97es/M+GvfmczHTmMjtfQjPde85I/p1hmrgnfezuUUZde4dE/fQHnMZvrOMOaZ5v15714aa5S0y1MuMSeI48laM9udIjHNgxSugG7ya0lqVx+jJr0bjM5dyWQNbFUHTqMbOGJ6aBz9BLEVDpGrj2Q2uLRgvlYu/E8aEYdp5EHEES8z34Kxly/+/3b6FNfwJzuonVzaULXk+p0r6DEbKtXcqab7jke+HKJxvAQHmYn8O3NX/gpzeheaMJXv2QVDQ/j+fgYPkryvDQaRT6KRRa+cxBt5JIPQdlNc/kx2Kw5hEol4HmkDhr+3dfd/UEJz0qZEo2O7zPhlWs9esNbIF6mkqhjM4e2ewmt4VZtk2dHTenNgD1eR/t1cfwOnbOWKByVjs2TPjPzUQSRZfxhbZfGuJFcbbiHNo3QV3geL/j2135k/EZ6ES2dAW3vji2o93LyF3Tzt3FI9tVXyw1JSKuaZChehghxauDkcTqWDGxUEd1FUnZjdNedWMzq6S0xp4MyIZfD782NDiVToMz2DjURL2FqWziMmDMHtVGmQck6h/wiON3sGQnqnq6cQU2gR5jT5VQZ8bPbsA63ZetuuvxFEC2a2nymNFC0lJQjepWIUVII5Poq0YCasF4GIieROzqg8Zo9C3fGffkL99HMGYhz3eXz6KH1ECVHR5G3i69YSJkc0k8lGygZh0jd3weRdSJX4bJC9LEUVjYeexVw/EpZzr4hH541h1D2jGVnwdxFebrmpRBZ43o9FnnxGk6HOpYN3icM8+1zQ91PNiwXsmcBpe/ZrW1exKGMnkH8+d2PG//jf7Oenn4Em+eJoFQj2kCvfD3a8Jv/9TpzP5+g2o9DThcixKmBk8fpmNhOcjrj0aFDLv3vt2Auz/WbKN2MybHdf9jZlmIaD4psuZvcEGouImTIpR2TnA6/xxM+RVwoPVYsa6XGenAozwNlC6iOORQ42JOPYQ1mw4YtNHsBvp+/qJ0KOvvOjoMryDJBqYx3M2N55nbIUy6HeaKk2TUdXPrCC843/s9+spnzDOXPxVfNpv/6ygYTntA53Stfv4a654IDdU9vp/vuwvGbe+/abfxYPE0LF4LK2p0rwunsLpiJTIXG9N6AcgF+PjtR3XVx7kWt9Mc3wCZITA2vkpnToZz26rBfP6erwQuI09Ve+GlR1oVVLx7h6SnasEGNOm3aMUH/+qn1JjwyiDqZPtOhtedAArnhdeumjNOdvEEnZ7LsznX1Hnt0hL7+VSyIp5tmUrIZnXDWLHTg9uYEdzRUTlztgJtBZ89A1YiXMR2UEe5giSg0U6etaGexDRove84tGq2nMVV2fOyjPzD+D75/B/3/X3ivCZ9/4XLKZLCGE4tBzPRFrNNbYiLmnBjSsrvmZYPt0DAUIe0tWE989KEeFuugxFl17nS67VYYypnQm06vevFyaulCnBOZcdryNNbXnAADLBY0UaWCvOf1zjjXdasmyuXGH3tbqlXsjI1laWQIIuu5F7bQ+z54pQmnRfPGkMOi3KAmHKm2hfonghfQoLOoHSA2XPEikxeLqOie4N9Vb1Xd9CAm6HXvhUT0SyUPxcsQIU4RnDRO5/tlZnSq7VAK8a1v7aDvfxPcoq1zLsWboAQ448w5xq+P++QoS7dHSWTJwF5CH43WcLok6IMf5Km9FdxkyYIGSqnSRq6RMr4XI70+nB5ev8P4oyM5Oud8PZKT4HzqXXOTl/JLFaAaAqH2eu2WmHEQCBcdz4C71qewNvfoA4eoXMY626LTuumnP3nShA8dwM6WV7/mCtrwOERJEZ3nzocCJp1G2h3NqeouG3tGL8acvVQC95NjRxV7k6u2UJH/jsgRKkZTS4zOvwgGmlJ6Clt2o1jaXD2N/pvgBcjpjo/aMqiYYEyD2OeWvXGf0mqCd3SdhZwuRIhTBCeN08ntOr7uEojrIdEffP8I/cfnQWkaW7po1lIoGVacht0dKfKYk5kgU3lQFdlQbOd3tXM6+6ziZ6m7C9R+6aJGTkvZmioQPD9KnlIonQYayHxJIEeQYirIyz5NwPryu7xnP5yMADFOzgfuv/sAjY8j89PmddMDD2APYH4C6ay7YC1965sPmvDyZYto7mLMH1esgjSwWm3wA0qNDaWdzMuxkFLX/uqa/a6SN3zvBGluUOSpuhG5usH5BHBKcjoLbpfq5m5bJ9y+1U3Ov1zvz3tOZ7SQZtfA5BgeHi6aWz7FyY4OmeCKM3XCTjYTiP4DLoDzxfHf5jeeBvPf4uRdcW7FI1F+ipO90EIzxLklzzipKI3erLOJE/HQEbMF7OI84GycYvgHTv7muNiZwSvboYzjv80zKROczWd9OspxecaJGDh92izj5i+YZ1yp7FFza2BcIl0iNygYl0qJiQmOW+KUtUF2vhAscxI6z3+zGCtOwoYAuPyvaFyB/2UqvnHFoMy5kVtjRRyVAaaDLAQTHG5TJjrGyd/sfIqxS6iTa7PFcR1GcsZhM7jtOc8tTtqgCxEixInhpA26aCRODnM5cRYV5iZjuZJxoxmX5GpqcZb6GMf/ibNMxec/REUPJ2E4y4l8e/CtCsTkxKPGWVolTriaOOFeInaJE0WJ2DiEnUN9U6LQeMxvwi1rOKY5LoOfjTgsrrmjjpLpmHFeRbgmU1V2DrNfceOFMaprqRiXTFc473JnNnOtknBlKY/EyRGxk6uwxAUsHlpHfoqZIOeXnZjBExfjd8Uwkzj5Jwaeap2BUGwrWhpIOr/Cmd9FZLD18XyD7RlSJm0Ek8/jOfzus5Qgorc4L5I3TrifLadYgDbO7EyycU4dTtqgi3hy+QV3LN1OZVAfob5sn3H9o2UaZBFTnO240uZVSU6duYiYR5d1FRYNxQU82MQ5IqKy6CZOUvJ5kIsjuWNNHHe4mDrRiIoz5yll3cqsXfEfcgbMnAMTOV5eYE+dSL8+fyvOzIdMY/FvCo/TEBdrTVBFbF+yK5dLLDLCkVy6z84XzWsyYZzr1lFQThqXSkaNE8WoaBhrXW3ezN11cj8duzgXQlySn8W51Mbx+xyTcdVBZ+IR0Uo6qRbIzNN+hTO/mwpi9zyEmJY3TibTUkfsxJMiitNHBtpe3DrseG7PTm7TlX+O/MU/i5Odccbxdw5/LG4ykucez9OaDhHi1MVJHHRCMcSWo1BOwPdLlM0MGjc+XKG+Xte4PM9bxYnSxGXuJU6uYIIj5gz8TJwXYBMwO1E+QgEZo1LRMw5/g0pP0iob+u0oF77Ev2o8wjD0L1duFpVdNCxOR6PMydjJhRnRKJecncsFEMdFo3S6wTiHOZctk2xfsluYjg+b0vFgf3u2309BGBMS4lLslDPXFr+2SlRkjpt/DXBBvXFGqyu2U8WJ2T5xIhWIWC833RqObyN6bhFyuhAhphgnb9A5HjOEmHEWq1ZMo86OpHGykXfgcMW47AQZJ8oEy8FcT7gELrMQbmcci/L2d7G7Kk4mv2JBWJwnEzDDWSe5q3CloxUD4n5T2O+EI4Er2SdiEFbcREZ2jDjqZP4onI9nW1wAcaL0aWpsNk6uYSqXXePskkOIEwUkGbTncWAbRubEOqdzApdi3IHEyR3p4oxaTCb3mOAbB+OyooASJ5FMDZ6lJL8FZGZa7ezA4gXNtGZNi3EV6qfDh4aM27Chx7hSWURHnuKyK3PYuIqsyfEzdnLA2/7uyuUV7GSPsNzUKq5sbpaxg87WvsBWbO2z3wZW3OCM2LCWUWxr5iZc43we/HbQ2UHpuQHV1TUYFxHxkgsjzq5BhjhRaL2bQSViPTsm8GKdwFgosCJjpEhyI5M435tgP2OcOegpjrtBvhAzrlBKGuf63C4crTiRTKcKkyMkRIgQU4KTd7RHTur62ABcK+39+xdxtOfd73yKKjyhFSxfC7Ly9j9ZRwk9MW5tXYhdlLq0bpxmihbXc3QJPYMXlMUsH87TrVqdpsWL7XYqUSkLQXNYwoCIWy2YUcefGERIthdqRO3yh3A3B3Fm1Lr0fQ/0UV8v8tlSnyYngefjei1Vya1QUxO2u2WHPCrlcTTommtwNGjxErGzeKL5UtYo4tOJwigGBFNIwp8LVLduSfva8ouEhDaqQJtGiXhysswsVkYTOLspt8cKvnTzRrrlezCZ2JjC8arWrgP0jr8804TPWrvgl9rjudoGdhIHHXc2zw4WrRweNAcO4dmrr/9feuQhnClr7MQ5tLe9+TI67TQc5BzTm1+SKTnEigEm4erFjRplXYzFTE/NNcz36axzcDZPDM/CP4mDzpqKl0Fn1vVk0KEhbrt9P40MNJrwtLY0ZdWEREVP8FZYhuxoVxPqh0pUzuP0wUtehkG3cGEyHHQnArUQBjER5ZcSVUQmNEB9ezy4UinsbxXs3IP2+PgnbzH+LT/g78uwWTo2hsteli7eS7f89DUmvGxR55QNOvTuECFCTBlOIqdjKuTpGFZOQVGe0BK4wZf+Zxt97t+wE7+taZHxz1wxjZYsgZEYS1VEuSSnCwRipMieQojoFb5C15w4dtenGobokstwpqzJ3sQqmirL6Syh+q05HbgnOB04diaPfHzzW1soN4YzcvNmtdDgGMRoV217ym7sxkaIvhODPrklnA5/yUvxzcJFsoPnRPM1yelskX7tl78Vp3u2d0+8/o4HrlHjY+2TIX8eL8rjFa5qvVr8SU4fqFQViYLTCQYOwOTG1757H/3nt+4x4Wf2oh7OOvs9/DtMQPbsxG//+m8r6G1/Zo0VcfyBSmqagZDThQhxiuA54nSIMnAmsIeRMZ5voa9/A/Y9Du2HsZjmVCNF1YBMWm38i4k9TzmMmFSLy40eDJ3mUVpuYjVUj2XziV10xdXglHNng8OYU9geZHvHHkc3FLKWatkiW59/1589pnbY3yIUCengd8SVySM/X/3qFsqP6/xsXgsdHoThoRG19NwxrYPzDMWSn2cK6mESf+1LlNMtlo3NJ1b1lkOYoyp6chwbspEXe3+BRFc1vaeczlrbFgjhnkxR4+Sy2ZoBh7dvVJ+qs6gNCzidaqQ2zlpwCrpGYm+ExZRf4+FHthrkuJWBvFaVfGycFX6M8po0FQf0Bt9HH9lP3/k2ONhP736YigH6wIUX3mD8puQ6uudOXL911lp8853vraXpbZhri0Ers+tF/xI87xUp5n46vfxd82w6SUUtLyfjTbRnOwr7w+9iIhsEc6mpBQMwW4BGsq4pSoEdYdxhojFUhKOXSzTwoEzIBROMbHaYzrkAg23ZcjwLgskbUh0dKGafc+1CTDWIzsDTcnZ46HHlW/PiVUNajmwZwh+jGeT337+wnXLjuDF21eo22rMXt8gcGYSyaMWqJVQYRzniLArFnF0mfO0fQpyZv6SF83piVW+HhWwA93wQJNlyFlOljdA743N0kSgIhbXdKTez2r5j+rxNsloHcrICERiCVc2TtgH/ihMZCE3mGBFIGSanBvpM4rSZ4nqzRp9i9oIOl9vFpm+aCISimjWJRweohbT+sN6c++Tmg/TIk+hDv7gXJjE2bxuknCq5krEYLZ4HkyDrzrjO+Ps2OXRw/yYT/pfPworaddfUM3lH/4ybrWB2UCMnoXgZIsQpgpPI6ZiaqQKjCp4EVzwY9HEiSab2UCw8/Sg43t2/GKaGZihaZNOzIFfIU0MTlgFc5nSWa9WroiTmVJjTgbtNZIZp2gx8d/U1ENsSSaGsUPXHlDuBYv0qqiXv4Xc5FmLFtaitmghP0JXyjowj7Y//4+NUzkG0Pefcbtq+E6Kzq1Sye0YXDR3GxL7eSVM6DfHzxS9HPhcsbTxhTme5tCxNefqNMa6kpirk6IpAtpxzqU1YdVHklysUUxFdqkEUTTYMVAPy4ySj08dShVUjR+LZ1/VFsTdq7ZjaOpL6tqJkTUL8jT4TVDlZbaTw5T6/DDT+dM9DsJv64JN76amnwNV27Bqmcf09UYe1UE+4o9ZHd3szvexKLAUM7gN3fezRe+hd78c95G99K5apfDn/qHmS/5VRV/H8Fy8lmloRTuCUuBpxM07AnSHQxXHyMJDWrx+l7XrHW1srtJADAwUqldFJUumGqiUsM6gZiTjHpAvVURZfyyXEf8llGKhLV0T4GxXBdF3LikdATSNXNXzW56wZDSESnRx0Mk+D+Do+gUb86/c/QIELEebSy+bQExthVt3TNcpZs6fR3u1YEG+ta6aODpTzJX8EA7ULl8qhVY3/18D2edmM6ukNN3JhpmhaBXKmDkjwM5TV6uGevdsgn/BteLIeUE/Wt79L2D63MUt91Nbvs0NnmzyVkLkxRMXeQ4N0WG/Y6dkPUe/JDcO0dzc01BufgVa4INprvZEzlUpRQwPEfDHBIfCCAovcSOHKdZdRXaXbhJ9+AhdJvuzVCXrvTRh0jmpE/UqM+wjKgT0atmxAKF6GCHGK4CRyOvkPYUsE5WZNz2w65Z+YXPt651g8Bo7n8oT6ySeheOjZB5GzUmqgvF4HnM0y5VLRJZbELha5DjnQ3QjJqMOcDpQunoAi5oJL6mnhcvwe1cl8PFLHogSomwiA1at57bpMjdjjsfha5XTVAgnnBIcbHgHH+/O33U4RT6/KeukSelDFoPEs8nHm2ato0xPgft0tHdTVhecvfyXE6cXLT1x7GVHFDlXyPNdHPBRLUcWFDc5bbwE1//ld27iewAFmzQTn7+qo4zrD98mUQy2taIOY2v4UkcxKh06skd9FnThy1zJD7ImK8SaBGHiyorurdwSOjBRYFIQYPTYKPzNapKEBcC1PlCY67RibwLf9Yx5N5PFuf/8gjY7hXddsYGfI+ymIjczWjBevT1JCt9p57oRpe4HjgzunnEaa1Y0LVToay1SaQL+67mUrjP+WdyymVBxSgqtXTCfM2qudikh8k/1AEHK6ECFOETynnE72zflqmxHzAlAYa0E56tTxdxj3e3aBCm3dOkydnZjfDQ4Vac8+UPGyC+5Y39RNulRFMnXT/dJUKoGjtncP0aVXQVkxuxNcRc7lVfX/EbFEDYoZsTedVOd2TAV5nmhV6EdzOuR9eASU909v/CF5ZeysedmrVtOdd8Oa9Mg4rFhfdc06uud2PFs4eyZ1tIGaX//HmNMtWwGOcyyO2xwVVGhEVO/K0clppZ/9COFP/AN2+vT3N5KTAKeKxhG/HJXyPcxxxRypvSwyYvdxWp9RdH1m+miPuHK8Qr7AUgLyJOtoclZQ4OpG42w2a84JCgI5BGkg+dW6NbNLW7/aMdKSBuKPMMdO1iGvXhTSThCdINdHfZGrGhNRZul8NhLkKBlDmabrPt7lSxbS3NkzTPiic6bRFZdiTtfehXbzqMipaz3apYFAnqA80SnkdM/xoJNGQOVg0B39AixYofI9F8+2bOtncQMV0dExjSYm0BCPb8SEejhTR6kERI+k01TV0lmlSdnbTzPmofGvunSx8VuaJivP9Uvc+SBSyDk3gPOgA8+Y5dQqqSpSZJ1OG2R4FB3krW/5LncyxH/Ny9bSj29Hxy9WMPgvvew8uv1W3AS7bP4CmtEN4vOa1+Geu+XLheDY+vjVCFSb5HPHiYrhI0axmKavfRVpPno/6iuVnE8TOaQzlkcdZLI+lUQdyBBRzxrLLakoV+D35PpnQdFN8feI395zJ+ca/eomAXl2zAAyfyMsxjqMz56YxAc4btUEiUl8gUdj3A76u/gpDTu4kYjqRyndgjQ79NKZJfO7aOZ0DKSO1jQtmA/xec4ctMe8eU0swkO0bq8XoyEoaEUJTiSoN+cdBcbOpQFnVCyvMUwWqxorIBQvQ4Q4RXBylwyq1G8SVTHGcDlQVNl6JBDmLsaJBPYWTbEA3XsEW3MGh/LUaO8plx0DjEc3HqGDe0DNEzSNUlGIa3rszpjBK1egnp8DaYOuuGwGNbXhhbJbongC1M/mwyhUdLeE78g6nQkyp4MPToc/RsZAGf/0xm+Q7y4x4cuuPZt+cd82Ex7PglpfdMn5dNdtT5jw0nmLacYMcMA/fh22jq1k8fKXDBRxFVgRSGAJrdgANT5LDta+pbyX13v0xkfxYi7jUcFetVVAHY1PjFMuC25RKUc5z+BUluMVmePpjV2UKTYYkV5QyOEFucvPrSDNUtGhQhbxl6xIyW1qc2zbUPpCxSYgv+oLjiphUiziJpkbCeoaA2qdjrpt7sQ3XXOi1D0X7T2nGxxt8ezp1FgPJZZsWALPOhpisVngGB91FuiWQPKTdicfv6BirPlfxWCz3GUbHAg5XYgQpwhOMqezmKQQRxMLvCOXFxqf/9ljNEIxjc+vRHRv5eBIhvr6oCxI1UM9Hk+10I6tePbw+sPkBJgj1StH9Jh7pFKYq+VHochYviRFl12NhewW5njlAJw0qiwtsCbeGI6oq7UsVulAOsEX9I3hvb/4868zh4CKet3FF9ETT2B+NTAARcqFzOnu/wVuZ106bwW1dSLN17wWSp61K+vJGiiySxjC+SJ6dZg8s9sX9T4W6jlUop/fjssnmxpaaM48cN2ubnzT2BSluhSeJfQ+84TGdyJwOb0iGJ2tAp7vMVfU4gvzKkGXQRU7zYtUuJ70ZfU8Ubio8ifBIoi9ZTepTKUx5VBcWZUc8I4jy78SUhcaPafncRhcrSoZmDko0jGKMGO0SqCVZ/ZzIlgFt7/td1g2OvqFF4Ai5aREY+KxG2elQrOqGBgfhl/I+NTSio3Ge/aVaf2D2GicK+mEON5Mjh7Vj+u9b9nRfTQXOg+69PKZNG8OBrWY3xY4gYiU6AVRsxkX6QeqtKjIdtsI4hzO49t3vvO/qDCBzcvXXX4Nrb9/owkfOQLx8kXXXkJ3/OxOE5436wyaplchv/o10LadsaJZIjaQLccC2WNir+0NOE/2dqFdu/HsU5/aSE8+ojfCNi/kjEMhUNcI0TXC+a1LQSxr60I+W1vjNL0LhKm9rZGaGlHOOt3E0tgYpaZmpFPPz3RZjOqh+KUkiv07ojpc1LfCHyYcdgk1P4GnpUKFBy0eTp+B8ghd8owaWr4t82BWJZiVGWXQ2R1Rxvvd+2MoXoYIcYrgecfpBMelMCprDfcN01gGlL/iN9HoOGSWXfsgF+3bN0GFIqhfIolJuIg4mcw+E25pydKrXnWaCc+bbTyKs3gUELiFV5YLRvC9Jzb0jS9XYmGZYkSP69x4w+fJLWA9cd6s02jffmxoLpXBfbpnNtLICETKpvr5NFHCZt1//Pilxj9zTTMFZZTD16ULESMtp2OhjHbvxxrlxz7xqPGf3txNSQdpktdYNcrjuhC1SsyFy3nkM5qEHFiqjPNzvJdOpCim6n9flzYSMZdFc9S301SkZD3quUG5X12Dx1wPz+QW25jefBvXI/3poJFSSXDVaBx9QPZDxlSz5Xol5lpom6IqUjIVlh2yeDc/EaFSBuHxEazNFfMlKuZQj+edj/b94IeupFmzscHc8zgfceRvcjeRVN4Lg9M9rwedxGk1fPbgqsOdslTG730DORoYwvNsDmLb6GiMDh5EhxrU+VexFKW4duzRgT6aBumUGxTzwHnzA5o7F41blxSDuYBdnyq7eUrpYvHgIDr4D779GHcYDOre/gKtOQOazJFRzOkGhvbRwgV41sdzsX29WLP7x4+9wvjT2iU+W06kXZT1Mt3eJITiYD+2Mv387j3Gz+Tn0HA/0hepO5NBxxzog+g9OpCi3CjmvoUc4pHbfsSIr8CJpKtaR093GMhG8rKuZY0VeHDoeqmd6iCP9g8eNGJjUmAnnNzpxZC5IFI99VDkNlQNYcCD37dzYrvQzfl1IPJSbDqL9yrul1FeIlmTxQC84EJ8+583v4aWLEN9Fku5qqbUWAFjmGurQ/EyRIgQx8PzktPZuI6mNJbayoZlhOQQztAIlCGDw6DA4+NRGuiHODM0ChFk46Z+ZlegrEGllYYGoOzonAYtwVh2J61cjd/XrumkObOhRZgzC1Q0nfZIjhEBoOYBi7B5McPAKDHpamgB/cqpKBeNirVhxJ8Z8yit5wHbGsHVZEfX3p149847IHrufGaQxTmIh+3TU9QxDcqh2bpmFWVxty6J35tb6qqbliuidmRMjHq0fy80u888jTLu21VhiQC/D48naWQCHL8SQRmLbpQmCuAmosjxXOQv4oOLk1fHIjeCYnAq0KNaJNaVGXJm0S3rli09UkWBvIN2kevLYg2Is0GVM9M66snTze9jw5y2csL2DnyzbFWRzrkQu09e83JowBZwW7gqTstOJ2ua0VGDUS8k8TLkdCFCTDGel5zuV4Opds3a3uTVXMp1vAiNKPc70gdFxoGeLB3pBdfq2ZekwMNaWUEPy8q+54qqo6P+EDWDsTDBxBzksstm09x5SKe9HXSqpRnUW2BOr2hYdioKVDVi/pcnwiMEDz6EtcNH75mgzU9hLjZ0BAnW1y2pHuCt8JzGJcwP4wn4sUiO0wcHmD69mZpbEf+0mcjLjJmNNHMWOGFrO7iCz3OuoiqWjhzxadsOzHcPHAR32rF7P+3fr/PQQ7PpcI9Sd51nMU9lZ2mzKGd0oY5USTSN8202MMsyA+bQTS1Jzic42YxZjdTchvn24BGkk/AKlEigPlq4DCtXggWecy4m20sXTs6rq7yGXxd1l0DWdid3E9m8vXA43Qtw0AlQ4XJGz54IqE7subIj1f0+qLTxTIn6+jFQNzw+SDt2QAzqH8BAzeVS5PoQ5erjzRTThsxl0bFaWjxq0VMC7W34tqU5xSKkCVLByVJFlTuBMaHDYmRbKy2YD/Xo0mUJGslgUN92CxQqjt9MBbUstmMb8jY63ExuEVahnRgTB1VMyEUqgrFhn7KquZXtc7IuJwgcEAeP+s1Vy4L6VuS3e1YdLVmE0xAL53dRVxsGU1Mj2ivqFFlMRpkG+tO0ayfWAffvY5GcMcFRT+QQ19h4H2Xz2GLnB/psYphe89pXm/CqVVB0pBtiVdP4qTq5zw95Gh3AgO9sijMRs6JzHRM5LaCK7p4vF8UgfwkxfsqAsSLtY2YjvYZfgIPO5jhEiBBThBcQp1MKz6LFpL3HKNmtVHKJvsCJyiQbYdw/Jt9EecKN9woVl3oPQxmwYwfE0G2bs7RnN8SmwwcbKZeBaOTrVqam5gZqSCP9iDnXJVuaEpTU9SnfyZv79wS5AjhAlNlgKgWxafrsIjW34/f5c7HWtPasJOlJFXrkYXCae+/upe1bkPeBIwlKxKD+TzRA/Cy4LnMe5Qp+gssMmmmNOgUs+pVdiKylEsqez7rkuyo6MydJpxB/XR3K29Hh0OxZ4DoLFxRpwQJw2hkzIRK2d6apsVXroU12zKDuRoYhKu7ZPkBLFkLZ0aRGpmTnSCKJ9IU71afBrZJ1aLdE0q9uWBaR0df1O2vLRtYo7dKFZV5mFnGKcLrf06Cz7/4G32iFy//2rJep5GOiEK2Wr7YhfTs4Y2I+Fp3M4dmSU21ydLY8S2mHeyGq7dnr0e496Jj33wOt4tNP9pFXhjavVMQAiDldlNRB1dgSUFsLOmkaHqX5NbuPMcJzMjeC84BtLZhztbSwmDUT+Tx7HcTQeCpNjz+EznzLd3fT4w9DHLOScx3HvXA+zA841MAD0IrWGPxlFslKJcRZVhMJ+aJbNeQk5wdLZV2oLmLw+GaQYiCLWaOYisfxBNLu6i5SSydEysa2Q/TWt19lwtf/4bnGP6pbWglfHh63v6KOXcpU2yZKSW5PbQ81vWDa1PYnjccss1af1TR61VZlTV8w39S881siFC9DhDhF8HvidEoSq2tfJwKlgpYaWlhidLzk7SkBk14N1VIxZJKS1VI0sSIJWnTgMDje5k0Z2rkdce14Bs/27K5Qby+4weG+g1QQdsnwdc2staWbxS1wNbnyK61brSxX8r0E5So4mbD8DHx74WUzaPXqeSacoFZ68D5sA9u1DQaOtm/aTEsWLTPhJUtW0UQGXM2tIL/loke5HDjV4WE8Gx5nTqBiKOeEw+AMgdZjiV8v5NEOw4NZyhaxgRy7QmRjdD91TgP3TaZ76dzzcFrjXz7xLuNP75CLUEzwqFqcNANhfQHESObH/L/Nk/hW8lCuZXaXICinGOBLuew38rt913K6GpiM1KZ7Ijgq9wYhpwsR4hTBC4jTKX2w87kTQQScAKroGippqVrtfEDhBnmmcAhHdLeDx99Yy82BctzxCZ/6+qG0OLA/Qz37sBPkqcew8fnIIZ+G+lC+kSGXJibAId1AOWJqguPHs2wGnCzwB6khjWWKN77hSvrEv7zNhNXWEO07MEb9/UhHjv5UyihTug7Kj/q6Op6L4ZmaRaH9B10aG0E6wwNlGh3BD1YhMzxUpJzaoYlw2TtU4bNoCZRJCxbFaP5CzF3nzW6kZBKVYy9gjNoj+wa17Wr7gzyzXMf68putc/n+WK7Fz+zn9lJI49u0xNd+UP2mtl9w+pbTVjUxx4HMDY/S1ByNU0yRoviNBl21FWqCNWnWaq6qv0PUglhjG0x6sG1kW9HsV6PyuCxoMLsYK+uB1rS4ow0BIRThKMdZHYzaR2SdqZjTQcUDdHgInX1wHNuzDk8coeE+KHf6e3TNq3+E9u983ISXLGmmf/vi35lwKo145Apmm00xyjUxgfJNbjiOs6iJN3wVI7PZcnXTuOcFlFfx0x5yTaWiFFULYMlovLr4n9QNAlgMRz5NHdqbbfRaYVz2ornSji4WtmRLGTA5ACcvIpH6spBneD45APRvQbWPiF/zvKq1tN9Yn2GImf3u10G/M4O3Jg5GKF6GCHGK4PfC6ar3lZnjGaAmYj3YonohRQ3lsRaahYpGqr//atjdIdi1gm/EzqYVG48rmnA2LIGzRapUJE08tOJbpeySY8+MUUntJnIqNdRxcotabd3YvFuLixLCN+Z+NlXEiDGiqO6s8Xyo950414GmEzUmLSbjAuR9jUvX7iRpXy/FN6Jgte6Ue5r37feST5tXKDB82Yysj5xoHfnK4UitTvsiAVhRrip5SCXimRimsnfqWeVNhOqqXARtbdP/VZCdRsg7+lptnuHbNVtzuYqKslZqkfSqx8Rq2sh+7Zi7Ko7OR8jpQoQ4RfCcc7rjURdrdi8mmxf1saVSQuFl7iEQGyH2sKKlXOVynucjmMS7npikw++yA0Tg8TN7A6ulcsJ9LJWU09ZRXSx21EcmkKZcumg301ZzLPscNWgvWzQPLNcwceP7Sbv/POfTfEgVyOWMtYg6Af8ODuD74MgBc4Wo2mKRuaedJ9q7FXyP36ty1FiVg9mcOlGe89n6dvRojajc7V0IMpdVLm+tZIuRX9s2QYzzZOtbD/3K4VBtLpOMDdt2FWO/1asildPBLB/CjrFjiHetZBH4CVMnAqlCp2o8SeOp+gK8KPs3q7fQch7j1TZWLsrveVof8ZjUofaHqgRl0+C49IhQPB6f7Jdmjjj5juC54nTP6aCrfSZhWwgrPooVblsu60tj2Mv9BZ5eFmLrw6lejyrfOPw+KnXSfiYPi+pAtf4k3DK/p49jcQQwUBCPF5HrvQArHpohZ6Uxa2WKRSVHRc2AB4+9WMOKZUFQYaKCgshOf1/Xyuzui1gsyd0EkQbaWVAvSNN0AltUVSbgZPZk+SfDtmPJfX4aV3XTt/g2LEA+bGf2WAy1d8lFhAhpm1kLzD43TKCNU+G4o2qazNaRqlV+Cdb8usmjipq2PievpQbsALQEC3lD2BJLx5pEM3D5VyimqgSJ69/e+JqIddpi8ACHL9FUxU+tAjtlEByv/z5Xg862QIgQIaYIJ5HTmf9NuBaWlQsniumVTSU1npgyeumjqZ6gkIfiIJ5gEUon3zG9h1yucKqUQeXKlSKl66Dbtsd5agni8ACOvIyPFqi9A2tZze2iDDBB/sj6lSp3ZP5EPUew19DKUgtmdlWpUwVZM/eRVHSPJyVZRLNXT4lNEEYkynFWxS2H68fGAN6QLUa4/HpsRZsgzpQ1ZlMSkdBWp8aTLcs+SvwulqpjardFuKoBc5SUbjR2PcRdLHtM4VHgKItxlsqLfVBBqi7B3B/fR/I+1yf2o7pWCRV3mZPjI4/Fw5wYwpSwJplmcbWqP9EMy/91astPuOTkKW/U8ZHDQ1TQjamzZ0+neBISgSisABZPlTvG9M75of6SuaxE0NJcRy0dyKcyRE7To2IReZNdOe1t2CxuD5vX9nLLaIulYlUcr2+U+GpeYjz/xUsqcGSYa9n1FonY3ooq87dHnoSBnX/4ux8Y/4YbX0qv+kOcwQpU7JrI5uimv/6UCS+Yu4LDrzRhVw3qxHggPvjIFhP+l3/+Bn3sE39hwisW42DqRClOX/nKQyb84+89Zvwjvf3UPQeD861vfyld/wpYA7MVLp0ho2fKPn3zffS9b9xrwjyqjPemG19PN75plQm3NqC3FUoufeSDN5vwosWz6c/+/OUm7HsYdFGeEwVqvrtMGUoQdkI/tgXrdP/4sa/RW9/2UhO+7uK1xvepRPHqvIeFWhWlN2/GOt4HPvQ9fkd7UVzEdTsA0AHjPKD/+sN/ZMJPbd1s/B99/36WiJUg8ZBOqC1NXy/r+PDfvpHaW2Ga/mM3/Tv91fv/2ITXno3taG5pnKK6ED6aidHH//mHJvzYozB625Bo58hQT2W1Spuuj9OH/8+rTPiMtd1MVpHm17/zoPE/88n/4UGHd9ddcjn99V+hjRfPRTkKTNnq4mivb9+CcvzbZ35Kg4PYdjd37lx629uuNeGXvQzW0aTHPfrAPhN+319/nt793tea8Ctfcbbx3UqBRWMQopzce8j4q5s+z22EdD732beRLl1yX0S9h+JliBCnCGpUFr8bImYTq/JtpdYRFqnsJNxnvvfkRlCQ236MZ51ziszpVD5QZUGRxarHngbJ+eH39tIr/hBauIVL7ZQ9YIqL3++7jzlbHpTKpv2Ff3uS/vbvwakqekOobB/evmu/Ce05fAedcz443fwZyE+x6ND/+bsnTfhTn/wP/l9FF4Im8YMf+jmNZ8Cp/u7DuCS+FMnQA49gI7DvtVWFZEdPe5OXZE6lXIVFNZfATdbfj21ct33rEM2audOEr7oQnE6O4BhTcgxRiETNXedycym4wi/u28d5VSpc18ZUGlvGSE08UMGnG/4c8u+u/Xjv7ttZJNPtJU7UJz+P9CPpw8Z//Z+MUjSOjdk/Xz9Ir3srOPla5Y7RWD23I9rILSdp01Oom/vvVjlbuKfaoKyIdpURjXv0zg/jGxGXf3zvARP+4EduN/6h3ZyGxv/Mlq00NopjUzd/CdyrsSFOB3UHz4c+eo/x922SrXIo0zPbe7kvfceE4ylw1OuunsPcE/3qwYfKtHAVpggvBeNnMXWEORfK2bMfZfzhrf00cz4kGNWxTAlCThcixBTjpHE6cAWl8naNh4ldLKoTev7p0XvtRBmcZuumUdrbB+q0YLqO/3iaWlqvMMGnBo/QN76NzcB/+9EFxhe+Efcxf2tLLaekB260fRO+//InNlBcT3z/2XsRz/wFLfTQ4+AKt33vEbrtNhxOfeefwnbIXfdn6HOfwXyga95aesufgPplhsFpvvKlffTZj4N7Xncp5h9nnN1GqSiuvUrGkB8AZQycPNNlkM8odVKxDA6x/g7Lfc+kxx9AeHgYlHdm5+SeRPFVBqA5s/HtTTddS4U80vr+dw7T4pVQDl1yFfyJ8YO0bAk48m23IM55i86gG//kfBMWC83ZDOZy0Tjq4/Tli2joCNqtsW4mz/kaTdhCuHX1sg6u4koF8Z53+YXGv3zdXG5ncL2UngyXKd682TDke2SE6BMfhBRxaDfmhu/74DuofRZK9/Wb99Mt39xlwte/GNLI9a9dSDd/Zr0JH9qK91534/W06kyk/fQDLn37W2q89yOYv198wRxqaFadAi2gbVtRjoN6Umn+jFYuB/rF5i2IZ3hkBi1dg1PvEMimht2dvEEnVortDZc1uhk7F92zq0iPPQJLWBect8b4+/bdQQ/fh0s4FrwaHV10F7kCKoVYJPv6t9Agr3gVGnHliibyVKkyMZohR3fa3/kjDJrcQIk+96U3mvCNfwLTCJKFG7SvX7l1Cz32EMyU+zrovvWtuyhVj8H7xS/+Jb38aohjthRrl/XR+97+ExO+61acgTt91bnk6lb+sp7WBlDgCFX4e+QtTg207Rm8s38PxLvTTj+fhgZYPmYM9UPLOrOTy2hVgeyXdU1wnioYPvSBiyijEuX9tw3QGSshst70FwuNX3IXsYiK78cHcB7ujDWd9P7346K+EtdtPAYFiVUiNUQd+tkzOLfnV4pMJLXBFLLsF9HLOkSZO5ZFL776xTg5/v99eC2L+CZIqkw1ora11vDjHx2k7Y+hzH/11jcb/28/OosadHxccvoZ9MaXfdOEn3gQbXjR+Qvpe/+JAXjxmRgUX/r8+VSvUv/E64mmN4Dg/dvNDxv/x98dp6WrIbJGnBbauQ0i+e49IJwLZ6R56mKC9NADyI9b6eC2w0OtjimBspcQIUJMFaJ/x9Dw7wZZJjCnfNm3am9ZP4oKha/Qxi2HqOdwL3V2+/SWt6+icy+so6B4iObPbKKg4tLK1dOY2vhUZFY3Op6lRYtT1NzSSPnsCMUTAS2aV0/9Rwq0alUzHTzcx6JSiY70Zejqq1fwZD9CRw7sp3i8SAsWttK7PrSa1qxtYorLolRQIL9YYSqZJLEKF3fKdNZps2jFkk6aN6+LvEpAjz28k6596Sq6+NIuesMbplPMrHv5zKVlO5NHyznNAU5rzRltNK0zoETSo9Wnz6XeI+M0b34nrVozi9aeDo4t29BkTc6YyBOuxVxfNlhv3DpE2XyeRjMZmjEnTje+8wwaHt1G3bPjdNaZc6nsujRnJotEsgxgjqYw1WZ5TtYf5TSf2NOMx1wq5uSCk4Ce2bqf1pzdRB0sbZ6+tpl/D7hsnGfZBsfsqffgEM2eVU/zFkyjZaumkSzFue4oi5ejLKJOcJ1nqVLKUiqRoiwzWjEeNDpRoAsvn0eptEPdnaqg8iMUMVu5HCqUHOrty3GcnTR/aQc1tyZpyfwmqhTHyHMLHA+3X65McglsQwOzDp5f7NnJbRU00IqVHfS+D62kcy5oos5p/B6/7/oVmj87ye+W6KJLO+j0czpowRJut0gHbds0Ql1dTfS6N8ynM85opzPOa6fALXJ+XErWRei0NV207uJOOrK/QiuXdVHXTM7LafWUYBG3j5lxa2MDzZheR2eeHSO3XOT+VEe5nMfczaf7bs+bTTJNDZ106RXttHB+M110YTPXr3D5SU7/vF+nM7KHmtq25gFcN8uDAyz/6R15OtAPln/+BTDE89RdPdTRCLFx9Zlo5JLj8nsQxUqFOhYbIZKeuQoi1CVXxelIBhdNDBxsp7ndEDXHDuMbhxt4zhosokZ9rOvk+5rJcyDjixTRrMaDCnnVDg4lyVXrWHPm+TxPVDlGTQWIMdnNm6CZk43+gs7paepVc+ViZGj+XF0M1i1dgSxIx1Fe14nRzj1owGd2w67kJVfPpPV3omyL50BUOm1ZIzc5ZLWA58hiNl4QUbPjDg/IiQzqae/uKLVNR5mnq4GjqJs2RESw/xC+yZUauZz4XdagK3qxiOdDTl17ZivlMni2fV+ZFi5HAWe2ojxekQd9CuUcmXBo10HtiCo+xkYLVKenLXITaGvRXi5bi3oY7g9oZADP5yxFezQ3cIWp+fYgUqZoAmmKllfQ29tMfQeQ5oJ5aIOOdm0TAYumxQraa8tmlK21q47ndCD2h3c7tHcXRPbFK/H7qpVNlM0i7w/cicyn6yM0W+8tnDtX9A+oOzsiwnW6ECFOEZw8TicaLr3yiWUX4/lUYG6FtZHbbh+iAZ1xn3UOdhFsXH+IghIo6tUvhlauaXqRHnmox4Rnz+kmX/cbPXIv1l1WnzuT2lg8E2x8bIBOXwkFSM8m/D5j2lJafi4oZ1S1anfcepC2bgPVSiZaae1poJoLF4LmbNo8QrOXIf3Fy2IUKyNOa9jIZcr99GZMvvt74K9au4A2bMLEf3p7hC44B0qZQBd8TGq6NWxozKdf3IW8HBkCBzj38ja68wfY1bF4Di4vuf7VXUxtQcEDihmuLIjoLN9hSSKXBde47xdZmqkXi6xag2fCkiMVhO99BAqPO+/ZRX2HwF0bk42UkQs7GDO1Dt/9/itYNET89z/SR+dciDjnT4NkEFRiFIkhzyPZGD3wKPJ39/1QYGT2HqK0mnAYG0P7rlozm951EzTHW56cYJEW9Xz2JUg7nWhgrow69iKjLA1pv1G93qNPO3RoF+rrssvUZGFzlLkivo9EWWTUbXX33g3tdqrJpe5urPft2V6iVD245pHDUCi94lXn0ZMbICHt36t9pXshFQpI58or2wknIrjMIacLEeLUwknkdBNMtkAdoQhgOAE9eAeo6Ic/vJmyRVC3aB2orMdzlYrYgGO8+k1QRNzw1m76wDt+YcLr1q2mP3s7zCB/+jPYg3f33cN05UvPMeG77rif/vYjCH/hH75r/OXzL6IPfRIGWSvKKz79+Tvp1luhgs70NdJL/gD7Pd/9ntON/6YbPk6vev11JnzjW84wTNvAmvCLOfSZzz1tgg/dBxsmn/7CW+imj8juFaI1S1roAzdh/a56Yls27UZRznt/MUofeA+WGjwfHNFLjFOsAire3gLO/oWb19CC6s4bl2NAXHInukAUOxkwWnrHmx6mdZcsNeG3vwd1xzMd8vL4/qYPYyfHhk0H6cY/hXq/oSFChRy4UWcn3rtk3VLa/DQ44Yc+8k366D+gHi46R9dFpTzKAeTqsXe9G/FGkuB+r3zJDCqX8b29cHLhwm46/xzk7ZP/uInu/zl+/+/v4xba9nbXKF8EUa6jBx4BB/N0Xj3U30Kf+ijWRb/69auNv3xZgsoB+kqMOVBZ12dvfAXW6RYubqVrX4F1iO98+2l601svN+GP/s2txn/3+66nu+/EDqDpczDfa04tort/Amnly1+5gJJ1U8Pp0JonAWXuIHYLlDU9HqUm2vAo2P8TT+ylM8/FFipKoFfL8f1HN20y4bvuxJraH752Ou3YDYtaixctrOqSzuPOIfjnj91G21SsK5QzLJqoKJjEjS8/f3An/WkWg66zAYz8j15zIV3+oktM+B/+/g7atA1iXTyOzbC5Upm++31sVXrtG86gOivtKLJcnB/qAm7XdDSsnKPd/wzElVmtmgdGoB3UYdFaRETBnbftp6c2bTPhc07HQKOES+M51M2d658w/n33dPOgQx2VeQBZahjTDeRiEqKo4a37R2jOSiiMlMQZ2G/27sQWtdnTWuhNr0HdyTCJ2nvjrCaEa3gsg86+c98BylbXHDGoWA6Ez5BroZ/ZgY0FL30VCNbLX4K6BmzqkgbinNU9nZ7YsNGEd23HoGm/MEWRFOL3vA769D//yIQnCO36N3/3DtrXA/H4R7dgoKz4wGnVjdtJStDObYjrvnt17Xfdy6qDf9+hMUqoJe19B/HNF/+9l3r2Q3n0zg+CkA/2+rRlJ56VeBYjWlHAluO5QShehggxxThpg67C1KfE9FmcHFERN8Zz1J/fu924Cy5qoE9+cZ5xn795kXFfvnklvf3PFhvXs2uncQf3ZaipfdS4xmYRA4TqBLR8VcK4M8/top6DR4yrT8UpmY4ad+31lxn3xPad9K9feMq4DDMCcYs6G2jB4jrj/PgET8KzxrV08ASd3Stfdx3d80Cfcd/+Xh+LLsxp2MnqmLhvfmMfbX7qsHGyE0Oc3CtQn0wbl0h2mvsQxGVKUePGKz6NlwPj1j+wnf7gD7qN++x/zTbuy/+zjD7zxUXGLZwTN279g/sM1xJXMcIx/okJCXGyZuZFo8ZF25nSt3AFi6siSQ4zMHH1ja5xUY6nwJ+KGy8ENFgsGzdcrBgnaqq65qhxPtfjaNE3ruBFjctkuF05Q8ZxncTqfeOylbxxshph61nOCIobzcNgg7g1Z7dTR1fRuC/f/DPjRjnrMZpm3N33TtBD6/uMO3PVBcatXRWns8+bbtz3v7fZuBH+Js3lEzeQjdDnPvewcUWnx7jzr64nL851xc6PB5Ruihi3eNlM43522wYaGx82btWa+cZVggqlWgrGYS1yahByuhAhphgnbU7nBHGmxqAWsSg24B46OE5bn8H+wr/+wOvorLVYBK7FG958kfFv+fEjxt/w8E5KxnTeYxanoUxoYyoseNXrZtKDd+OIiB+Ra7GwpLD2dOxDnNaVpk/+PfZJ7ngSewoXrW2iLb1QE//8tu10zWVnmbCd1qw8ewk5detN+CMf+h/asnmdCQdRcJH//vKdlO7AXOvsS7FIL7qNRBPmd9+/9XHa14s5TLGEeVZLZ5HWnX+xCfcODtMrbsCm4zNOg1pbZjTLF2Pp4oorodi5664HaMdO7EtdtqSJCububpmHoV7F+E7Vvow3SG4JZbKUU0wNVvSkfVQvh7zj51vpzTcin3KgmHmUCRcrmPPd+CdX05z5mOMUeD73uc/+zIRv/QbmYX6B545LMA9853vew+VCnm/5PtrrwDOD5Fd0HqiKh3xunN7+XrTrhWevoNla5v/+BuJu6opQxzTk83+/toUmcvj+zLNxT0JLiuiSK6Ec+psP4dDsX7yrg1afDkXdk0/00He/dr8JX3bVmcZfsCpJD29APLFIgeze5wvPQZw//NpttO6CC0x44XQ0fKSUobiPssWP2XP6XOKkDTqZ5FtLwTb7W7c9SsPjG0x4/pIbqtPTogdtVdJJ0sxZ0Fq4FUxo773zKe5EqDFj/Yq7nMBVzdWLXzqPvnoWOvhTGx+nSgm3gC5bgs7wjr+4gD7w3q+Z8Hf+Fxtp6X9FU4Y0461L6PrXnmfCrqqpTj+9jW54C5QqX/z4f9Kn/+luE4agJ2imv/3YK0xoka7tZbkMRRdrQDu2HKa9u3C6uyIyGKO+2aWNj2HQZHM57tjYBiPnCgVFf4zLj060eCmI1Je/+ig9+TgG58olV1Bc7mVmxFSL6vsFKpW1NwX9lEqAEDiq9JCLOYIolDpBFBq6/r499J1v4mRBJNJCMRa9BJUSFCIXXrSa5i5EB69w53/0p1BMbNCrn/3gCLcd2uY1fzxBEV3T27MRBG3Plgwnhp0m5Orw57q54iU4I3gNj71XvwUa5vUPol4/+0mxDGDF4g664YY/NaGrrwZRLrPQ+4rroeX90hfR7t+8WU7p228q1D4TF6m8673XGL+Fkx4eQZ6G+/eRKldp7XIMeKIeWr4IyrQmdCkqjg1TdgjKLE9MRaT0h+cYlkiGCBFiinDS1ul4us4iD9Z+LKs+cKCfdm4HNzjznJXU2gq27iqni/FXrtpVWX8/qFTZDZiC49ncufW0UDlERW2tRCJJ2vg0xKq+w4O07pLlJlyXRjE8N0EP3481uf5eULG8y5RYzeV1TZtB11yx0oQpgGgRjcZoUE+j33/XFhofRV4SalyntaWDxR2oxhuS+Gai7NKmp3D6OjOS4DLrGqUHalnxcjSmZ9ca6pto3aXzTbi5GVxaqt2JIP7DPeCojzz0OK1Yg2M4K1bMZ66I547uyhFDRzkf3zzwyB6aOws7WZbNs0sWYpoOHP/hx7DEMThYolQMorcTqTPGiQS5IvK2eu0MrjvkecPjXF8V7EhpjOIbr5wnPw71/ZqzFtPmbRBLJ3KIR26ktZa0fT2KJDfDrjkTSzjTu+JVieKRB5CnA3t6OC9IM13XQOsuxvnK7ukoW4UmKKFC2COPof/s2zVOZd1vmUzEacFi1OcZZ0M0lg3h+w/h3R4WeS+7FGL62IiK2Xc8SqtW45vVa5C3DY8cptFB9KsrXrS0xiQj8vu83/DsmbkCGtwy0GOZtaeileyIF0RojEM2jA50NERGQCe3Z9M8FqFipB2cJ2UQdkRs047JwolTzcezVFrVviI6i9m+VjU8+2zAu9ztjR8hmXfqpNDAVuOzNRTkHWuvUTY022/k3MCx8KqiLf9ebaKAKmosNn7UNxBt5XdX8xRTUxO/jGPzKXVhn/068UrK8OveORrGVmYE4qlTbTcVkY+BiJWTQPmZnBn/2WdC+Ea6cUTFcUGgdRLhGABJ09Y9FuYjhMFnwFVg+6JFuA0sRIhTBCeN0wmNrVLPqo1HQS1lxe/2cDTJpSD6h6tKGKG79kolofB2J5a1Ehw4Gf5dqZtQzgBczVr6DSKFatie6zP5qRIt/q2avuZTrpWyz8wlHZbi6u/mPY3AxmPiVm4UEUWCpcSWslYTxLv2Wi6bNfnZvmK3zcmD2rqzlp11F4r8VlH7nEKV47qxPKK+bLtzNR+Oiu2OOedo82nLJdD8Gm2nLSfXsabJAXjyfTVP+kxgGzFaZC6jZbZ16LBU4EJMlWeROKQV25Se4V61UoLNl60jTk+TilU5loiWED+R99qKFJ/j8zXOqKirtBxVbiWpaw7sN9JW1uS8eNWOCYScLkSIUwQnjdMZwnIsYRCqaamsUHrLeSyVkom3fqPLS/xI9jHgYdR3mMHpu9W4+W97WNZQLEsx7Qv8u1KyKkFjVMORMr9pKZ7OLZja2d89jtte3DFJgfn94Jg5krnlFQohpG05nC2j0DPLwcTH744e8I2ZerFVr77UT/V+cIbd92jLw3VotfJyoDWq14dxpuGzNykxWL4icds8ia/v2qQZVjIweVKuZhkAYP/wOf8al3YbWcsM7EWRthGjXC++nbdVKOJgLkUBlGKB4aaaD5NPLYfNp5UKGLatkEfNh5F67Df2XfY1T77Eb7mvRS0Xs41d8wgCRM0DxvNekRIiRIgTgyU3IUKEmCKEgy5EiCkF0f8FYF8fN2scs6EAAAAASUVORK5CYII='

    doc.addImage(imgData, 'JPEG', 150, 5, 45, 28);


    doc.setFillColor(0, 102, 255)
    doc.rect(10, 15, 55, 8, 'FD')
    doc.rect(10, 23, 55, 8)
    doc.setFontSize(10)
    doc.setFontType('bold')
    doc.setTextColor(255, 255, 255)
    doc.text(36, 21, 'INVOICE NUMBER', 'center')
    doc.setTextColor(0, 0, 0)
    doc.text(36, 28.6, '' + factura.factura_sigla, 'center')

    doc.setTextColor(0, 102, 255)
    doc.text(11.5, 41, 'IPLC INVOICE')


    doc.setFillColor(0, 102, 255)
    doc.rect(10, 60, 50, 8, 'FD')
    doc.rect(10, 68, 50, 8)
    doc.setTextColor(255, 255, 255)
    doc.text(21, 66, 'CURRENCY')
    doc.setTextColor(0, 0, 0)
    doc.text(30, 74, '' + (factura.moeda_iso == null ? '' : factura.moeda_iso))

    doc.setFontType('normal')
    doc.setFontSize(11)
    doc.text(111, 38, 'DIRECÇÃO DE VENDA A GROSSO-WHOLESALE ')
    doc.text(109, 43, 'Deptº de Venda a Grosso Internacional e Interligação')
    doc.text(112, 49, 'Facturação e Cobrança Internacional e Interligação ')
    doc.text(161, 54, 'Rua das Kipacas n 186 ')
    doc.text(169, 59, '6 Andar Porta 602 ')
    doc.text(172, 64, 'Luanda - Angola ')
    doc.text(176, 69, 'P.O. Box: 625')
    doc.text(159, 74, 'Tel: + 244 222 63 03 52 ')
    doc.text(192.3, 79, 'Fax:')

    doc.setFontType('bold')
    doc.setLineWidth(0.5)
    doc.line(10, 80, 200, 80)

    doc.setFontSize(10)
    doc.setTextColor(0, 0, 0)
    doc.text(10, 95, 'CUSTOMER:')
    doc.text(10, 100, '' + cliente.nome)
    doc.setFontType('normal')
    doc.text(10, 105, 'VAT Reg. No: ' + (cliente.contribuente == '999999999' || cliente.contribuente == null || cliente.contribuente == 0 ? 'Consumidor Final' : cliente.contribuente))
    doc.text(10, 110, 'Address: ' + (cliente.morada == null ? '' : cliente.morada))
    doc.text('Conta: ' + (factura.conta_id == null ? '' : factura.conta_id), 10, 115);
    doc.setFontType('bold')
    //doc.text(98, 110, 'Roma- Italia')

    var Period = new Date(tecnologia.ano + '-' + tecnologia.mes + '-01');

    var somaMonth = new Date(Period);
    var d2 = somaMonth.setMonth(somaMonth.getMonth() + 1);

    var lastDay = new Date(d2)

    var anoCc = lastDay.getFullYear();
    var mesCc = lastDay.getMonth();

    var getLastDay = new Date(anoCc, mesCc + 1, 0);

    doc.setFontSize(10)
    doc.setFillColor(0, 102, 255)
    doc.rect(122, 90, 33, 8, 'FD')
    doc.setTextColor(255, 255, 255)
    doc.text(141, 96, 'Period:')
    doc.rect(155, 90, 45, 8)
    doc.setTextColor(0, 0, 0)
    doc.setFontType('normal')
    doc.text(162, 96, '' + moment(Period).locale('en').format('MMMM').toUpperCase() + '.' + moment(Period).format('YYYY'))

    doc.setFillColor(0, 102, 255)
    doc.rect(122, 98, 33, 8, 'FD')
    doc.setTextColor(255, 255, 255)
    doc.setFontType('bold')
    doc.text(144.4, 103, 'Date:')
    doc.rect(155, 98, 45, 8)
    doc.setTextColor(0, 0, 0)
    doc.setFontType('normal')
    doc.text(162, 103, '' + moment(factura.created_at).format('DD') + '.' + moment(factura.created_at).locale('en').format('MMMM') + '.' + moment(factura.created_at).format('YYYY'))

    doc.setFillColor(0, 102, 255)
    doc.rect(122, 106, 33, 8, 'FD')
    doc.setTextColor(255, 255, 255)
    doc.setFontType('bold')
    doc.text(136, 111, 'Due Date:')
    doc.rect(155, 106, 45, 8)
    doc.setTextColor(0, 0, 0)
    doc.setFontType('normal')
    doc.text(162, 111, '' + moment(getLastDay).format('DD') + '.' + moment(d2).locale('en').format('MMMM') + '.' + moment(d2).format('YYYY'))

    doc.setFontType('bold')
doc.setFontSize(10)
doc.setFillColor(0, 102, 255)
doc.rect(10, 120, 190, 15, 'F')

doc.setTextColor(255, 255, 255)
doc.text(90, 125, 'Monthly Account')

/*
doc.setDrawColor(230, 230, 230)
doc.setLineWidth(0.5)
doc.line(10, 127, 200, 127)
doc.text(13, 133, 'Month')
doc.text(28, 133, 'Circuit Designation')
doc.text(70, 133, 'Origin')
doc.text(91, 133, 'Dest')
doc.text(106, 133, 'Capacity')
doc.text(128, 133, 'Description')
doc.text(160, 133, 'W/VAT')
doc.text(182, 133, 'Amount')
doc.setFontSize(7.5)
doc.setFontType('normal')
doc.setTextColor(0, 0, 0)
var y=0;
y+=141;
var total = 0;
var totalSemImposto = 0;
for (var i = 0; i < produtos.length; i++) {
    doc.text(14, y, '' + moment(Period).format('YYYY') + '' + moment(Period).format('DD'))
    doc.text(29, y, '' + (produtos[i].chaveServico == null ? '' : produtos[i].chaveServico), {maxWidth: 38})
    doc.text(70, y, '' + (produtos[i].origem == null ? '' : produtos[i].origem), {maxWidth: 15})
    doc.text(90, y, '' + (produtos[i].destino == null ? '' : produtos[i].destino), {maxWidth: 15})
    doc.text(113, y, '' + (produtos[i].capacidade == null ? '' : produtos[i].capacidade ),'center')
    doc.text(140, y, 'MONTHLY R.CHARGE','center')
    doc.text(173, y, '' + this.numberFormat(produtos[i].linhaTotalSemImposto),'right')
    doc.text(197, y, '' + this.numberFormat(produtos[i].total),'right')
    doc.line(10, y+5, 200, y+4)
    y+=10;
    total += produtos[i].total;
    totalSemImposto += produtos[i].linhaTotalSemImposto;
    newPage()
}
*/

doc.setDrawColor(230, 230, 230)
doc.setLineWidth(0.5)
doc.line(10, 127, 200, 127)
doc.text(13, 133, 'Circuit Designation')
doc.text(55, 133, 'Origin')
doc.text(75, 133, 'Dest')
doc.text(90, 133, 'Capacity')
doc.text(112, 133, 'Description')
doc.text(144, 133, 'W/VAT')
doc.text(166, 133, 'VAT')
doc.text(182, 133, 'Amount')
doc.setFontSize(7.5)
doc.setFontType('normal')
doc.setTextColor(0, 0, 0)
var y=0;
y+=141;
var total = 0;
var totalSemImposto = 0;
var totalImposto = 0;
for (var i = 0; i < produtos.length; i++) {
    doc.text(13, y, '' + (produtos[i].chaveServico == null ? '' : produtos[i].chaveServico), {maxWidth: 38})
    doc.text(55, y, '' + (produtos[i].origem == null ? '' : produtos[i].origem), {maxWidth: 15})
    doc.text(74, y, '' + (produtos[i].destino == null ? '' : produtos[i].destino), {maxWidth: 15})
    doc.text(97, y, '' + (produtos[i].capacidade == null ? '' : produtos[i].capacidade ),'center')
    doc.text(124, y, 'MONTHLY R.CHARGE','center')
    doc.text(157, y, '' + this.numberFormat(produtos[i].linhaTotalSemImposto),'right')
    doc.text(174, y, '' + this.numberFormat(produtos[i].valorImposto),'right')
    doc.text(197, y, '' + this.numberFormat(produtos[i].total),'right')
    doc.line(10, y+5, 200, y+4)
    y+=10;
    total += produtos[i].total;
    totalImposto += produtos[i].valorImposto;
    totalSemImposto += produtos[i].linhaTotalSemImposto;
    /* //newPage()
    if (doc.internal.pageSize.height < (y + 145)) {
      doc.addPage();
     


  } */
}

console.log(doc.internal.pageSize.height)
console.log(y)


//
doc.setFontType('bold')
doc.setFontSize(12)
doc.setFillColor(0, 102, 255)
doc.rect(10, y + 1, 190, 10, 'F')

doc.setTextColor(255, 255, 255)
doc.setDrawColor(230, 230, 230)
doc.setLineWidth(0.5)
doc.setFontType('bold')
doc.setFontSize(12)
doc.text(13, y + 8, 'Total')

doc.setFontSize(7.5)
doc.setFontType('bold')
doc.text(157, y + 8, '' + this.numberFormat(totalSemImposto),'right')
doc.text(174, y  + 8, '' + this.numberFormat(totalImposto),'right')
doc.text(197, y  + 8, '' + this.numberFormat(total),'right')
//doc.text(197, y - 24, 'Amount')
//

doc.setDrawColor(0, 0, 0)
doc.setFontType('bold')
doc.setLineWidth(0.5)
doc.line(10, y + 28, 200, y +28)


doc.setFontSize(10)
doc.setTextColor(0, 0, 0)
doc.text(18, y + 25, 'TOTAL')

doc.setFillColor(0, 102, 255)
doc.rect(160, y + 13, 40, 7, 'FD')
doc.rect(160, y + 20, 40, 7)
doc.setFontType('bold')
doc.setTextColor(255, 255, 255)
doc.text(180.5, y + 18, 'Total ' + (factura.moeda_iso == null ? '' : factura.moeda_iso))
doc.setTextColor(0, 0, 0)
doc.text(170, y  + 26, ''+ this.numberFormat(total))


if (doc.internal.pageSize.height < (y + 107)) {
  doc.addPage();

}

footer();

function footer() {
doc.setFontType('normal')
doc.text(40, doc.internal.pageSize.height - 75, 'Elabored by:')
doc.text(10, doc.internal.pageSize.height - 68, '______________________________________')
doc.setFontType('bold')
doc.text(30, doc.internal.pageSize.height - 62, 'Nair Pereira ')
//doc.text(30, doc.internal.pageSize.height - 62, 'Inocêncio Fernandes ')
doc.setFontType('normal')
doc.text(14, doc.internal.pageSize.height - 57, 'Wholesale Billing & Collection')
doc.text(10, doc.internal.pageSize.height - 52, 'E-mail:nair.pereira@angolatelecom.ao')
//doc.text(10, doc.internal.pageSize.height - 52, 'E-mail:Inocencio.Fernandes@angolatelecom.ao')

doc.text(150, doc.internal.pageSize.height - 75, 'Verified by:')
doc.text(120, doc.internal.pageSize.height - 68, '_____________________________________')
doc.setFontType('bold')
doc.text(150, doc.internal.pageSize.height - 62, 'Miguel Paulo')
//doc.text(150, doc.internal.pageSize.height - 62, 'Carlitos Vú')
doc.setFontType('normal')
doc.text(134, doc.internal.pageSize.height - 57, 'Agreement Settlement Manager')
doc.text(125, doc.internal.pageSize.height - 52, 'E-mail: miguel.paulo@angolatelecom.ao')

doc.setFontSize(9)
doc.setFontStyle('italic');
doc.setTextColor(0);
//doc.text(doc.internal.pageSize.width - 63, doc.internal.pageSize.height - 44, 'PROCESSADO POR COMPUTADOR')
doc.text(doc.internal.pageSize.width - 50, doc.internal.pageSize.height - 40, 'Unig versão 1.0')


doc.setFontSize(10)
doc.setFontType('bold')
doc.text(10, doc.internal.pageSize.height - 27, 'NOTICE:')
doc.setFontType('normal')
doc.text(10, doc.internal.pageSize.height - 22, 'Please note that,if you do not question our  invoice/statement within 30 days after receipt, it will be considered as accepted.')

doc.setFontSize(9);
      doc.line(15, doc.internal.pageSize.height - 18, 196, doc.internal.pageSize.height - 18); // vertical line
      var hash = factura.hash.substring(0, 1);
      hash += factura.hash.substring(10, 11);
      hash += factura.hash.substring(20, 21);
      hash += factura.hash.substring(30, 31);
      doc.setFontSize(7);
      
      doc.text("NIF: " + user.taxRegistrationNumber + " - " + user.companyName + " / " + user.addressDetail + " / " + user.telefone + " / " + user.email, 105, doc.internal.pageSize.height - 14, null, null, 'center');
      
      doc.setFontSize(8);
      doc.text(hash + '-Processado por programa validado 4/AGT119', 105, doc.internal.pageSize.height - 10, null, null, 'center');
      
}
function newPage(){
    if(y + 30 > doc.internal.pageSize.height)
    {
        //y = 10;
        doc.addPage();
        //footer();
    }
}

    doc.autoPrint();
    doc.output("dataurlnewwindow");
  }

  public numberFormat(number) {
    return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number).replace('€', '').trim();
  }


}

