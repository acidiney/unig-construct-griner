import { TestBed } from '@angular/core/testing';

import { NotaCreditoInterconexaoService } from './nota-credito-interconexao.service';

describe('NotaCreditoInterconexaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotaCreditoInterconexaoService = TestBed.get(NotaCreditoInterconexaoService);
    expect(service).toBeTruthy();
  });
});
