import { TestBed } from '@angular/core/testing';

import { FacturaPosPagoVozService } from './factura-pos-pago-voz.service';

describe('FacturaPosPagoVozService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacturaPosPagoVozService = TestBed.get(FacturaPosPagoVozService);
    expect(service).toBeTruthy();
  });
});
