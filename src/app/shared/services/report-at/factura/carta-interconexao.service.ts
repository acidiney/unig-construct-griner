import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from '@core/providers/http/http.service';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Injectable({
  providedIn: 'root'
})
export class CartaInterconexaoService {


  public pdfEvent = new EventEmitter<Object>();

  constructor(private http: HttpService) { }

  public imprimirCartaInterconexao(factura: any, produtos: any[], cliente: any, user: any, original: any, pagamento: any, servico_conta: any, tecnologia: any, detalhesInterconexao: any, detalhesInterconexaoInt: any, detalhesInterconexaoNac: any) {


    var imgData = '' + user.logotipo;
    var doc = new jsPDF()

    doc.setProperties({
      title: 'Factura_Interconexao',
      subject: 'Factura Interconexão',
      author: 'ITGEST',
      //keywords: '',
      creator: 'UNIG'
    });

    doc.addImage(imgData, 'JPEG', 12, 15, 33, 18)

    textoLateral();
    function textoLateral() {
      doc.setFontSize(9);
      doc.setFont("times");
      doc.setTextColor(0, 83, 155);

      doc.text(142, 290, 'Wholesale -ANGOLATELECOM-Empresa de Telecomunicações de Angola E.P. Contribuinte Nº 5410000323, Rua das Quipacas nº 186 Luanda-Angola. Tel.:222 700 000 | 222 800 220,.www.angolatelecom.ao', 'center', 90);
    }
    //corpo
    doc.setFontSize(13);
    doc.setFont("times");
    doc.setFontType("bold");
    doc.setTextColor(0);
    //doc.text('Angola Telecom', 45, 20 );

    doc.text('Direcção de Venda A Grosso - WHOLESALE', 75, 30);
    doc.setLineWidth(0.3);
    doc.line(50, 35, 190, 35);
    doc.setFontSize(8);
    doc.setFontType("times");
    doc.text('VAT: 5410000323', 13, 45);
    doc.text('Rua das Quipacas Nº 186, 7º Andar Porta 707 -Luanda', 13, 50);
    doc.text('Luanda -Angola', 13, 55);

    doc.setFontSize(8);
    doc.setFont("times");
    doc.setFontType("normal");
    doc.text('P.O BOX: 625', 115, 45);
    doc.text('Email: GR_DEC_VGII@angolatelecom.ao', 115, 50);
    doc.text('Phone: + 244 222 63 00 41 / 63 07 23', 115, 55);

    doc.setFontSize(8);


    doc.setFontType("bold");
    doc.setFont("arial");
    doc.setLineWidth(0.3);
    doc.line(13, 60, 190, 60);

    var d = new Date(tecnologia.ano + '-' + tecnologia.mes + '-01');

    var anoC = d.getFullYear();
    var mesC = d.getMonth();

    var d2 = new Date(anoC, mesC + 1, 0);

    var Period = new Date(tecnologia.ano + '-' + tecnologia.mes + '-01');

    var somaMonth = new Date(Period);


    var d3 = somaMonth.setMonth(somaMonth.getMonth() + 1);

    var lastDay = new Date(d3)

    var anoCc = lastDay.getFullYear();
    var mesCc = lastDay.getMonth();

    var getLastDay = new Date(anoCc, mesCc + 1, 0);

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setFont("times");
    doc.text('Document Type:', 13, 70);
    doc.text('Document Number:', 13, 75);
    //doc.text('Document Reference:', 13, 80);
    //doc.text('Contract Number:', 13, 85);
    doc.text('Issue Date:', 13, 80);
    doc.text('Account Period:', 13, 85);
    doc.text('Deadline payment:', 13, 90);

    doc.setFontType("normal");
    doc.text('Invoice', 50, 70);
    doc.text('' + factura.factura_sigla, 50, 75);
    //doc.text('XXX', 50, 80);
    //doc.text('XXX', 50, 85);
    doc.text('' + moment(factura.created_at).format('DD') +
      '-' + moment(factura.created_at).format('MM') +
      '-' + moment(factura.created_at).format('YYYY'), 50, 80);
    doc.text(50, 85, '01-' + tecnologia.mes + '-' + tecnologia.ano + ' a ' + moment(d2).format('DD') +
      '-' + moment(d2).format('MM') +
      '-' + moment(d2).format('YYYY'));
    doc.text('01' + '-' + moment(d3).format('MM') + '-' + moment(d3).format('YYYY'), 50, 90);

    doc.setFontType("normal");
    doc.setFont("times");
    doc.text('INVOICE TO:', 115, 70);
    doc.setFontType("bold");
    doc.setFont("times");
    doc.text('' + cliente.nome, 134, 70, { maxWidth: 55 });
    doc.setFontType("normal");
    doc.setFont("times");
    doc.text('VAT: ' + (cliente.contribuente == '999999999' || cliente.contribuente == null || cliente.contribuente == 0 ? 'Consumidor Final' : cliente.contribuente), 115, 77);
    doc.text('' + (cliente.morada == null ? '' : cliente.morada), 115, 80, { maxWidth: 68 });
    doc.text('P.O. Box:', 115, 87);
    //doc.text('Dublin 2:', 115, 90);
    //doc.text('Ireland:', 115, 95);

    doc.setLineWidth(0.3);
    doc.line(13, 104, 190, 104);

    var y = 123;
    var yRect = 119.7;
    var yLine = 119.4;
    var total = 0;


    if (detalhesInterconexaoNac.length > 0) {

      var totalMinuto = 0;
      var totalRate = 0;
      var totalAmount = 0;

      doc.setFillColor(166, 166, 166);
      doc.rect(13, 107, 181, 6, 'F')

      doc.setFontSize(11);
      doc.setTextColor(255, 255, 255);
      doc.setFontStyle('times');
      doc.setFontType("bold");
      doc.text(105, 111, 'National Monthly Incomming Account', 'center');

      doc.setFillColor(32, 55, 100);
      doc.rect(13, 113, 181, 6, 'F')


      doc.setFontSize(8);
      doc.setTextColor(255, 255, 255);
      doc.setFontStyle('times');
      doc.setFontType("bold");
      doc.text(15, 117, 'Traffic Month');
      doc.text(40, 117, 'Origin');
      doc.text(65, 117, 'Destination');
      doc.text(95, 117, 'Service');
      doc.text(120, 117, 'Call');
      doc.text(135, 117, 'Minutes');
      doc.text(157, 117, 'Rate');
      doc.text(173, 117, 'Amount ' + factura.moeda_iso);



      doc.setDrawColor(115, 115, 115);
      doc.setLineWidth(0.7);
      doc.line(13, 119.4, 194, 119.4);


      for (var i = 0; i < detalhesInterconexaoNac.length; i++) {
        doc.setFillColor(255, 255, 255);
        doc.setFontSize(7);
        doc.setFontStyle('times');
        doc.setFontType("normal");
        //doc.rect(13, yRect, 181, 5, 'F')
        doc.setTextColor(0);
        doc.text(18, y, '' + detalhesInterconexaoNac[i].periodo);
        doc.text(33, y, '' + (detalhesInterconexaoNac[i].origin == null ? '' : detalhesInterconexaoNac[i].origin));


        doc.text(60, y, '' + (detalhesInterconexaoNac[i].destination == null ? '' : detalhesInterconexaoNac[i].destination), { maxWidth: 27, align: 'left' });

        doc.text(85, y, '' + (detalhesInterconexaoNac[i].service == null ? '' : detalhesInterconexaoNac[i].service), { maxWidth: 30 });
        doc.text(124, y, '' + (detalhesInterconexaoNac[i].call == null ? '' : detalhesInterconexaoNac[i].call), 'center');
        doc.text(140, y, '' + (detalhesInterconexaoNac[i].minutes == null ? '' : this.numberFormat(detalhesInterconexaoNac[i].minutes)), 'center');
        doc.text(160, y, '' + (detalhesInterconexaoNac[i].rate == null ? '' : this.numberFormatRate(detalhesInterconexaoNac[i].rate)), 'center');
        doc.text(190, y, '' + this.numberFormat(detalhesInterconexaoNac[i].amount), 'right');


        doc.setDrawColor(115, 115, 115);
        doc.setLineWidth(0.3);
        doc.line(13, yLine, 194, yLine);

        var destinationLength = (detalhesInterconexaoNac[i].destination == null ? 0 : detalhesInterconexaoNac[i].destination.length);
        var serviceLength = (detalhesInterconexaoNac[i].service == null ? 0 : detalhesInterconexaoNac[i].service.length);
        /*
              if(destinationLength < 24){
                y+=5;
                yLine+= 5;
              }else if(destinationLength > 46){
                y+=11;
                yLine+=11;
              } else if(destinationLength > 24 ){
                y+= 8;
                yLine+= 8;
              }
              */


        total += detalhesInterconexaoNac[i].amount;
        totalMinuto += detalhesInterconexaoNac[i].minutes;
        totalRate += detalhesInterconexaoNac[i].rate;
        totalAmount += detalhesInterconexaoNac[i].amount;
        y += (destinationLength > 24 ? 8 : 5 && serviceLength > 26 ? 8 : 5);
        yRect += 5;
        yLine += (destinationLength > 24 ? 8 : 5 && serviceLength > 26 ? 8 : 5);

        if (doc.internal.pageSize.height < (y + 35)) {
          doc.addPage();
          textoLateral();
          y = 33.5;
          yLine = 30;


        }
      }



      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.3);
      doc.line(13, yLine, 194, yLine);

      doc.setDrawColor(179, 179, 179);
      doc.setLineWidth(0.3);
      doc.line(105, yLine + 2, 194, yLine + 2);

      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.setFont("times");
      doc.setFontSize(8);
      doc.text('TOTAL', 13, y + 2);

      doc.text(122, y + 2, '', 'center');
      doc.text(140, y + 2, '' + this.numberFormat(totalMinuto), 'center');
      //doc.text(140, y + 2, ''+(factura.minutos_interconexao == null ? '' : factura.minutos_interconexao), 'center');
      doc.text(192, y + 2, '' + this.numberFormat(totalAmount), 'right');

      doc.setDrawColor(179, 179, 179);
      doc.setLineWidth(0.3);
      doc.line(13, y + 3, 194, y + 3);
      /*
          doc.text('VAT TAX', 13, y + 11);
          doc.setDrawColor(0, 0, 0);
          doc.setLineWidth(0.3);
          doc.line(13, y + 12.5, 194, y + 12.5);
      
          doc.text('Total VAT', 13, y + 16);
      
          doc.setTextColor(0);
          doc.setFontType("normal");
          doc.setFont("times");
          doc.setFontSize(8);
          //doc.setFillColor(217, 217, 217);
          //doc.rect(145, y-2, 49, 5, 'F')
      
          doc.text(192, y + 11, ''+ this.numberFormat(produtos[0].vImposto)+"%", 'right');
      
          doc.setFontType("bold");
          doc.text(192, y + 16, ''+ this.numberFormat(totalRate), 'right');
          */


    }

    if (detalhesInterconexaoInt.length > 0) {

      var totalMinuto = 0;
      var totalRate = 0;
      var totalAmount = 0;

      (detalhesInterconexaoNac.length > 0 ? y = y : y = 123);
      (detalhesInterconexaoNac.length > 0 ? yRect = 119.7 + y : yRect = 119.7);
      //var yRect = 119.7 + y;


      doc.setFillColor(166, 166, 166);
      doc.rect(13, (detalhesInterconexaoNac.length > 0 ? y + 24 : 107), 181, 6, 'F')

      doc.setFontSize(11);
      doc.setTextColor(255, 255, 255);
      doc.setFontStyle('times');
      doc.setFontType("bold");
      doc.text(105, (detalhesInterconexaoNac.length > 0 ? y + 28 : 111), 'Interational Monthly Incomming Account', 'center');

      doc.setFillColor(32, 55, 100);
      doc.rect(13, (detalhesInterconexaoNac.length > 0 ? y + 30 : 113), 181, 6, 'F')


      doc.setFontSize(8);
      doc.setTextColor(255, 255, 255);
      doc.setFontStyle('times');
      doc.setFontType("bold");
      doc.text(15, (detalhesInterconexaoNac.length > 0 ? y + 34 : 117), 'Traffic Month');
      doc.text(40, (detalhesInterconexaoNac.length > 0 ? y + 34 : 117), 'Origin');
      doc.text(65, (detalhesInterconexaoNac.length > 0 ? y + 34 : 117), 'Destination');
      doc.text(95, (detalhesInterconexaoNac.length > 0 ? y + 34 : 117), 'Service');
      doc.text(120, (detalhesInterconexaoNac.length > 0 ? y + 34 : 117), 'Call');
      doc.text(135, (detalhesInterconexaoNac.length > 0 ? y + 34 : 117), 'Minutes');
      doc.text(157, (detalhesInterconexaoNac.length > 0 ? y + 34 : 117), 'Rate');
      doc.text(173, (detalhesInterconexaoNac.length > 0 ? y + 34 : 117), 'Amount ' + factura.moeda_iso);



      doc.setDrawColor(115, 115, 115);
      doc.setLineWidth(0.7);
      doc.line(13, (detalhesInterconexaoNac.length > 0 ? y + 36.4 : 119.4), 194, (detalhesInterconexaoNac.length > 0 ? y + 36.4 : 119.4));

      (detalhesInterconexaoNac.length > 0 ? y = y + 40 : y = 123);
      (detalhesInterconexaoNac.length > 0 ? yLine = y + 2 : yLine = 119.4);
      //y = y + 40;
      //var yLine = y +2;
      var total = 0;
      var totalMinuto = 0;
      for (var i = 0; i < detalhesInterconexaoInt.length; i++) {
        doc.setFillColor(255, 255, 255);
        doc.setFontSize(7);
        doc.setFontStyle('times');
        doc.setFontType("normal");
        //doc.rect(13, yRect, 181, 5, 'F')
        doc.setTextColor(0);
        doc.text(18, y, '' + detalhesInterconexaoInt[i].periodo);
        doc.text(33, y, '' + (detalhesInterconexaoInt[i].origin == null ? '' : detalhesInterconexaoInt[i].origin));


        doc.text(60, y, '' + (detalhesInterconexaoInt[i].destination == null ? '' : detalhesInterconexaoInt[i].destination), { maxWidth: 27, align: 'left' });

        doc.text(85, y, '' + (detalhesInterconexaoInt[i].service == null ? '' : detalhesInterconexaoInt[i].service), { maxWidth: 30 });
        doc.text(124, y, '' + (detalhesInterconexaoInt[i].call == null ? '' : detalhesInterconexaoInt[i].call), 'center');
        doc.text(140, y, '' + (detalhesInterconexaoInt[i].minutes == null ? '' : this.numberFormat(detalhesInterconexaoInt[i].minutes)), 'center');
        doc.text(160, y, '' + (detalhesInterconexaoInt[i].rate == null ? '' : this.numberFormatRate(detalhesInterconexaoInt[i].rate)), 'center');
        doc.text(190, y, '' + this.numberFormat(detalhesInterconexaoInt[i].amount), 'right');


        doc.setDrawColor(115, 115, 115);
        doc.setLineWidth(0.3);
        doc.line(13, yLine, 194, yLine);

        var destinationLength = (detalhesInterconexaoInt[i].destination == null ? 0 : detalhesInterconexaoInt[i].destination.length);
        var serviceLength = (detalhesInterconexaoInt[i].service == null ? 0 : detalhesInterconexaoInt[i].service.length);
        /*
              if(destinationLength < 24){
                y+=5;
                yLine+= 5;
              }else if(destinationLength > 46){
                y+=11;
                yLine+=11;
              } else if(destinationLength > 24 ){
                y+= 8;
                yLine+= 8;
              }
              */


        total += detalhesInterconexaoInt[i].amount;
        totalMinuto += detalhesInterconexaoInt[i].minutes;
        totalRate += detalhesInterconexaoInt[i].rate;
        totalAmount += detalhesInterconexaoInt[i].amount;
        y += (destinationLength > 24 ? 8 : 5 && serviceLength > 26 ? 8 : 5);
        yRect += 5;
        yLine += (destinationLength > 24 ? 8 : 5 && serviceLength > 26 ? 8 : 5);

        if (doc.internal.pageSize.height < (y + 35)) {
          doc.addPage();
          textoLateral();
          y = 33.5;
          yLine = 30;


        }
      }



      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.3);
      doc.line(13, yLine, 194, yLine);

      doc.setDrawColor(179, 179, 179);
      doc.setLineWidth(0.3);
      doc.line(105, yLine + 2, 194, yLine + 2);

      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.setFont("times");
      doc.setFontSize(8);
      doc.text('TOTAL', 13, y + 2);

      doc.text(122, y + 2, '', 'center');
      doc.text(140, y + 2, '' + this.numberFormat(totalMinuto), 'center');
      //doc.text(140, y + 2, ''+(factura.minutos_interconexao == null ? '' : factura.minutos_interconexao), 'center');
      doc.text(192, y + 2, '' + this.numberFormat(totalAmount), 'right');

      doc.setDrawColor(179, 179, 179);
      doc.setLineWidth(0.3);
      doc.line(13, y + 3, 194, y + 3);
      /*
          doc.text('VAT TAX', 13, y + 11);
          doc.setDrawColor(0, 0, 0);
          doc.setLineWidth(0.3);
          doc.line(13, y + 12.5, 194, y + 12.5);
      
          doc.text('Total VAT', 13, y + 16);
      
          doc.setTextColor(0);
          doc.setFontType("normal");
          doc.setFont("times");
          doc.setFontSize(8);
          //doc.setFillColor(217, 217, 217);
          //doc.rect(145, y-2, 49, 5, 'F')
      
          doc.text(192, y + 11, ''+ this.numberFormat(produtos[0].vImposto)+"%", 'right');
      
          doc.setFontType("bold");
          doc.text(192, y + 16, ''+ this.numberFormat(totalRate), 'right');
      
          */


    }


    if ((detalhesInterconexaoNac.length == 0) && (detalhesInterconexaoInt.length == 0)) {

      var totalMinuto = 0;
      var totalRate = 0;
      var totalAmount = 0;
      doc.setFillColor(166, 166, 166);
      doc.rect(13, 107, 181, 6, 'F')

      doc.setFontSize(11);
      doc.setTextColor(255, 255, 255);
      doc.setFontStyle('times');
      doc.setFontType("bold");
      doc.text(105, 111, 'Monthly Incomming Account', 'center');

      doc.setFillColor(32, 55, 100);
      doc.rect(13, 113, 181, 6, 'F')


      doc.setFontSize(8);
      doc.setTextColor(255, 255, 255);
      doc.setFontStyle('times');
      doc.setFontType("bold");
      doc.text(15, 117, 'Traffic Month');
      doc.text(40, 117, 'Origin');
      doc.text(65, 117, 'Destination');
      doc.text(95, 117, 'Service');
      doc.text(120, 117, 'Call');
      doc.text(135, 117, 'Minutes');
      doc.text(157, 117, 'Rate');
      doc.text(173, 117, 'Amount ' + factura.moeda_iso);



      doc.setDrawColor(115, 115, 115);
      doc.setLineWidth(0.7);
      doc.line(13, 119.4, 194, 119.4);


      for (var i = 0; i < detalhesInterconexao.length; i++) {
        doc.setFillColor(255, 255, 255);
        doc.setFontSize(7);
        doc.setFontStyle('times');
        doc.setFontType("normal");
        //doc.rect(13, yRect, 181, 5, 'F')
        doc.setTextColor(0);
        doc.text(18, y, '' + detalhesInterconexao[i].periodo);
        doc.text(detalhesInterconexao[i].origin.length >= 24 ? 33 : 40, y, '' + (detalhesInterconexao[i].origin == null ? '' : detalhesInterconexao[i].origin));


        doc.text(detalhesInterconexao[i].destination.length >= 24 ? 60 : 66, y, '' + (detalhesInterconexao[i].destination == null ? '' : detalhesInterconexao[i].destination), { maxWidth: 27, align: 'left' });

        doc.text(detalhesInterconexao[i].destination.length >= 24 ? 85 : 92, y, '' + (detalhesInterconexao[i].service == null ? '' : detalhesInterconexao[i].service), { maxWidth: 30 });
        doc.text(124, y, '' + (detalhesInterconexao[i].call == null ? '' : detalhesInterconexao[i].call), 'center');
        doc.text(140, y, '' + (detalhesInterconexao[i].minutes == null ? '' : this.numberFormat(detalhesInterconexao[i].minutes)), 'center');
        doc.text(160, y, '' + (detalhesInterconexao[i].rate == null ? '' : this.numberFormatRate(detalhesInterconexao[i].rate)), 'center');
        doc.text(190, y, '' + this.numberFormat(detalhesInterconexao[i].amount), 'right');


        doc.setDrawColor(115, 115, 115);
        doc.setLineWidth(0.3);
        doc.line(13, yLine, 194, yLine);

        var destinationLength = (detalhesInterconexao[i].destination == null ? 0 : detalhesInterconexao[i].destination.length);
        var serviceLength = (detalhesInterconexao[i].service == null ? 0 : detalhesInterconexao[i].service.length);

        total += detalhesInterconexao[i].amount;
        totalMinuto += detalhesInterconexao[i].minutes;
        y += (destinationLength > 24 ? 8 : 5 && serviceLength > 26 ? 8 : 5);
        yRect += 5;
        yLine += (destinationLength > 24 ? 8 : 5 && serviceLength > 26 ? 8 : 5);

        if (doc.internal.pageSize.height < (y + 35)) {
          doc.addPage();
          textoLateral();
          y = 33.5;
          yLine = 30;


        }
      }



      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.3);
      doc.line(13, yLine, 194, yLine);

      doc.setDrawColor(179, 179, 179);
      doc.setLineWidth(0.3);
      doc.line(105, yLine + 2, 194, yLine + 2);

      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.setFont("times");
      doc.setFontSize(8);
      doc.text('TOTAL', 13, y + 2);

      doc.text(122, y + 2, '', 'center');
      doc.text(140, y + 2, '' + this.numberFormat(factura.minutos_interconexao), 'center');
      //doc.text(140, y + 2, ''+(factura.minutos_interconexao == null ? '' : factura.minutos_interconexao), 'center');
      doc.text(192, y + 2, '' + this.numberFormat(factura.totalSemImposto), 'right');

      doc.setDrawColor(179, 179, 179);
      doc.setLineWidth(0.3);
      doc.line(13, y + 3, 194, y + 3);

      doc.text('VAT TAX', 13, y + 11);
      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.3);
      doc.line(13, y + 12.5, 194, y + 12.5);

      doc.text('Total VAT', 13, y + 16);

      doc.setTextColor(0);
      doc.setFontType("normal");
      doc.setFont("times");
      doc.setFontSize(8);
      //doc.setFillColor(217, 217, 217);
      //doc.rect(145, y-2, 49, 5, 'F')

      doc.text(192, y + 11, '' + this.numberFormat(produtos[0].vImposto) + "%", 'right');

      doc.setFontType("bold");
      doc.text(192, y + 16, '' + this.numberFormat(factura.totalComImposto), 'right');





    }

    doc.setDrawColor(179, 179, 179);
    doc.setLineWidth(0.3);
    doc.line(13, y + 17.5, 194, y + 17.5);

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setFont("times");
    doc.setFontSize(8);
    doc.text('TOTAL AMOUNT DUE TO ANGOLA TELECOM', 13, y + 21);


    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setFont("times");
    doc.setFontSize(8);
    doc.text(192, y + 21, '' + this.numberFormat(factura.total), 'right');

    doc.setDrawColor(179, 179, 179);
    doc.setLineWidth(0.3);
    doc.line(13, y + 22, 194, y + 22);



    newPage();
    footer();

    function footer() {
      doc.setTextColor(0);
      doc.setFontType("normal");
      doc.setFontSize(11);
      doc.text('This invoice has been issued electronically and is therefore valid without a stamp or a signature', 13, doc.internal.pageSize.width + 25);
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.setFont("times");
      doc.setFontSize(9);
      doc.text('NOTICE', 13, doc.internal.pageSize.width + 55);
      doc.setFontType("normal");
      doc.setFontSize(8);
      doc.text('Please note that, if you do not question our telephone invoce/statement with 30 days after receipt, it will be considered as accepted.', 13, doc.internal.pageSize.width + 60);
    }

    function newPage() {
      if (y > 203) {

        doc.addPage();


      }

    }


    doc.autoPrint();
    doc.output('dataurlnewwindow');     //opens the data uri in new window


  }

  public numberFormat(number) {
    return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number).replace('€', '').trim();
  }

  public numberFormatRate(number) {
    return number.toFixed(4)
  }

}
