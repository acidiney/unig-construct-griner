import { TestBed } from '@angular/core/testing';

import { FacturaPosPagoIplcService } from './factura-pos-pago-iplc.service';

describe('FacturaPosPagoIplcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacturaPosPagoIplcService = TestBed.get(FacturaPosPagoIplcService);
    expect(service).toBeTruthy();
  });
});
