import { TestBed } from '@angular/core/testing';

import { FacturaReportService } from './factura-report.service';

describe('FacturaReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacturaReportService = TestBed.get(FacturaReportService);
    expect(service).toBeTruthy();
  });
});
