import { TestBed } from '@angular/core/testing';

import { NotaCreditoIplcService } from './nota-credito-iplc.service';

describe('NotaCreditoIplcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotaCreditoIplcService = TestBed.get(NotaCreditoIplcService);
    expect(service).toBeTruthy();
  });
});
