import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from '@shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class ReportFacturaEnviadaEmailService {

  constructor(private configService: ConfigService) { }
  public relatorioFacturaEmail(file, p = 'print', filter) {

    var today = moment().format("DD-MM-YYYY H:mm:ss");

    var imgData = this.configService.logotipoBase64;
    var doc = new jsPDF('l', '', 'a4')
    doc.setProperties({
      title: 'Report_Facturas_enviadas_email ' + today,
      subject: 'Report',
      author: 'Unig',
      keywords: '',
      creator: 'Angola Telecom'
    });


    var versao = 'F.E.E 1.0.0';
    doc.addImage(imgData, 'JPEG', 140, 10, 28, 18)
    const totalPagesExp = "{total_pages_count_string}";

    doc.setFontType('bold')
    doc.setFontSize(15)
    doc.setFontSize(15)
    doc.text(150, 40, 'Facturas Enviadas Por Email', 'center')

    doc.setFontSize(9)
    doc.text('Estado: ', 14, 45);
    doc.text('Nome Cliente: ', 14, 50);
    doc.text('Direcção: ', 14, 55);
    doc.text(filter.data1 != null ? 'Data:' : 'Periodo:', 14, 60);
    doc.setFontType('normal')
    doc.text('' + (filter.estado == 'T' ? 'Todos' : filter.estado == '1' ? 'Enviado' : 'Não Enviado'), 36, 45);
    doc.text('' + (filter.cliente == null ? 'Todos' : filter.cliente), 36, 50);
    doc.text('' + (filter.direccao == 'T' || filter.direccao == null ? 'Todas' : filter.direccao), 36, 55);
    doc.text('' + (filter.data1 != null ? 'De ' + moment(filter.data1).format("DD/MM/YYYY") + ' a ' + moment(filter.data2).format("DD/MM/YYYY") : filter.ano == null && filter.mes==null ?'Todos':(filter.ano==null?'':filter.ano)+(filter.mes== null ?'':filter.mes)), 36, 60);


    doc.autoTable({
      html: file,
      didParseCell: function (data) {
        var rows = data.table.body;
        if (data.row.index === 0) {
          data.cell.styles.fontStyle = 'bold';
          data.cell.styles.textColor = "white";
          data.cell.styles.fillColor = [32, 95, 190];
        }
        if (data.row.index === 0) {
          data.cell.styles.fontStyle = 'bold';
        }
      },
      didDrawPage: data => {
        let footerStr = "Página " + doc.internal.getNumberOfPages();
        if (typeof doc.putTotalPages === 'function') {
          footerStr = footerStr + " de " + totalPagesExp;
        }
        doc.setFontType('bold')
        doc.setFontSize(10);
        doc.text(versao, 267, 200, 'left')
        doc.text(today, 150, 200, 'center')
        doc.setFontSize(10);
        doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
      },
      styles: { textColor: [0, 0, 0] }, margin: { bottom: 20 },
      columnStyles: {
        0: { cellWidth: 70, halign: 'center' },
        1: { cellWidth: 20, halign: 'center' },
        2: { cellWidth: 40, halign: 'center' },
        3: { cellWidth: 65, halign: 'center' },
        4: { cellWidth: 25, halign: 'center' },
        5: { cellWidth: 20, halign: 'center' },
        6: { cellWidth: 30, halign: 'center' },
      },
      rowsStyles: {},
      startY: 64,
      theme: 'grid',

    })

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp);
    }

    if (p === 'save') {
      doc.save('Report_Facturas_enviadas_Email ' + today + '.pdf');
    } else {
      doc.autoPrint();
      doc.output("dataurlnewwindow");
    }


  }
}
