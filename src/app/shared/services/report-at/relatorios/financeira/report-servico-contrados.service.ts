import { Injectable } from '@angular/core'; 
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from '@shared/services/config.service';
@Injectable({
  providedIn: 'root'
})
export class ReportServicoContradosService {

  constructor(private configService: ConfigService) { }

  public relatorioServicoContratados(file, cliente, p = 'print') {

    var imgData = this.configService.logotipoBase64;
    var doc = new jsPDF('l', '', 'a4')
        doc.setProperties({
        title: 'Report_Servicos_contratados',
        subject: 'Report',
        author: 'Unig',
        keywords: '',
        creator: 'Angola Telecom'
        });

    doc.addImage(imgData, 'JPEG',140, 10,28, 18)
    const totalPagesExp = "{total_pages_count_string}";

        doc.setFontType('bold')
        doc.setFontSize(15)
       // doc.text(145, 60, 'Relatório', 'center')
        doc.setFontSize(15)
        doc.text(150, 49, 'Serviços Contratados', 'center')
        doc.setFontSize(10)
        doc.text('Cliente:', 14, 59);
        doc.setFontType('normal')
        doc.text(30, 59,cliente.cliente)
        doc.setFontType('bold')

  doc.autoTable({ html: file ,
    didParseCell: function (data) {
    var rows = data.table.body;
    if (data.row.index === 0) {
        data.cell.styles.fontStyle = 'bold';
    }
},
    didDrawPage : data => {
      let footerStr = "Página " + doc.internal.getNumberOfPages();
      if (typeof doc.putTotalPages === 'function') {
        footerStr = footerStr + " de " + totalPagesExp;
      }
      doc.setFontSize(10);
      doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
    },
    styles: { textColor: [0, 0, 0] },
    columnStyles: {
    0: {cellWidth: 20,halign: 'center'},
    1: {cellWidth: 40,halign: 'center'},
    2: {cellWidth: 25,halign: 'center'},
    3: {cellWidth: 45,halign: 'center'},
    4: {cellWidth: 25,halign: 'center'},
    5: {cellWidth: 82,halign: 'center'},
    6: {cellWidth: 30,halign: 'center'}},
    rowsStyles:{},
  startY: 70,
  theme: 'grid',
  
  })

  if (typeof doc.putTotalPages === 'function') {
    doc.putTotalPages(totalPagesExp);
  }
 
  if (p === 'save') {
    doc.save('Report_Servicos_contratados'+'.pdf');
  } else {
    doc.autoPrint();
    doc.output("dataurlnewwindow");
  }
   }
}
