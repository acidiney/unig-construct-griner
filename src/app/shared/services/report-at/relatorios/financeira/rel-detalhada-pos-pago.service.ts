import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from '@shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class RelDetalhadaPosPagoService {

  constructor(private configService: ConfigService) { }

  public relatorioFacturacaoPospago(file, p = 'print',filtros) {

    var today = moment().format("DD-MM-YYYY H:mm:ss");

    var imgData = this.configService.logotipoBase64;
    var doc = new jsPDF('l', '', 'a4')
        doc.setProperties({
        title: 'Facturacao_Detalhada_Pos-Pago'+today,
        subject: 'Report',
        author: 'Unig',
        keywords: '',
        creator: 'Angola Telecom'
        });
    var versao = 'F.D.P 1.0.0';
    doc.addImage(imgData, 'JPEG',140, 10, 28, 18)
    const totalPagesExp = "{total_pages_count_string}";

        doc.setFontType('bold')
        doc.setFontSize(15)
  
        doc.setFontSize(15)
        doc.text(150, 40, 'Facturação Detalhada Pos-Pago', 'center')
        doc.setFontSize(9)
        doc.text('Ano:', 14, 45);
        doc.text('Mês:', 14, 50);
        doc.text('Moeda:', 14, 55);
        doc.text('Direcção:', 14, 60);
        doc.text('Nome Cliente:', 14, 65);
        doc.text('Gestor Conta:', 14, 70);
        doc.text('Nome Serviço:', 14, 75);
        
        doc.setFontType('normal')
        doc.text('' + (filtros.ano), 37, 45);
        doc.text('' + (filtros.mes == 'T'      || filtros.mes == null      ? 'Todos' : filtros.mes_nome), 37, 50);
        doc.text('' + (filtros.moeda == 'T' || filtros.moeda == null ? 'Todas' : filtros.moeda), 37, 55);
        doc.text('' + (filtros.direccao == 'T' || filtros.direccao == null ? 'Todas' : filtros.direccao), 37, 60);
        doc.text('' + (filtros.cliente == 'T'  || filtros.cliente == null  ? 'Todos' : filtros.cliente), 37, 65);
        doc.text('' + (filtros.gestor == 'T'   || filtros.gestor == null   ? 'Todos' : filtros.gestor), 37, 70);
        doc.text('' + (filtros.servico == 'T'  || filtros.servico == null ? 'Todos' : filtros.servico), 37, 75);
       
    


        doc.setFontType('bold')
    doc.autoTable({ html: file ,
    didParseCell: function (data) {
    var rows = data.table.body;
    if (data.row.index === 0) {
      data.cell.styles.fontStyle = 'bold';
      data.cell.styles.textColor = "white";
      data.cell.styles.fillColor = [32, 95, 190];
    }
    if (data.row.index === 0) {
      data.cell.styles.fontStyle = 'bold';
    } if (data.row.index === rows.length - 1) {
      data.cell.styles.fontStyle = 'bold';
    }
    },
    didDrawPage : data => {
      let footerStr = "Página " + doc.internal.getNumberOfPages();
      if (typeof doc.putTotalPages === 'function') {
        footerStr = footerStr + " de " + totalPagesExp;
      }
        doc.setFontType('bold')
        doc.setFontSize(10);
        doc.text(versao, 267, 200, 'left')
        doc.text(today, 150, 200, 'center')
        doc.setFontSize(10);
        doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
    },
    styles: { textColor: [0, 0, 0] },margin: {bottom:25}, 
    tableWidth: 20,
    columnStyles: {
    0: {cellWidth: 18,halign: 'center'},
    1: {cellWidth: 25,halign: 'center'},
    2: {cellWidth: 20,halign: 'center'},
    3: {cellWidth: 27,halign: 'center'},
    4: {cellWidth: 20,halign: 'center'},
    5: {cellWidth: 21,halign: 'center'},
    6: {cellWidth: 20,halign: 'center'},
    7: {cellWidth: 20,halign: 'center'},
    8: {cellWidth: 15,halign: 'center'},
    9: {cellWidth: 21,halign: 'center'},
    10: {cellWidth: 19,halign: 'center'},
    11: {cellWidth: 25,halign: 'center'},
    12: {cellWidth: 20,halign: 'center'}},
    rowsStyles:{},
  startY: 80,
  theme: 'grid',
  
  })

  if (typeof doc.putTotalPages === 'function') {
    doc.putTotalPages(totalPagesExp);
  }
 
  if (p === 'save') {
    doc.save('Facturacao_Detalhada_Pos-Pago'+today+'.pdf');
  } else {
    doc.autoPrint();
    doc.output("dataurlnewwindow");
  }
 

  }

}
