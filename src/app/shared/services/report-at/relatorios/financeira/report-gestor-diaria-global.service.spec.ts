import { TestBed } from '@angular/core/testing';

import { ReportGestorDiariaGlobalService } from './report-gestor-diaria-global.service';

describe('ReportGestorDiariaGlobalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportGestorDiariaGlobalService = TestBed.get(ReportGestorDiariaGlobalService);
    expect(service).toBeTruthy();
  });
});
