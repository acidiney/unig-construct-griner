import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from '@shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class RelCobracaGlobalService {

  constructor(private configService: ConfigService) { }

  public relatorioCobracaGlobal(file, filter, p = 'print',titulo) {

    var imgData = this.configService.logotipoBase64;
    var doc = new jsPDF('l', '', 'a4')
        doc.setProperties({
        title: titulo+'_'+filter.tipoFacturacao,
        subject: 'Report',
        author: 'Unig',
        keywords: '',
        creator: 'Angola Telecom'
        });

    doc.addImage(imgData, 'JPEG',140, 10,28, 18)
    const totalPagesExp = "{total_pages_count_string}";

        doc.setFontType('bold')
        doc.setFontSize(15)
       // doc.text(145, 60, 'Relatório', 'center')
        doc.setFontSize(15)
        doc.text(150, 50,titulo, 'center')

        
        doc.setFontSize(9)
        doc.text('Ano: ', 14, 50);
        doc.text('Direcção: ', 14, 55);
        doc.text('Tipo Facturação: ', 14, 60);
        doc.text('Loja:', 14, 65);
        doc.text('Produto: ', 14, 70);
        doc.setFontType('normal')
    
        doc.text('' + filter.ano, 43, 50);
        doc.text('' + (filter.direccao === 'T' ? 'Todas' : filter.direccao), 43, 55);
        doc.text('' + filter.tipoFacturacao.replace("'PRE-PAGO','POS-PAGO'", "PRE-PAGO, POS-PAGO").replace("'", ""), 43, 60);
        doc.text('' + (filter.loja == "T" ? 'Todas' : filter.loja_nome), 43, 65);
        doc.text('' + (filter.produto == "T" ? 'Todos' : filter.produto_nome), 43, 70);
        doc.setFontType('bold')
  doc.autoTable({ html: file ,
    didParseCell: function (data) {
    var rows = data.table.body;
    if (data.row.index === 0) {
        data.cell.styles.fontStyle = 'bold';
    } if (data.row.index === rows.length - 1) {
      data.cell.styles.fontStyle = 'bold';
  }
},
    didDrawPage : data => {
      let footerStr = "Página " + doc.internal.getNumberOfPages();
      if (typeof doc.putTotalPages === 'function') {
        footerStr = footerStr + " de " + totalPagesExp;
      }
      doc.setFontSize(10);
      doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
    },
    styles: { textColor: [0, 0, 0] },margin: {bottom : 22},
    columnStyles: {
    0: {cellWidth: 18,halign: 'center'},
    1: {cellWidth: 18,halign: 'center'},
    2: {cellWidth: 19,halign: 'center'},
    3: {cellWidth: 19,halign: 'center'},
    4: {cellWidth: 19,halign: 'center'},
    5: {cellWidth: 19,halign: 'center'},
    6: {cellWidth: 19,halign: 'center'},
    7: {cellWidth: 19,halign: 'center'},
    8: {cellWidth: 19,halign: 'center'},
    9: {cellWidth: 20,halign: 'center'},
    10: {cellWidth: 18,halign: 'center'},
    11: {cellWidth: 21,halign: 'center'},
    12: {cellWidth: 21,halign: 'center'},
    13: {cellWidth: 20,halign: 'center'}},
    rowsStyles:{},
  startY: 80,
  theme: 'grid',
  
  })

  if (typeof doc.putTotalPages === 'function') {
    doc.putTotalPages(totalPagesExp);
  }
 
  if (p === 'save') {
    doc.save(titulo+'_'+filter.tipoFacturacao+'.pdf');
  } else {
    doc.autoPrint();
    doc.output("dataurlnewwindow");
  }
 
 


  }
}
