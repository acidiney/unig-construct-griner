import { TestBed } from '@angular/core/testing';

import { ReportFacturacaoDiariaGlobalService } from './report-facturacao-diaria-global.service';

describe('ReportFacturacaoDiariaGlobalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportFacturacaoDiariaGlobalService = TestBed.get(ReportFacturacaoDiariaGlobalService);
    expect(service).toBeTruthy();
  });
});
