import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from '@shared/services/config.service';
@Injectable({
  providedIn: 'root'
})

export class ReportReciboGlobalService {

  constructor(private configService: ConfigService) { }


 

  public relatorioRecibo(file, p = 'print', filtro) {

    var today = moment().format("DD-MM-YYYY H:mm:ss");

    var imgData = this.configService.logotipoBase64;
    var doc = new jsPDF('l', '', 'a4')
    doc.setProperties({
      title: 'Relatorio_Recibos_pos_pago',
      subject: 'Report',
      author: 'Unig',
      keywords: '',
      creator: 'Angola Telecom'
    });
    var versao = 'R.C.P 1.0.0';
    doc.addImage(imgData, 'JPEG', 140, 10, 28, 18)
    const totalPagesExp = "{total_pages_count_string}";

    doc.setFontType('bold')
    doc.setFontSize(15)
    doc.setFontSize(15)
    doc.text(150, 45, 'Recibos Detalhados', 'center')

    doc.setFontSize(9)
    doc.text('Direcção: ', 14, 45);
    doc.text('Provincia: ', 14, 50);
    doc.text('Meio de Pagamento: ', 14, 55);
    doc.text(filtro.data1 != null ? 'Data:' : 'Periodo:', 14, 60);
    doc.setFontType('normal')
    doc.text('' + (filtro.direccao == 'T' ? 'Todos' : filtro.direccao), 50, 45);
    doc.text('' + (filtro.province == 'T' ? 'Todas' : filtro.province_nome), 50, 50);
    doc.text('' + (filtro.forma_pagamento == 'T' ? 'Todos' : filtro.forma_nome), 50, 55);
    doc.text('' + (moment(filtro.data1).format("DD/MM/YYYY") + ' a ' + moment(filtro.data2).format("DD/MM/YYYY")), 50, 60);


    doc.autoTable({
      html: file,
      didParseCell: function (data) {
        var rows = data.table.body;
        if (data.row.index === 0) {
        } if (data.row.index === rows.length - 1) {
          //data.cell.styles.fontStyle = 'bold';
        } else {
          //data.cell.styles.fontSize=8
        }
      },
      didDrawPage: data => {
        let footerStr = "Página " + doc.internal.getNumberOfPages();
        if (typeof doc.putTotalPages === 'function') {
          footerStr = footerStr + " de " + totalPagesExp;
        }
        doc.setFontSize(10);
        doc.setFontType('bold')
        doc.text(versao, 267, 200, 'left')
        doc.text(today, 150, 200, 'center')
        doc.setFontSize(10);
        doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
      },
      styles: { textColor: [0, 0, 0] }, margin: { bottom: 20 },
      columnStyles: {
        0: {cellWidth: 23,halign: 'center'},
        1: {cellWidth: 24,halign: 'center',},
        2: {cellWidth: 35,halign: 'center',},
        3: {cellWidth: 35,halign: 'center',},
        4: {cellWidth: 30,halign: 'center',},
        5: {cellWidth: 30,halign: 'center',},
        6: {cellWidth: 30,halign: 'center',},
        7: {cellWidth: 30,halign: 'center',},
        8: {cellWidth: 35,halign: 'center',}
      },
      rowsStyles: {},
      startY: 65,
      theme: 'grid',
      useCss: true,

    })

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp);
    }

    if (p === 'save') {
      doc.save('Relatorio_Recibos' + today + '.pdf');
    } else {
      doc.autoPrint();
      doc.output("dataurlnewwindow");
    }



  }

}
