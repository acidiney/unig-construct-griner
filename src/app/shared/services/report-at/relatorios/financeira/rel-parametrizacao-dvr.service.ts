import { Injectable } from '@angular/core';
import 'jspdf-autotable';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from '@shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class RelParametrizacaoDvrService {

  constructor(private configService: ConfigService) { }

  public relatorioParametrizacaodvr(p = 'print', file) {
    var imgData = this.configService.logotipoBase64;
    var doc = new jsPDF('l', '', 'a4')
    doc.addImage(imgData, 'JPEG',140, 10,28, 18)
    const totalPagesExp = "{total_pages_count_string}";

        doc.setFontType('bold')
        doc.setFontSize(15)
       // doc.text(145, 60, 'Relatório', 'center')
        doc.setFontSize(15)
        doc.text(150, 50, 'Relatorio Diario de Vendas, Produtos e Serviços', 'center')

  doc.autoTable({ html: file ,didParseCell: function (data) {
    var rows = data.table.body;
    if (data.row.index === 0) {
        data.cell.styles.fontStyle = 'bold';
    } if (data.row.index === rows.length - 1) {
      data.cell.styles.fontStyle = 'bold';
  }
},
    addPageContent: data => {
      let footerStr = "Página " + doc.internal.getNumberOfPages();
      if (typeof doc.putTotalPages === 'function') {
        footerStr = footerStr + " de " + totalPagesExp;
      }
      doc.setFontSize(10);
      doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
    },
    styles: { textColor: [0, 0, 0] },
    columnStyles: {
    0: {cellWidth: 150,halign: 'center',},
    1: {cellWidth: 80,halign: 'center'},
    2: {cellWidth: 37,halign: 'center'}},
    rowsStyles:{},
  startY: 60,
  theme: 'grid',
  
  })

  if (typeof doc.putTotalPages === 'function') {
    doc.putTotalPages(totalPagesExp);
  }
 
     if (p === 'save') {
       doc.save();
     } else {
       doc.autoPrint();
       doc.output("dataurlnewwindow");
     }
 
 
   }
  }
