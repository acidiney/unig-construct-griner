import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from '@shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class ReportIVAService {

  constructor(private configService: ConfigService) { }

  public relatorioIVA(file,file2,file3,filter, p = 'print') {

    let currentpage = 0;
    var today =  moment().format("DD-MM-YYYY H:mm:ss");
    var versao =  'IVA 1.0.0';

    var imgData = this.configService.logotipoBase64;
    var doc = new jsPDF('l', '', 'a4')
        doc.setProperties({
        title: 'Relatorio_IVA_'+today,
        subject: 'Report',
        author: 'Unig',
        keywords: '',
        creator: 'Angola Telecom'
        });

    doc.addImage(imgData, 'JPEG',140, 10,28, 18)
    const totalPagesExp = "{total_pages_count_string}";

        doc.setFontType('bold')
        doc.setFontSize(15)
        doc.setFontSize(15)
        doc.text(150, 40, 'Relatório Imposto de Valor Acrescentado', 'center')
        

        doc.setFontSize(9)
        doc.text('Data Inicial: ', 14, 45);
        doc.text('Data Final: ', 14, 50);
        doc.text('Direcção: ', 14, 55);
        doc.setFontType('normal')
        doc.text('' + moment(filter.data1).format("DD-MM-YYYY"), 35, 45);
        doc.text('' + moment(filter.data2).format("DD-MM-YYYY"), 35, 50);
        doc.text('' + (filter.direccao =='T'?'Todas':filter.direccao), 35, 55);

        //TABELA DE DÉBITOS
        doc.autoTable({ html: file ,
        didParseCell: function (data) {
        var rows = data.table.body;
        if (data.row.index === 0 ) {
        data.cell.styles.fontStyle = 'bold';
        data.cell.styles.textColor = "white";
        data.cell.styles.fillColor = [32,95,190];
        }
        if (data.row.index === 1) {
        data.cell.styles.fontStyle = 'bold';
        data.cell.styles.textColor = "white";
        data.cell.styles.fillColor = [68,76,214];
        data.cell.styles.halign = 'center';
        } if (data.row.index === rows.length - 1) {
        data.cell.styles.fontStyle = 'bold';
        data.cell.styles.halign = 'right';
        }
        },
        didDrawPage : data => {
        let footerStr = "Página " + doc.internal.getNumberOfPages();
        if (currentpage < doc.internal.getNumberOfPages()) {
          doc.setFontType('normal')
          footerStr = footerStr + " de " + totalPagesExp;
          
        }
        doc.setFontType('normal')
        doc.setFontSize(10);
        doc.text(versao,267, 200,'left')
        doc.text(today,150, 200, 'center')
        doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
        currentpage = doc.internal.getNumberOfPages();
        },
          styles: { textColor: [0, 0, 0] },margin: {bottom : 20},
          columnStyles: {
            0: {cellWidth: 33,halign: 'center'},
            1: {cellWidth: 22,halign: 'center'},
            2: {cellWidth: 18,halign: 'center'},
            3: {cellWidth: 30,halign: 'center'},
            4: {cellWidth: 24,halign: 'center'},
            5: {cellWidth: 24,halign: 'center'},
            6: {cellWidth: 18,halign: 'center'},
            7: {cellWidth: 25,halign: 'right'},
            8: {cellWidth: 25,halign: 'right'},
            9: {cellWidth: 25,halign: 'right'},
            10: {cellWidth: 25,halign:'right'}},
          rowsStyles:{},
        startY: 60,
        theme: 'grid',
        
        })

      //TABELA DE CRÉDITOS
        doc.autoTable({ html: file2 ,
          didParseCell: function (data) {
          var rows = data.table.body;
          if (data.row.index === 0 ) {
              data.cell.styles.fontStyle = 'bold';
              data.cell.styles.textColor = "white";
              data.cell.styles.fillColor = [32,95,190];
          }
          if (data.row.index === 1) {
            data.cell.styles.halign = 'center';
            data.cell.styles.fontStyle = 'bold';
            data.cell.styles.textColor = "white";
            data.cell.styles.fillColor = [68,76,214];
            data.cell.styles.halign = 'center';
          }if (data.row.index === rows.length - 1) {
            data.cell.styles.fontStyle = 'bold';
            data.cell.styles.halign = 'right';
        }
        },didDrawPage : data => {
        let footerStr = "Página " + doc.internal.getNumberOfPages();
        if (currentpage != doc.internal.getNumberOfPages()) {
          doc.setFontType('normal')
          footerStr = footerStr + " de " + totalPagesExp;
          
        }
        doc.setFontType('normal')
        doc.setFontSize(10);
        doc.text(versao,267, 200,'left')
        doc.text(today,150, 200, 'center')
        doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
        currentpage = doc.internal.getNumberOfPages();
        },
          
          styles: { textColor: [0, 0, 0] },margin: {bottom : 20},
          columnStyles: {
            0: {cellWidth: 33,halign: 'center'},
            1: {cellWidth: 22,halign: 'center'},
            2: {cellWidth: 18,halign: 'center'},
            3: {cellWidth: 30,halign: 'center'},
            4: {cellWidth: 24,halign: 'center'},
            5: {cellWidth: 24,halign: 'center'},
            6: {cellWidth: 18,halign: 'center'},
            7: {cellWidth: 25,halign: 'right'},
            8: {cellWidth: 25,halign: 'right'},
            9: {cellWidth: 25,halign: 'right'},
            10: {cellWidth: 25,halign:'right'}},
          rowsStyles:{},
        startY: doc.lastAutoTable.finalY + 10,
        theme: 'grid',
        })

        //TABELA DE RESUMO
        doc.autoTable({ html: file3 ,
          didParseCell: function (data) {
          var rows = data.table.body;
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = 'bold';
            data.cell.styles.textColor = "white";
            data.cell.styles.fillColor = [32,95,190];
            data.cell.styles.halign = 'center';
          } if (data.row.index === rows.length - 1) {
            data.cell.styles.fontStyle = 'bold';
          }
        },
        didDrawPage : data => {
        let footerStr = "Página " + doc.internal.getNumberOfPages();
        if (currentpage != doc.internal.getNumberOfPages()) {
          doc.setFontType('normal')
          //footerStr = footerStr + " de " + totalPagesExp;
          
        }
        doc.setFontType('normal')
        doc.setFontSize(10);
        doc.text(versao,267, 200,'left')
        doc.text(today,150, 200, 'center')
        doc.text(footerStr, data.settings.margin.right, doc.internal.pageSize.height - 10);
        currentpage = doc.internal.getNumberOfPages();
        },
          styles: { textColor: [0, 0, 0] },margin: {bottom : 20,left:84},
          columnStyles: {
          0: {cellWidth: 40,halign: 'center',fillColor: [32,95,190],fontStyle:'bold',textColor:"white"},
          1: {cellWidth: 40,halign: 'right'},
          2: {cellWidth: 40,halign: 'right'},
          3: {cellWidth: 40,halign: 'right'},
          4: {cellWidth: 40,halign: 'right'}},
          rowsStyles:{},
        startY: doc.lastAutoTable.finalY + 10,
        theme: 'grid',
        
        })

  if (typeof doc.putTotalPages === 'function') {
    doc.putTotalPages(totalPagesExp);
  }
 
  if (p === 'save') {
    doc.save('Relatorio_IVA_'+today+'.pdf');
  } else {
    doc.autoPrint();
    doc.output("dataurlnewwindow");
  }

  }
}
