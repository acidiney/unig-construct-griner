import { TestBed } from '@angular/core/testing';

import { ReportFacuracaoDiariaServicoService } from './report-facuracao-diaria-servico.service';

describe('ReportFacuracaoDiariaServicoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportFacuracaoDiariaServicoService = TestBed.get(ReportFacuracaoDiariaServicoService);
    expect(service).toBeTruthy();
  });
});
