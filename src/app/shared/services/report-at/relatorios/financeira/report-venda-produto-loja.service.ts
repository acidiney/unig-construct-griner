import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from '@shared/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class ReportVendaProdutoLojaService{

  constructor(private configService: ConfigService) { }

  public relatorioVendasLojaProduto(file, p = 'print',date) {


    var imgData = this.configService.logotipoBase64;
    var doc = new jsPDF('l', '', 'a4')
        doc.setProperties({
        title: 'Report_Venda_produto_loja',
        subject: 'Report',
        author: 'Unig',
        keywords: '',
        creator: 'Angola Telecom'
        });

    doc.addImage(imgData, 'JPEG',140, 10,28, 18)
    const totalPagesExp = "{total_pages_count_string}";

        doc.setFontType('bold')
        doc.setFontSize(15)
       // doc.text(145, 60, 'Relatório', 'center')
        doc.setFontSize(15)
        doc.text(150, 49, 'Vendas por Lojas por Produtos', 'center')
        doc.setFontSize(13)
        doc.text(148, 56, 'De '+moment(date.data1).format("DD/MM/YYYY")+' à '+ moment(date.data2).format("DD/MM/YYYY"), 'center')

  doc.autoTable({ html: file ,
    didParseCell: function (data) {
    var rows = data.table.body;
    if (data.row.index === 0) {
        data.cell.styles.fontStyle = 'bold';
    } if (data.row.index === rows.length - 1) {
      data.cell.styles.fontStyle = 'bold';
  }
},
    didDrawPage : data => {
      let footerStr = "Página " + doc.internal.getNumberOfPages();
      if (typeof doc.putTotalPages === 'function') {
        footerStr = footerStr + " de " + totalPagesExp;
      }
      doc.setFontSize(10);
      doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
    },
    styles: { textColor: [0, 0, 0] },
    columnStyles: {
    0: {cellWidth: 19,halign: 'center'},
    1: {cellWidth: 23,halign: 'center'},
    2: {cellWidth: 20,halign: 'center'},
    3: {cellWidth: 45,halign: 'center'},
    4: {cellWidth: 25,halign: 'center'},
    5: {cellWidth: 82,halign: 'center'},
    6: {cellWidth: 25,halign: 'center'},
    7: {cellWidth: 30,halign: 'center'}},
    rowsStyles:{},
  startY: 60,
  theme: 'grid',
  
  })

  if (typeof doc.putTotalPages === 'function') {
    doc.putTotalPages(totalPagesExp);
  }
 
  if (p === 'save') {
    doc.save('Report_Venda_produto_loja'+'.pdf');
  } else {
    doc.autoPrint();
    doc.output("dataurlnewwindow");
  }
 
 
   }

}
