import { TestBed } from '@angular/core/testing';

import { ReportDiarioVendasService } from './report-diario-vendas.service';

describe('ReportDiarioVendasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportDiarioVendasService = TestBed.get(ReportDiarioVendasService);
    expect(service).toBeTruthy();
  });
});
