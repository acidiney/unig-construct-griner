import { TestBed } from '@angular/core/testing';

import { PedidoReportService } from './pedido-report.service';

describe('PedidoReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PedidoReportService = TestBed.get(PedidoReportService);
    expect(service).toBeTruthy();
  });
});
