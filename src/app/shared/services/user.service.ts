import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { map, debounceTime } from 'rxjs/operators';
import { ApiService } from '@core/providers/http/api.service';
import { User } from '@shared/models/user';
import { Methods } from '@shared/interfaces/methods';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class UserService implements Methods {
  public params = new HttpParams();
  constructor(private http: ApiService) {}

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite listar todas users'
   * @return Observable
   */
  public list(): Observable<any> {
    return this.http.get('users?without-organism=true?page=' + this.params.get('page') + '&perPage=' + this.params.get('perPage')).pipe(
      debounceTime(500),
      map((data) => data.object)
    );
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite visualizar os dados de uma determinada user'
   * @param id
   */
  show(id: number): Observable<User> {
    return this.http
      .get('users/' + id)
      .pipe(map((user) => new User().deserialize(user.object)));
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite registar users'
   * @param form
   * @returns
   */
  store(form: FormGroup): Observable<User> {
    const user = new User().deserialize(form.value);
    return this.http
      .post('users', user)
      .pipe(map((userMapped) => new User().deserialize(userMapped.object)));
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite actualizar os dados de uma determinada user'
   * @param form
   * @param params
   */
  update(form: FormGroup, params): Observable<User> {
    const user = new User().deserialize(form.value);
    return this.http
      .put('users/' + params, user)
      .pipe(map((userMapped) => new User().deserialize(userMapped.object)));
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @description 'Permite eliminar user'
   * @param id
   */
  delete(id: number) {
    return this.http.delete('users/' + id);
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @param form
   * @param params
   */
  updateStatus(params, is_active: boolean): Observable<User> {
    return this.http
      .put('users/status/' + params, { is_active: is_active ? 0 : 1 })
      .pipe(map((user) => new User().deserialize(user.object)));
  }

  /**
   * @author 'caniggia.moreira@ideiasdinamicas.com'
   * @update 'caniggiamoreira@gmail.com'
   * @description 'Permite registar users'
   * @param form
   * @returns
   */
  buscarUser(
    search: string = null,
    organismo_id = null,
    page = null
  ): Observable<any> {

    return this.http
      .get(
        'users?search=' + search + '&organism_id=' + organismo_id + '&page=' + page
      )
      .pipe(map((user) => new User().deserialize(user.object)));
  }

  /**
   * @author 'caniggiamoreira@gmail.com'
   * @description 'Permite alterar senha do users'
   * @param form
   * @returns
   */
  storePassword(form: FormGroup): Observable<User> {
    return this.http.put('users/change_password', {
      newPassword: form.value.newPassword,
      confirmPassword: form.value.confirmPassword,
      password: form.value.password,
    });
  }

  setRedefinirSenha(form: FormGroup, user_id): Observable<User> {
    return this.http.put('users/' + user_id + '/redefine_password', {
      confirmPassword: form.value.confirmPassword,
      password: form.value.password,
    });
  }
}
