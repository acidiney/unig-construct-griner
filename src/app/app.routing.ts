import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers

import { P404Component } from './resources/error/404.component';
import { P403Component } from './resources/error/403.component';
import { P500Component } from './resources/error/500.component';
import { AuthGuard } from '@core/guards/auth.guard';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404',
      layout:{ customLayout: false, layoutNavigationTop: true, layoutModule: 'ALOJAMENTO'}
    },
  },
  {
    path: '403',
    component: P403Component,
    data: {
      title: 'Page 403',
      layout:{ customLayout: false, layoutNavigationTop: true }
    },
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500',
      layout:{ customLayout: false, layoutNavigationTop: true}
    },
  },
  {
    path: 'reclamacoes',
    component: P500Component,
    data: {
      title: 'Page 500',
      layout:{ customLayout: true, layoutNavigationTop: false}
    },
  },
  {
    path: '',
    canActivate: [AuthGuard],
    data: {
      title: 'Dashboard',
    },
    loadChildren: () => import('./resources/views/dashboard/dashboard.module').then((m) => m.DashboardModule)
  },{
    path: 'operacoes',
    //component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Operacões',
    },
    loadChildren: () => import('./resources/views/operacoes/operacoes.module').then((m) => m.OperacoesModule)
  },{
    path: 'bireporting',
    canActivate: [AuthGuard],
    data: {
      title: 'BI & Reporting',
    },
    loadChildren: () => import('./resources/views/bireporting/bireporting.module').then((m) => m.BIReportingModule)
  },{
    path: 'configs',
    //component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Configurações',
      layout: {
        customLayout: true,
        layoutNavigationTop: false,
      },
    },
    loadChildren: () => import('./resources/views/configuracoes/configuracoes.module').then((m) => m.ConfiguracoesModule)
  },
  {
    path: 'crm',
    //component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'CRM',
    },
    loadChildren: () => import('./resources/views/crm/crm.module').then((m) => m.CRMModule),

  },
  {
    path: 'facturacao',
    //component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Facturação',
    },
    loadChildren: () => import('./resources/views/facturacao/facturacao.module').then((m) => m.FacturacaoModule),
  },
  {
    path: 'manutencao',
    //component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: "Listar Manutenções",
      layout: {
        customLayout: true,
        layoutNavigationTop: false,
      },
    },
    loadChildren: () => import('./resources/views/manutencao/manutencao.module').then((m) => m.ManutencaoModule),
  },
  {
    path: 'oficinas',
    //component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: "Oficinas",
      layout: {
        customLayout: true,
        layoutNavigationTop: false,
      },
    },
    loadChildren: () => import('./resources/views/oficina/oficina.module').then((m) => m.OficinaModule),
  },
  {
    path: 'auth',
    data: {
      title: 'Home',
    },
    children: [
      {
        path: 'login',
        data: {
          title: "Login",
          layout:{ customLayout: false, layoutNavigationTop: true }
        },
        loadChildren: () => import('./resources/views/authentication/authentication.module').then((m) => m.AuthenticationModule)
      },
    ],
  },
  { path: '**', component: P404Component, data: {
    title: 'Page 404',
    layout:{ customLayout: false, layoutNavigationTop: true, layoutModule: 'ALOJAMENTO'}
  } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
