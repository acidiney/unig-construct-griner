import { Component, OnInit, Input } from '@angular/core';
import { ConfigService } from "@shared/services/config.service";
import { User } from "@shared/models/user";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  @Input() public currentUser: User
  constructor(public configService: ConfigService) { }

  ngOnInit() {
  }

  public canActivateRouterLink(permission:string){
    return true;
  }
}
