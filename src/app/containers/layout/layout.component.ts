 
import { Component, OnInit, Compiler, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, Event as RouterEvent } from '@angular/router';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { AuthService } from "@core/authentication/auth.service";
import { RoutingService } from "@core/services/routing.service";   
import { NgxSpinnerService } from "ngx-spinner";
import { User } from "@shared/models/user";

@Component({
  selector: 'layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})

export class LayoutComponent implements OnInit, OnDestroy {
  public description: string;
  public header: string;
  public heightStyle: number;
  public sidebarLeftHeight: number;
  public windowInnerHeight: number;
 
  private titleTag: string;
  private navigationEnd: boolean;
  private subscriptions = [];  
  private layout = {
    customLayout: false,
    layoutNavigationTop: true,
    LayoutMenuModule:""
  }


  currentUser : User//{id:'', name:''};
  user: User;
  currentUserSubscription: Subscription;
  /**
   * @method constructor
   * @param authService 
   * @param routingService 
   * @param titleService 
   * @param changeDetectorRef 
   * @param router 
   */ 
  constructor( 
    private authService: AuthService,
    private routingService: RoutingService,
    private titleService: Title, 
    private changeDetectorRef: ChangeDetectorRef, 
    private router: Router,private compiler: Compiler,
    private spinner: NgxSpinnerService
  ) {  
    if(this.authService.isAuthenticated()){

        const user = this.authService.profile();
        this.currentUser = new User().deserialize(user); 
    } 
    this.compiler.clearCache();
  }

  
  /**
   * @method ngOnInit
   */
  ngOnInit() { 
    

    this.titleTag = this.titleService.getTitle();

    this.subscriptions.push(this.routingService.onChange.subscribe((value: any) => {
      if (value && value[value.length - 1]) {
        this.titleService.setTitle(this.getTitle(value[value.length - 1].data['title']));
        this.header = value[value.length - 1].data['title'];   
        const layout = value[value.length - 1].data['layout'];
        if(layout != undefined){
          this.layout = layout; 
        } 
        this.description = value[value.length - 1].data['description'];
      } 
      this.changeDetectorRef.markForCheck();
    }));

    this.subscriptions.push(this.router.events.subscribe((routeEvent: RouterEvent) => {
      if (routeEvent instanceof NavigationStart) {
        this.navigationEnd = false;
      }
      if (routeEvent instanceof NavigationEnd) {
        this.navigationEnd = true; 
      }
    })); 
 
  }

  /**
   * @method ngOnDestroy
   */
  ngOnDestroy() {
     
  }

   

  /**
   * [getTitle description]
   * @method getTitle
   * @param title [description]
   * @return [description]
   */
  private getTitle(title: string): string {
    return title ? `UNIG :: ${title}` : this.titleTag;
  }

   
}
