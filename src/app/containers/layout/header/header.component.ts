import { Component, OnInit, Input } from "@angular/core";
import { UserData } from "@shared/models/userResponse";
import { User } from "@shared/models/user";
import { AuthService } from "@core/authentication/auth.service";

//import { ConnectionState, ConnectionService } from "ngx-connection-service";
@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {
  @Input() public currentUser: User
  @Input() public layoutNavigationTop:boolean = true
  //currentState: ConnectionState;
  
  constructor(
    //private connectionService: ConnectionService,
    private authService: AuthService
  ) {
    
   /* this.connectionService
      .monitor()
      .subscribe((currentState: ConnectionState) => {
        //console.log(currentState);
        this.currentState = currentState;
      });*/
  }

  ngOnInit() {}
  logout() {
    this.authService.logout() 
  } 
}
 